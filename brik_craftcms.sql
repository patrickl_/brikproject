-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2020 at 09:39 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `brik_craftcms`
--

-- --------------------------------------------------------

--
-- Table structure for table `assetindexdata`
--

CREATE TABLE `assetindexdata` (
  `id` int(11) NOT NULL,
  `sessionId` varchar(36) NOT NULL DEFAULT '',
  `volumeId` int(11) NOT NULL,
  `uri` text,
  `size` bigint(20) UNSIGNED DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `recordId` int(11) DEFAULT NULL,
  `inProgress` tinyint(1) DEFAULT '0',
  `completed` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE `assets` (
  `id` int(11) NOT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `folderId` int(11) NOT NULL,
  `uploaderId` int(11) DEFAULT NULL,
  `filename` varchar(255) NOT NULL,
  `kind` varchar(50) NOT NULL DEFAULT 'unknown',
  `width` int(11) UNSIGNED DEFAULT NULL,
  `height` int(11) UNSIGNED DEFAULT NULL,
  `size` bigint(20) UNSIGNED DEFAULT NULL,
  `focalPoint` varchar(13) DEFAULT NULL,
  `deletedWithVolume` tinyint(1) DEFAULT NULL,
  `keptFile` tinyint(1) DEFAULT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `assets`
--

INSERT INTO `assets` (`id`, `volumeId`, `folderId`, `uploaderId`, `filename`, `kind`, `width`, `height`, `size`, `focalPoint`, `deletedWithVolume`, `keptFile`, `dateModified`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(17, 1, 1, 1, 'chemex.jpg', 'image', 3175, 2678, 408807, NULL, 0, 0, '2020-03-04 18:44:06', '2020-03-04 18:44:06', '2020-03-04 18:44:06', '73111d55-d630-4c3a-955b-fdcfa03a25ea'),
(52, 1, 1, 1, 'hakon-sataoen-qyfco1nfMtg-unsplash.jpg', 'image', 5295, 3725, 3528682, NULL, 0, 0, '2020-03-04 19:35:39', '2020-03-04 19:35:40', '2020-03-04 19:35:40', '82c3e750-ef26-4ad3-a4f0-5e72e79f8cef'),
(55, 1, 6, 1, 'meik-schneider-e9zSM8orIfA-unsplash.jpg', 'image', 6000, 4000, 7536397, NULL, 1, 1, '2020-03-04 19:39:34', '2020-03-04 19:39:35', '2020-03-04 19:39:35', '8a17bd0f-572c-4900-be98-a2422674c8b2'),
(60, 1, 8, 1, 'hakon-sataoen-qyfco1nfMtg-unsplash.jpg', 'image', 5295, 3725, 3528682, NULL, 1, 1, '2020-03-04 19:54:53', '2020-03-04 19:54:53', '2020-03-04 19:54:53', 'e639b35b-c167-4eed-bb87-e1bf34df1446'),
(63, 2, 9, 1, 'hakon-sataoen-qyfco1nfMtg-unsplash.jpg', 'image', 5295, 3725, 3528682, NULL, NULL, NULL, '2020-03-04 20:03:53', '2020-03-04 20:03:53', '2020-03-04 20:03:53', 'fc133469-5b76-4d4f-99bf-b5540a01d9de'),
(66, 2, 9, 1, 'kevin-mueller-Sw72_7RSeu0-unsplash.jpg', 'image', 4160, 6240, 2963122, NULL, NULL, NULL, '2020-03-04 20:10:37', '2020-03-04 20:10:37', '2020-03-04 20:10:37', '929dec3b-b5f9-44f2-9a42-7242f324ca94'),
(76, 2, 9, 1, 'ben-white-qDY9ahp0Mto-unsplash.jpg', 'image', 6016, 4016, 2009324, NULL, NULL, NULL, '2020-03-04 20:21:04', '2020-03-04 20:21:04', '2020-03-04 20:21:04', 'c46f6ad4-c708-4802-b102-967ff015fc11'),
(77, 2, 9, 1, 'patrick-ward-z_dLXnQg0JY-unsplash.jpg', 'image', 5958, 3866, 2100732, NULL, NULL, NULL, '2020-03-04 20:22:16', '2020-03-04 20:22:16', '2020-03-04 20:22:16', '150b6be8-acbd-4e37-9713-2a6b6d77ccf5');

-- --------------------------------------------------------

--
-- Table structure for table `assettransformindex`
--

CREATE TABLE `assettransformindex` (
  `id` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `location` varchar(255) NOT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `fileExists` tinyint(1) NOT NULL DEFAULT '0',
  `inProgress` tinyint(1) NOT NULL DEFAULT '0',
  `dateIndexed` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `assettransforms`
--

CREATE TABLE `assettransforms` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `mode` enum('stretch','fit','crop') NOT NULL DEFAULT 'crop',
  `position` enum('top-left','top-center','top-right','center-left','center-center','center-right','bottom-left','bottom-center','bottom-right') NOT NULL DEFAULT 'center-center',
  `width` int(11) UNSIGNED DEFAULT NULL,
  `height` int(11) UNSIGNED DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `quality` int(11) DEFAULT NULL,
  `interlace` enum('none','line','plane','partition') NOT NULL DEFAULT 'none',
  `dimensionChangeTime` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `assettransforms`
--

INSERT INTO `assettransforms` (`id`, `name`, `handle`, `mode`, `position`, `width`, `height`, `format`, `quality`, `interlace`, `dimensionChangeTime`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, '488x692', 'x488692', 'crop', 'center-center', 488, 692, NULL, NULL, 'none', '2020-03-04 20:12:23', '2020-03-04 20:12:23', '2020-03-04 20:12:23', '2cd64a25-250b-4eb1-803f-1a9fd25f5a70');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `deletedWithGroup` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categorygroups`
--

CREATE TABLE `categorygroups` (
  `id` int(11) NOT NULL,
  `structureId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categorygroups_sites`
--

CREATE TABLE `categorygroups_sites` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text,
  `template` varchar(500) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `changedattributes`
--

CREATE TABLE `changedattributes` (
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `attribute` varchar(255) NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `propagated` tinyint(1) NOT NULL,
  `userId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `changedattributes`
--

INSERT INTO `changedattributes` (`elementId`, `siteId`, `attribute`, `dateUpdated`, `propagated`, `userId`) VALUES
(2, 1, 'fieldLayoutId', '2020-03-04 18:32:36', 0, 1),
(5, 1, 'fieldLayoutId', '2020-03-04 18:41:06', 0, 1),
(35, 1, 'fieldLayoutId', '2020-03-04 19:04:17', 0, 1),
(73, 1, 'fieldLayoutId', '2020-03-04 20:20:10', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `changedfields`
--

CREATE TABLE `changedfields` (
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `propagated` tinyint(1) NOT NULL,
  `userId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `changedfields`
--

INSERT INTO `changedfields` (`elementId`, `siteId`, `fieldId`, `dateUpdated`, `propagated`, `userId`) VALUES
(35, 1, 9, '2020-03-04 20:18:50', 0, 1),
(73, 1, 9, '2020-03-04 20:24:25', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `elementId`, `siteId`, `title`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 1, NULL, '2020-03-04 09:23:46', '2020-03-04 09:23:46', '07eed5b4-5fb6-4622-983f-be7a95aee53c'),
(2, 2, 1, 'Home', '2020-03-04 18:32:28', '2020-03-04 18:32:36', '07b0f779-7931-465a-8bda-b3bf74dbae9e'),
(3, 3, 1, 'Home', '2020-03-04 18:32:28', '2020-03-04 18:32:28', '0e82a82e-f595-4b01-88be-36e63151584f'),
(4, 4, 1, 'Home', '2020-03-04 18:32:36', '2020-03-04 18:32:36', '1e6fec85-2be1-4508-9fbf-cc27458508b1'),
(5, 5, 1, 'Homepage', '2020-03-04 18:40:40', '2020-03-04 18:44:39', 'a09c0982-e148-4bcd-9c9c-36f022d8cc3b'),
(6, 6, 1, 'Homepage', '2020-03-04 18:40:40', '2020-03-04 18:40:40', 'd780906c-6599-419a-8fa9-348305d8cce8'),
(7, 7, 1, 'Homepage', '2020-03-04 18:41:06', '2020-03-04 18:41:06', 'f0b61097-9377-4819-8da7-f305c7fa491c'),
(8, 12, 1, 'Homepage', '2020-03-04 18:42:26', '2020-03-04 18:42:26', '4e142a6b-3b0e-42cf-88ad-427143b9183d'),
(9, 17, 1, 'Chemex', '2020-03-04 18:44:05', '2020-03-04 18:44:05', '81a49349-b5de-43a3-9b49-2ac671e95b16'),
(11, 29, 1, 'Homepage', '2020-03-04 18:44:39', '2020-03-04 18:44:39', 'ea7600b6-27ca-4896-ba0e-eb6ea5dd8748'),
(12, 35, 1, 'Homepage', '2020-03-04 19:02:23', '2020-03-04 20:18:50', '126f17f9-690b-4ed2-99cf-5cce5dc0c04f'),
(13, 36, 1, 'Homepage', '2020-03-04 19:02:23', '2020-03-04 19:02:23', '793333dd-2c0b-43ac-a9ad-b98f9ebf1c6c'),
(14, 37, 1, 'Homepage', '2020-03-04 19:04:17', '2020-03-04 19:04:17', 'ba042f6d-90a7-4112-b718-2d3312a2a53e'),
(15, 40, 1, 'Homepage', '2020-03-04 19:07:42', '2020-03-04 19:07:42', '2d881f46-a657-4af0-ab53-b4cb6ead8c3c'),
(16, 43, 1, 'Homepage', '2020-03-04 19:08:41', '2020-03-04 19:08:41', '0e55d433-efcb-429e-b2de-9b4bf0e23305'),
(17, 46, 1, 'Homepage', '2020-03-04 19:23:51', '2020-03-04 19:23:51', 'cae8325c-787e-4048-a3b7-30433987f48d'),
(18, 48, 1, 'Homepage', '2020-03-04 19:24:32', '2020-03-04 19:24:32', '00bcaa9c-73b0-4c36-928e-9b2d00c113f9'),
(19, 50, 1, 'Homepage', '2020-03-04 19:31:45', '2020-03-04 19:31:45', '18e8f027-36c7-47a2-a60d-4dccce6e6b64'),
(20, 52, 1, 'Hakon sataoen qyfco1nf Mtg unsplash', '2020-03-04 19:35:36', '2020-03-04 19:35:36', '99048480-c6d1-4b59-b28b-2d09b84ee4ca'),
(21, 53, 1, 'Homepage', '2020-03-04 19:35:52', '2020-03-04 19:35:52', 'b359846d-cea4-44de-ab89-f2229a790fd7'),
(22, 55, 1, 'Meik schneider e9z SM8or If A unsplash', '2020-03-04 19:39:31', '2020-03-04 19:39:31', '597d3052-7112-4a94-b4e2-4491f05bc809'),
(23, 56, 1, 'Homepage', '2020-03-04 19:39:41', '2020-03-04 19:39:41', 'e23305d8-dc4b-4640-afec-1d19c4df2e7f'),
(24, 58, 1, 'Homepage', '2020-03-04 19:47:30', '2020-03-04 19:47:30', '0cb80b5a-e67e-4a7a-a011-dc066e8cf1ba'),
(25, 60, 1, 'Hakon sataoen qyfco1nf Mtg unsplash', '2020-03-04 19:54:50', '2020-03-04 19:54:50', '113a6663-c830-4f0f-bf1e-d673b54c6c1a'),
(26, 61, 1, 'Homepage', '2020-03-04 19:54:56', '2020-03-04 19:54:56', '5f911773-35db-4375-930f-4cb1f4bd72a6'),
(27, 63, 1, 'Hakon sataoen qyfco1nf Mtg unsplash', '2020-03-04 20:03:50', '2020-03-04 20:03:50', '7d65c22f-6b65-491a-862f-25fe9e9f1fd5'),
(28, 64, 1, 'Homepage', '2020-03-04 20:03:56', '2020-03-04 20:03:56', '1470e75a-1d7c-4ab4-a675-98ebc5e78241'),
(29, 66, 1, 'Kevin mueller Sw72 7 R Seu0 unsplash', '2020-03-04 20:10:34', '2020-03-04 20:10:34', 'fa537eb1-6bdd-42b5-a34f-628a12589b5d'),
(30, 67, 1, 'Homepage', '2020-03-04 20:10:40', '2020-03-04 20:10:40', '795d0210-d408-484f-a719-c04b3aa38b0e'),
(31, 70, 1, 'Homepage', '2020-03-04 20:18:50', '2020-03-04 20:18:50', '166c390d-448a-42f9-9ee9-dd40d2a604af'),
(32, 73, 1, 'TestPage', '2020-03-04 20:19:53', '2020-03-04 20:24:25', '5693d89b-57bf-48e3-a405-857a253648ad'),
(33, 74, 1, 'TestPage', '2020-03-04 20:19:53', '2020-03-04 20:19:53', '157abb6b-ed12-4110-a6bb-bf38693481a2'),
(34, 75, 1, 'TestPage', '2020-03-04 20:20:10', '2020-03-04 20:20:10', '31a38c4d-1702-4961-8785-bedbaba0afc6'),
(35, 76, 1, 'Ben white q DY9ahp0 Mto unsplash', '2020-03-04 20:21:02', '2020-03-04 20:21:02', 'f91549f8-0e76-4c8e-9a8d-1a83ccc69b37'),
(36, 77, 1, 'Patrick ward z d L Xn Qg0 JY unsplash', '2020-03-04 20:22:13', '2020-03-04 20:22:13', '34e10dcb-80d5-47a0-9b4c-d7f284bd0359'),
(37, 80, 1, 'TestPage', '2020-03-04 20:22:19', '2020-03-04 20:22:19', '57b52999-061d-4096-9c7b-de30b671e48f'),
(38, 83, 1, 'TestPage', '2020-03-04 20:23:23', '2020-03-04 20:23:23', '2a6e13a4-f476-4b9e-8912-c93780a29cb7'),
(39, 86, 1, 'TestPage', '2020-03-04 20:24:25', '2020-03-04 20:24:25', '7554efba-1fb0-48a1-a834-8b5541ca8b6d');

-- --------------------------------------------------------

--
-- Table structure for table `craftidtokens`
--

CREATE TABLE `craftidtokens` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `accessToken` text NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `deprecationerrors`
--

CREATE TABLE `deprecationerrors` (
  `id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `fingerprint` varchar(255) NOT NULL,
  `lastOccurrence` datetime NOT NULL,
  `file` varchar(255) NOT NULL,
  `line` smallint(6) UNSIGNED DEFAULT NULL,
  `message` text,
  `traces` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `deprecationerrors`
--

INSERT INTO `deprecationerrors` (`id`, `key`, `fingerprint`, `lastOccurrence`, `file`, `line`, `message`, `traces`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'ElementQuery::getIterator()', 'C:\\xampp\\htdocs\\CraftCMS\\craft\\templates\\index.twig:13', '2020-03-04 18:50:58', 'C:\\xampp\\htdocs\\CraftCMS\\craft\\templates\\index.twig', 13, 'Looping through element queries directly has been deprecated. Use the all() function to fetch the query results before looping over them.', '[{\"objectClass\":\"craft\\\\services\\\\Deprecator\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\elements\\\\db\\\\ElementQuery.php\",\"line\":558,\"class\":\"craft\\\\services\\\\Deprecator\",\"method\":\"log\",\"args\":\"\\\"ElementQuery::getIterator()\\\", \\\"Looping through element queries directly has been deprecated. Us...\\\"\"},{\"objectClass\":\"craft\\\\elements\\\\db\\\\MatrixBlockQuery\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\storage\\\\runtime\\\\compiled_templates\\\\f6\\\\f69414bc3fcd41b5b054062e45e1117026915a04b4a3ee31e45d09615ef5cca0.php\",\"line\":73,\"class\":\"craft\\\\elements\\\\db\\\\ElementQuery\",\"method\":\"getIterator\",\"args\":null},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":407,\"class\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":380,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":392,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\TemplateWrapper.php\",\"line\":45,\"class\":\"Twig\\\\Template\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], []\"},{\"objectClass\":\"Twig\\\\TemplateWrapper\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Environment.php\",\"line\":318,\"class\":\"Twig\\\\TemplateWrapper\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\twig\\\\Environment\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\View.php\",\"line\":397,\"class\":\"Twig\\\\Environment\",\"method\":\"render\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\View.php\",\"line\":458,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Controller.php\",\"line\":235,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderPageTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], \\\"site\\\"\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\controllers\\\\TemplatesController.php\",\"line\":98,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"renderTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":null,\"line\":null,\"class\":\"craft\\\\controllers\\\\TemplatesController\",\"method\":\"actionRender\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":null,\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\InlineAction.php\",\"line\":57,\"class\":null,\"method\":\"call_user_func_array\",\"args\":\"[craft\\\\controllers\\\\TemplatesController, \\\"actionRender\\\"], [\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"yii\\\\base\\\\InlineAction\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Controller.php\",\"line\":157,\"class\":\"yii\\\\base\\\\InlineAction\",\"method\":\"runWithParams\",\"args\":\"[\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Controller.php\",\"line\":178,\"class\":\"yii\\\\base\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Module.php\",\"line\":528,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Application.php\",\"line\":291,\"class\":\"yii\\\\base\\\\Module\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\web\\\\Application.php\",\"line\":103,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Application.php\",\"line\":276,\"class\":\"yii\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Application.php\",\"line\":386,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\web\\\\index.php\",\"line\":21,\"class\":\"yii\\\\base\\\\Application\",\"method\":\"run\",\"args\":null}]', '2020-03-04 18:50:58', '2020-03-04 18:50:58', '133d4120-1681-4a70-bdfd-1e067e175a66'),
(3, 'ElementQuery::first()', 'C:\\xampp\\htdocs\\CraftCMS\\craft\\templates\\_modules\\moduleimage.twig:1', '2020-03-04 18:50:58', 'C:\\xampp\\htdocs\\CraftCMS\\craft\\templates\\_modules\\moduleimage.twig', 1, 'The first() function used to query for elements is now deprecated. Use one() instead.', '[{\"objectClass\":\"craft\\\\services\\\\Deprecator\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\elements\\\\db\\\\ElementQuery.php\",\"line\":1845,\"class\":\"craft\\\\services\\\\Deprecator\",\"method\":\"log\",\"args\":\"\\\"ElementQuery::first()\\\", \\\"The first() function used to query for elements is now deprecate...\\\"\"},{\"objectClass\":\"craft\\\\elements\\\\db\\\\AssetQuery\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Extension\\\\CoreExtension.php\",\"line\":1499,\"class\":\"craft\\\\elements\\\\db\\\\ElementQuery\",\"method\":\"first\",\"args\":null},{\"objectClass\":null,\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\helpers\\\\Template.php\",\"line\":96,\"class\":null,\"method\":\"twig_get_attribute\",\"args\":\"craft\\\\web\\\\twig\\\\Environment, Twig\\\\Source, craft\\\\elements\\\\db\\\\AssetQuery, \\\"first\\\", ...\"},{\"objectClass\":null,\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\storage\\\\runtime\\\\compiled_templates\\\\a0\\\\a0d2659abbc76b79f9e20b78f24c8ea6d9485bc74d0b523fab30a7da4d3b6adb.php\",\"line\":38,\"class\":\"craft\\\\helpers\\\\Template\",\"method\":\"attribute\",\"args\":\"craft\\\\web\\\\twig\\\\Environment, Twig\\\\Source, craft\\\\elements\\\\db\\\\AssetQuery, \\\"first\\\", ...\"},{\"objectClass\":\"__TwigTemplate_12ed15612bdc5f6c3058b25609456fb3bff792348bf22d3631ff6ab059fa9028\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":407,\"class\":\"__TwigTemplate_12ed15612bdc5f6c3058b25609456fb3bff792348bf22d3631ff6ab059fa9028\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_12ed15612bdc5f6c3058b25609456fb3bff792348bf22d3631ff6ab059fa9028\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":380,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_12ed15612bdc5f6c3058b25609456fb3bff792348bf22d3631ff6ab059fa9028\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\storage\\\\runtime\\\\compiled_templates\\\\f6\\\\f69414bc3fcd41b5b054062e45e1117026915a04b4a3ee31e45d09615ef5cca0.php\",\"line\":78,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...]\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":407,\"class\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":380,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":392,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\TemplateWrapper.php\",\"line\":45,\"class\":\"Twig\\\\Template\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], []\"},{\"objectClass\":\"Twig\\\\TemplateWrapper\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Environment.php\",\"line\":318,\"class\":\"Twig\\\\TemplateWrapper\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\twig\\\\Environment\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\View.php\",\"line\":397,\"class\":\"Twig\\\\Environment\",\"method\":\"render\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\View.php\",\"line\":458,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Controller.php\",\"line\":235,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderPageTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], \\\"site\\\"\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\controllers\\\\TemplatesController.php\",\"line\":98,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"renderTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":null,\"line\":null,\"class\":\"craft\\\\controllers\\\\TemplatesController\",\"method\":\"actionRender\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":null,\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\InlineAction.php\",\"line\":57,\"class\":null,\"method\":\"call_user_func_array\",\"args\":\"[craft\\\\controllers\\\\TemplatesController, \\\"actionRender\\\"], [\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"yii\\\\base\\\\InlineAction\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Controller.php\",\"line\":157,\"class\":\"yii\\\\base\\\\InlineAction\",\"method\":\"runWithParams\",\"args\":\"[\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Controller.php\",\"line\":178,\"class\":\"yii\\\\base\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Module.php\",\"line\":528,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Application.php\",\"line\":291,\"class\":\"yii\\\\base\\\\Module\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\web\\\\Application.php\",\"line\":103,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Application.php\",\"line\":276,\"class\":\"yii\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Application.php\",\"line\":386,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\web\\\\index.php\",\"line\":21,\"class\":\"yii\\\\base\\\\Application\",\"method\":\"run\",\"args\":null}]', '2020-03-04 18:50:58', '2020-03-04 18:50:58', '4eb49ea2-c9cd-4a69-8f07-5584d717cf9f'),
(4, 'ElementQuery::getIterator()', 'C:\\xampp\\htdocs\\CraftCMS\\craft\\templates\\index.twig:287', '2020-03-04 19:27:43', 'C:\\xampp\\htdocs\\CraftCMS\\craft\\templates\\index.twig', 287, 'Looping through element queries directly has been deprecated. Use the all() function to fetch the query results before looping over them.', '[{\"objectClass\":\"craft\\\\services\\\\Deprecator\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\elements\\\\db\\\\ElementQuery.php\",\"line\":558,\"class\":\"craft\\\\services\\\\Deprecator\",\"method\":\"log\",\"args\":\"\\\"ElementQuery::getIterator()\\\", \\\"Looping through element queries directly has been deprecated. Us...\\\"\"},{\"objectClass\":\"craft\\\\elements\\\\db\\\\MatrixBlockQuery\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\storage\\\\runtime\\\\compiled_templates\\\\f6\\\\f69414bc3fcd41b5b054062e45e1117026915a04b4a3ee31e45d09615ef5cca0.php\",\"line\":347,\"class\":\"craft\\\\elements\\\\db\\\\ElementQuery\",\"method\":\"getIterator\",\"args\":null},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":407,\"class\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":380,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":392,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\TemplateWrapper.php\",\"line\":45,\"class\":\"Twig\\\\Template\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], []\"},{\"objectClass\":\"Twig\\\\TemplateWrapper\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Environment.php\",\"line\":318,\"class\":\"Twig\\\\TemplateWrapper\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\twig\\\\Environment\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\View.php\",\"line\":397,\"class\":\"Twig\\\\Environment\",\"method\":\"render\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\View.php\",\"line\":458,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Controller.php\",\"line\":235,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderPageTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], \\\"site\\\"\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\controllers\\\\TemplatesController.php\",\"line\":98,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"renderTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":null,\"line\":null,\"class\":\"craft\\\\controllers\\\\TemplatesController\",\"method\":\"actionRender\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":null,\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\InlineAction.php\",\"line\":57,\"class\":null,\"method\":\"call_user_func_array\",\"args\":\"[craft\\\\controllers\\\\TemplatesController, \\\"actionRender\\\"], [\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"yii\\\\base\\\\InlineAction\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Controller.php\",\"line\":157,\"class\":\"yii\\\\base\\\\InlineAction\",\"method\":\"runWithParams\",\"args\":\"[\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Controller.php\",\"line\":178,\"class\":\"yii\\\\base\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Module.php\",\"line\":528,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Application.php\",\"line\":291,\"class\":\"yii\\\\base\\\\Module\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\web\\\\Application.php\",\"line\":103,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Application.php\",\"line\":276,\"class\":\"yii\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Application.php\",\"line\":386,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\web\\\\index.php\",\"line\":21,\"class\":\"yii\\\\base\\\\Application\",\"method\":\"run\",\"args\":null}]', '2020-03-04 19:27:43', '2020-03-04 19:27:43', '8ba71990-8bd7-46e5-9b8c-f30724ff786a'),
(25, 'ElementQuery::getIterator()', 'C:\\xampp\\htdocs\\CraftCMS\\craft\\templates\\index.twig:283', '2020-03-04 20:31:56', 'C:\\xampp\\htdocs\\CraftCMS\\craft\\templates\\index.twig', 283, 'Looping through element queries directly has been deprecated. Use the all() function to fetch the query results before looping over them.', '[{\"objectClass\":\"craft\\\\services\\\\Deprecator\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\elements\\\\db\\\\ElementQuery.php\",\"line\":558,\"class\":\"craft\\\\services\\\\Deprecator\",\"method\":\"log\",\"args\":\"\\\"ElementQuery::getIterator()\\\", \\\"Looping through element queries directly has been deprecated. Us...\\\"\"},{\"objectClass\":\"craft\\\\elements\\\\db\\\\MatrixBlockQuery\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\storage\\\\runtime\\\\compiled_templates\\\\f6\\\\f69414bc3fcd41b5b054062e45e1117026915a04b4a3ee31e45d09615ef5cca0.php\",\"line\":346,\"class\":\"craft\\\\elements\\\\db\\\\ElementQuery\",\"method\":\"getIterator\",\"args\":null},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":407,\"class\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":380,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":392,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\TemplateWrapper.php\",\"line\":45,\"class\":\"Twig\\\\Template\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], []\"},{\"objectClass\":\"Twig\\\\TemplateWrapper\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Environment.php\",\"line\":318,\"class\":\"Twig\\\\TemplateWrapper\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\twig\\\\Environment\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\View.php\",\"line\":397,\"class\":\"Twig\\\\Environment\",\"method\":\"render\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\View.php\",\"line\":458,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Controller.php\",\"line\":235,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderPageTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], \\\"site\\\"\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\controllers\\\\TemplatesController.php\",\"line\":98,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"renderTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":null,\"line\":null,\"class\":\"craft\\\\controllers\\\\TemplatesController\",\"method\":\"actionRender\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":null,\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\InlineAction.php\",\"line\":57,\"class\":null,\"method\":\"call_user_func_array\",\"args\":\"[craft\\\\controllers\\\\TemplatesController, \\\"actionRender\\\"], [\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"yii\\\\base\\\\InlineAction\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Controller.php\",\"line\":157,\"class\":\"yii\\\\base\\\\InlineAction\",\"method\":\"runWithParams\",\"args\":\"[\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Controller.php\",\"line\":178,\"class\":\"yii\\\\base\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Module.php\",\"line\":528,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Application.php\",\"line\":291,\"class\":\"yii\\\\base\\\\Module\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\web\\\\Application.php\",\"line\":103,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Application.php\",\"line\":276,\"class\":\"yii\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Application.php\",\"line\":386,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\web\\\\index.php\",\"line\":21,\"class\":\"yii\\\\base\\\\Application\",\"method\":\"run\",\"args\":null}]', '2020-03-04 20:31:56', '2020-03-04 20:31:56', '901a3340-545c-4d0d-abd4-c5a06190e440'),
(36, 'ElementQuery::first()', 'C:\\xampp\\htdocs\\CraftCMS\\craft\\templates\\_modules\\defaultdesign.twig:1', '2020-03-04 20:31:57', 'C:\\xampp\\htdocs\\CraftCMS\\craft\\templates\\_modules\\defaultdesign.twig', 1, 'The first() function used to query for elements is now deprecated. Use one() instead.', '[{\"objectClass\":\"craft\\\\services\\\\Deprecator\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\elements\\\\db\\\\ElementQuery.php\",\"line\":1845,\"class\":\"craft\\\\services\\\\Deprecator\",\"method\":\"log\",\"args\":\"\\\"ElementQuery::first()\\\", \\\"The first() function used to query for elements is now deprecate...\\\"\"},{\"objectClass\":\"craft\\\\elements\\\\db\\\\AssetQuery\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Extension\\\\CoreExtension.php\",\"line\":1499,\"class\":\"craft\\\\elements\\\\db\\\\ElementQuery\",\"method\":\"first\",\"args\":null},{\"objectClass\":null,\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\helpers\\\\Template.php\",\"line\":96,\"class\":null,\"method\":\"twig_get_attribute\",\"args\":\"craft\\\\web\\\\twig\\\\Environment, Twig\\\\Source, craft\\\\elements\\\\db\\\\AssetQuery, \\\"first\\\", ...\"},{\"objectClass\":null,\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\storage\\\\runtime\\\\compiled_templates\\\\66\\\\66e6956f1d47615ec795f64d837718ea87879d0cbe4f00a392692217ac420331.php\",\"line\":38,\"class\":\"craft\\\\helpers\\\\Template\",\"method\":\"attribute\",\"args\":\"craft\\\\web\\\\twig\\\\Environment, Twig\\\\Source, craft\\\\elements\\\\db\\\\AssetQuery, \\\"first\\\", ...\"},{\"objectClass\":\"__TwigTemplate_2fd30bc1d052a812a1d88197dc2294e99f2dda0b5faf5445165f20b5bf507869\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":407,\"class\":\"__TwigTemplate_2fd30bc1d052a812a1d88197dc2294e99f2dda0b5faf5445165f20b5bf507869\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_2fd30bc1d052a812a1d88197dc2294e99f2dda0b5faf5445165f20b5bf507869\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":380,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_2fd30bc1d052a812a1d88197dc2294e99f2dda0b5faf5445165f20b5bf507869\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\storage\\\\runtime\\\\compiled_templates\\\\f6\\\\f69414bc3fcd41b5b054062e45e1117026915a04b4a3ee31e45d09615ef5cca0.php\",\"line\":351,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...]\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":407,\"class\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":380,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":392,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\TemplateWrapper.php\",\"line\":45,\"class\":\"Twig\\\\Template\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], []\"},{\"objectClass\":\"Twig\\\\TemplateWrapper\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Environment.php\",\"line\":318,\"class\":\"Twig\\\\TemplateWrapper\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\twig\\\\Environment\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\View.php\",\"line\":397,\"class\":\"Twig\\\\Environment\",\"method\":\"render\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\View.php\",\"line\":458,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Controller.php\",\"line\":235,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderPageTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], \\\"site\\\"\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\controllers\\\\TemplatesController.php\",\"line\":98,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"renderTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":null,\"line\":null,\"class\":\"craft\\\\controllers\\\\TemplatesController\",\"method\":\"actionRender\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":null,\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\InlineAction.php\",\"line\":57,\"class\":null,\"method\":\"call_user_func_array\",\"args\":\"[craft\\\\controllers\\\\TemplatesController, \\\"actionRender\\\"], [\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"yii\\\\base\\\\InlineAction\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Controller.php\",\"line\":157,\"class\":\"yii\\\\base\\\\InlineAction\",\"method\":\"runWithParams\",\"args\":\"[\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Controller.php\",\"line\":178,\"class\":\"yii\\\\base\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Module.php\",\"line\":528,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Application.php\",\"line\":291,\"class\":\"yii\\\\base\\\\Module\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\web\\\\Application.php\",\"line\":103,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Application.php\",\"line\":276,\"class\":\"yii\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Application.php\",\"line\":386,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\web\\\\index.php\",\"line\":21,\"class\":\"yii\\\\base\\\\Application\",\"method\":\"run\",\"args\":null}]', '2020-03-04 20:31:57', '2020-03-04 20:31:57', '47022a9d-6c68-4a25-9949-8f3be27733cf');
INSERT INTO `deprecationerrors` (`id`, `key`, `fingerprint`, `lastOccurrence`, `file`, `line`, `message`, `traces`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(57, 'ElementQuery::first()', 'C:\\xampp\\htdocs\\CraftCMS\\craft\\templates\\_modules\\defaultdesign.twig:5', '2020-03-04 20:04:55', 'C:\\xampp\\htdocs\\CraftCMS\\craft\\templates\\_modules\\defaultdesign.twig', 5, 'The first() function used to query for elements is now deprecated. Use one() instead.', '[{\"objectClass\":\"craft\\\\services\\\\Deprecator\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\elements\\\\db\\\\ElementQuery.php\",\"line\":1845,\"class\":\"craft\\\\services\\\\Deprecator\",\"method\":\"log\",\"args\":\"\\\"ElementQuery::first()\\\", \\\"The first() function used to query for elements is now deprecate...\\\"\"},{\"objectClass\":\"craft\\\\elements\\\\db\\\\AssetQuery\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Extension\\\\CoreExtension.php\",\"line\":1499,\"class\":\"craft\\\\elements\\\\db\\\\ElementQuery\",\"method\":\"first\",\"args\":null},{\"objectClass\":null,\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\helpers\\\\Template.php\",\"line\":96,\"class\":null,\"method\":\"twig_get_attribute\",\"args\":\"craft\\\\web\\\\twig\\\\Environment, Twig\\\\Source, craft\\\\elements\\\\db\\\\AssetQuery, \\\"first\\\", ...\"},{\"objectClass\":null,\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\storage\\\\runtime\\\\compiled_templates\\\\66\\\\66e6956f1d47615ec795f64d837718ea87879d0cbe4f00a392692217ac420331.php\",\"line\":45,\"class\":\"craft\\\\helpers\\\\Template\",\"method\":\"attribute\",\"args\":\"craft\\\\web\\\\twig\\\\Environment, Twig\\\\Source, craft\\\\elements\\\\db\\\\AssetQuery, \\\"first\\\", ...\"},{\"objectClass\":\"__TwigTemplate_2fd30bc1d052a812a1d88197dc2294e99f2dda0b5faf5445165f20b5bf507869\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":407,\"class\":\"__TwigTemplate_2fd30bc1d052a812a1d88197dc2294e99f2dda0b5faf5445165f20b5bf507869\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_2fd30bc1d052a812a1d88197dc2294e99f2dda0b5faf5445165f20b5bf507869\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":380,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_2fd30bc1d052a812a1d88197dc2294e99f2dda0b5faf5445165f20b5bf507869\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\storage\\\\runtime\\\\compiled_templates\\\\f6\\\\f69414bc3fcd41b5b054062e45e1117026915a04b4a3ee31e45d09615ef5cca0.php\",\"line\":348,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...]\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":407,\"class\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":380,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":392,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\TemplateWrapper.php\",\"line\":45,\"class\":\"Twig\\\\Template\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], []\"},{\"objectClass\":\"Twig\\\\TemplateWrapper\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Environment.php\",\"line\":318,\"class\":\"Twig\\\\TemplateWrapper\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\twig\\\\Environment\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\View.php\",\"line\":397,\"class\":\"Twig\\\\Environment\",\"method\":\"render\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\View.php\",\"line\":458,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Controller.php\",\"line\":235,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderPageTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], \\\"site\\\"\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\controllers\\\\TemplatesController.php\",\"line\":98,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"renderTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":null,\"line\":null,\"class\":\"craft\\\\controllers\\\\TemplatesController\",\"method\":\"actionRender\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":null,\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\InlineAction.php\",\"line\":57,\"class\":null,\"method\":\"call_user_func_array\",\"args\":\"[craft\\\\controllers\\\\TemplatesController, \\\"actionRender\\\"], [\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"yii\\\\base\\\\InlineAction\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Controller.php\",\"line\":157,\"class\":\"yii\\\\base\\\\InlineAction\",\"method\":\"runWithParams\",\"args\":\"[\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Controller.php\",\"line\":178,\"class\":\"yii\\\\base\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Module.php\",\"line\":528,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Application.php\",\"line\":291,\"class\":\"yii\\\\base\\\\Module\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\web\\\\Application.php\",\"line\":103,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Application.php\",\"line\":276,\"class\":\"yii\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Application.php\",\"line\":386,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\web\\\\index.php\",\"line\":21,\"class\":\"yii\\\\base\\\\Application\",\"method\":\"run\",\"args\":null}]', '2020-03-04 20:04:55', '2020-03-04 20:04:55', 'aea6680c-c37c-4e42-94c0-8ccfd6835484'),
(115, 'ElementQuery::first()', 'C:\\xampp\\htdocs\\CraftCMS\\craft\\templates\\_modules\\secondarydesign.twig:1', '2020-03-04 20:31:57', 'C:\\xampp\\htdocs\\CraftCMS\\craft\\templates\\_modules\\secondarydesign.twig', 1, 'The first() function used to query for elements is now deprecated. Use one() instead.', '[{\"objectClass\":\"craft\\\\services\\\\Deprecator\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\elements\\\\db\\\\ElementQuery.php\",\"line\":1845,\"class\":\"craft\\\\services\\\\Deprecator\",\"method\":\"log\",\"args\":\"\\\"ElementQuery::first()\\\", \\\"The first() function used to query for elements is now deprecate...\\\"\"},{\"objectClass\":\"craft\\\\elements\\\\db\\\\AssetQuery\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Extension\\\\CoreExtension.php\",\"line\":1499,\"class\":\"craft\\\\elements\\\\db\\\\ElementQuery\",\"method\":\"first\",\"args\":null},{\"objectClass\":null,\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\helpers\\\\Template.php\",\"line\":96,\"class\":null,\"method\":\"twig_get_attribute\",\"args\":\"craft\\\\web\\\\twig\\\\Environment, Twig\\\\Source, craft\\\\elements\\\\db\\\\AssetQuery, \\\"first\\\", ...\"},{\"objectClass\":null,\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\storage\\\\runtime\\\\compiled_templates\\\\cd\\\\cd3f041c89f6fd2c2a21a14df47d7f2612fc95018b081cd91b755d5a4fcdc5b1.php\",\"line\":38,\"class\":\"craft\\\\helpers\\\\Template\",\"method\":\"attribute\",\"args\":\"craft\\\\web\\\\twig\\\\Environment, Twig\\\\Source, craft\\\\elements\\\\db\\\\AssetQuery, \\\"first\\\", ...\"},{\"objectClass\":\"__TwigTemplate_461c2af1d699a5673ee4000dd2f80fdd94bd31bfaa4ffdeb7a639a938e741b02\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":407,\"class\":\"__TwigTemplate_461c2af1d699a5673ee4000dd2f80fdd94bd31bfaa4ffdeb7a639a938e741b02\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_461c2af1d699a5673ee4000dd2f80fdd94bd31bfaa4ffdeb7a639a938e741b02\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":380,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_461c2af1d699a5673ee4000dd2f80fdd94bd31bfaa4ffdeb7a639a938e741b02\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\storage\\\\runtime\\\\compiled_templates\\\\f6\\\\f69414bc3fcd41b5b054062e45e1117026915a04b4a3ee31e45d09615ef5cca0.php\",\"line\":351,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...]\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":407,\"class\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":380,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Template.php\",\"line\":392,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"__TwigTemplate_6226502e9191c1e2771cac0baee31140439eb4d5f51bb5ce924205313ad8bcf5\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\TemplateWrapper.php\",\"line\":45,\"class\":\"Twig\\\\Template\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], []\"},{\"objectClass\":\"Twig\\\\TemplateWrapper\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\twig\\\\twig\\\\src\\\\Environment.php\",\"line\":318,\"class\":\"Twig\\\\TemplateWrapper\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\twig\\\\Environment\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\View.php\",\"line\":397,\"class\":\"Twig\\\\Environment\",\"method\":\"render\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\View.php\",\"line\":458,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Controller.php\",\"line\":235,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderPageTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], \\\"site\\\"\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\controllers\\\\TemplatesController.php\",\"line\":98,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"renderTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":null,\"line\":null,\"class\":\"craft\\\\controllers\\\\TemplatesController\",\"method\":\"actionRender\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":null,\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\InlineAction.php\",\"line\":57,\"class\":null,\"method\":\"call_user_func_array\",\"args\":\"[craft\\\\controllers\\\\TemplatesController, \\\"actionRender\\\"], [\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"yii\\\\base\\\\InlineAction\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Controller.php\",\"line\":157,\"class\":\"yii\\\\base\\\\InlineAction\",\"method\":\"runWithParams\",\"args\":\"[\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Controller.php\",\"line\":178,\"class\":\"yii\\\\base\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Module.php\",\"line\":528,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Application.php\",\"line\":291,\"class\":\"yii\\\\base\\\\Module\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\web\\\\Application.php\",\"line\":103,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\craftcms\\\\cms\\\\src\\\\web\\\\Application.php\",\"line\":276,\"class\":\"yii\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\vendor\\\\yiisoft\\\\yii2\\\\base\\\\Application.php\",\"line\":386,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"C:\\\\xampp\\\\htdocs\\\\CraftCMS\\\\craft\\\\web\\\\index.php\",\"line\":21,\"class\":\"yii\\\\base\\\\Application\",\"method\":\"run\",\"args\":null}]', '2020-03-04 20:31:57', '2020-03-04 20:31:57', '575d892c-f34b-4200-bbaf-dc6f12bdb8df');

-- --------------------------------------------------------

--
-- Table structure for table `drafts`
--

CREATE TABLE `drafts` (
  `id` int(11) NOT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `notes` text,
  `trackChanges` tinyint(1) NOT NULL DEFAULT '0',
  `dateLastMerged` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `elementindexsettings`
--

CREATE TABLE `elementindexsettings` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `elements`
--

CREATE TABLE `elements` (
  `id` int(11) NOT NULL,
  `draftId` int(11) DEFAULT NULL,
  `revisionId` int(11) DEFAULT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `elements`
--

INSERT INTO `elements` (`id`, `draftId`, `revisionId`, `fieldLayoutId`, `type`, `enabled`, `archived`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, NULL, NULL, NULL, 'craft\\elements\\User', 1, 0, '2020-03-04 09:23:46', '2020-03-04 09:23:46', NULL, 'f75f7f8f-4bea-4291-b541-557ca34b652e'),
(2, NULL, NULL, 4, 'craft\\elements\\Entry', 1, 0, '2020-03-04 18:32:28', '2020-03-04 18:32:36', '2020-03-04 18:34:01', '2f056d4b-0177-4363-bed2-1a317389ff40'),
(3, NULL, 1, NULL, 'craft\\elements\\Entry', 1, 0, '2020-03-04 18:32:28', '2020-03-04 18:32:28', '2020-03-04 18:34:01', '1658730a-556a-4ec9-b797-3ad249ee9be1'),
(4, NULL, 2, 4, 'craft\\elements\\Entry', 1, 0, '2020-03-04 18:32:36', '2020-03-04 18:32:36', '2020-03-04 18:34:01', 'd950130e-d8e0-463b-bdd0-9e1d9ec05b69'),
(5, NULL, NULL, 8, 'craft\\elements\\Entry', 1, 0, '2020-03-04 18:40:40', '2020-03-04 18:44:39', '2020-03-04 18:57:27', '0c300085-6654-4782-b5f1-eae6624d7eb9'),
(6, NULL, 3, NULL, 'craft\\elements\\Entry', 1, 0, '2020-03-04 18:40:40', '2020-03-04 18:40:40', '2020-03-04 18:57:27', '2955d8ef-7e76-4e6a-98c7-78b757c09431'),
(7, NULL, 4, 8, 'craft\\elements\\Entry', 1, 0, '2020-03-04 18:41:06', '2020-03-04 18:41:06', '2020-03-04 18:57:27', '479685fc-6de8-4df1-b03e-69c8d2500d3f'),
(8, NULL, NULL, 5, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 18:42:26', '2020-03-04 18:42:26', '2020-03-04 18:44:39', '53c6cd57-8ce1-4da6-83bd-f25440c1464a'),
(9, NULL, NULL, 6, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 18:42:26', '2020-03-04 18:42:26', '2020-03-04 18:44:39', '9dcb6a30-533e-4816-bbbe-bb8050252b80'),
(10, NULL, NULL, 5, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 18:42:26', '2020-03-04 18:42:26', '2020-03-04 18:44:39', 'b5750fdd-160d-4cd4-9534-ec17014248c4'),
(11, NULL, NULL, 7, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 18:42:26', '2020-03-04 18:42:26', '2020-03-04 18:44:39', '2e833b70-6e56-4d87-b51f-dc262a218733'),
(12, NULL, 5, 8, 'craft\\elements\\Entry', 1, 0, '2020-03-04 18:42:26', '2020-03-04 18:42:26', '2020-03-04 18:57:27', 'e0df9e26-fbe6-4291-b010-9970d4144641'),
(13, NULL, NULL, 5, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 18:42:26', '2020-03-04 18:42:26', NULL, '986f096f-3dc6-43d1-b82c-6f39b88e9be5'),
(14, NULL, NULL, 6, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 18:42:26', '2020-03-04 18:42:26', NULL, 'b4275f72-40f7-450a-aa9e-492cf80aff21'),
(15, NULL, NULL, 5, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 18:42:26', '2020-03-04 18:42:26', NULL, 'dc54dbf1-d848-4a69-a13f-beaa706a34b4'),
(16, NULL, NULL, 7, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 18:42:26', '2020-03-04 18:42:26', NULL, '44eac112-bc03-400a-92b7-ec608d7d6cc2'),
(17, NULL, NULL, NULL, 'craft\\elements\\Asset', 1, 0, '2020-03-04 18:44:05', '2020-03-04 18:44:05', '2020-03-04 20:01:39', 'b83aac04-5706-4b47-80c6-2f10b8193418'),
(24, NULL, NULL, 5, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 18:44:39', '2020-03-04 18:42:26', '2020-03-04 18:57:20', 'edb8b56c-61dc-47c1-a6aa-c224d09734de'),
(25, NULL, NULL, 6, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 18:44:39', '2020-03-04 18:42:26', '2020-03-04 18:57:20', '02a68c52-1d24-4942-99e9-bd6565916ee0'),
(26, NULL, NULL, 5, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 18:44:39', '2020-03-04 18:42:26', '2020-03-04 18:57:20', 'f9de700c-dee7-417e-8ca2-8816d566bbf3'),
(27, NULL, NULL, 7, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 18:44:39', '2020-03-04 18:44:35', '2020-03-04 18:57:20', 'd9c8b92c-bca1-41ad-a2d3-6c49e3c605c7'),
(28, NULL, NULL, 5, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 18:44:39', '2020-03-04 18:44:35', '2020-03-04 18:57:20', '8c4ee858-f3ac-4c94-97e3-b3b342dd1870'),
(29, NULL, 6, 8, 'craft\\elements\\Entry', 1, 0, '2020-03-04 18:44:39', '2020-03-04 18:44:39', '2020-03-04 18:57:27', '83aaa9e7-4f19-4101-82c3-d6b2c730371f'),
(30, NULL, NULL, 5, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 18:44:39', '2020-03-04 18:42:26', NULL, '035ecc64-eef6-4066-a85f-053b3d3af5e0'),
(31, NULL, NULL, 6, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 18:44:39', '2020-03-04 18:42:26', NULL, '55ae82fe-029c-4dcb-9726-888703231143'),
(32, NULL, NULL, 5, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 18:44:39', '2020-03-04 18:42:26', NULL, '76c62308-fb59-4feb-b6c9-8c6de814eaf8'),
(33, NULL, NULL, 7, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 18:44:39', '2020-03-04 18:44:35', NULL, 'b6c1c4de-68c0-4b7f-8e49-b71bdbf1e89a'),
(34, NULL, NULL, 5, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 18:44:39', '2020-03-04 18:44:35', NULL, '879c55a6-2631-41be-8a91-4cb1dc31b591'),
(35, NULL, NULL, 11, 'craft\\elements\\Entry', 1, 0, '2020-03-04 19:02:23', '2020-03-04 20:18:50', NULL, '53ffcbd4-2e3d-4350-b915-65c57091a3ca'),
(36, NULL, 7, NULL, 'craft\\elements\\Entry', 1, 0, '2020-03-04 19:02:23', '2020-03-04 19:02:23', NULL, '3c75c47c-aecc-4f55-8b73-4100d94c3023'),
(37, NULL, 8, 11, 'craft\\elements\\Entry', 1, 0, '2020-03-04 19:04:17', '2020-03-04 19:04:17', NULL, '9d9e4882-909d-4c5d-81d4-0d3a1c747f5e'),
(38, NULL, NULL, 9, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 19:07:42', '2020-03-04 20:10:40', NULL, 'd4699d1a-7c1c-4a4d-9c6f-696069bf2732'),
(39, NULL, NULL, 10, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 19:07:42', '2020-03-04 19:07:42', '2020-03-04 19:15:23', '7e18b21b-56a1-4d16-9f65-e8f543745792'),
(40, NULL, 9, 11, 'craft\\elements\\Entry', 1, 0, '2020-03-04 19:07:42', '2020-03-04 19:07:42', NULL, '45914c96-bbdb-4dfe-ae78-5ab013aca2ce'),
(41, NULL, NULL, 9, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 19:07:42', '2020-03-04 19:07:42', NULL, '2b675815-8555-46da-ac31-f013a38d019b'),
(42, NULL, NULL, 10, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 19:07:42', '2020-03-04 19:07:42', NULL, '96b794ae-ac13-4072-b498-3f05afcf28ea'),
(43, NULL, 10, 11, 'craft\\elements\\Entry', 1, 0, '2020-03-04 19:08:41', '2020-03-04 19:08:41', NULL, '2e664c49-c68b-4ac5-9305-96ba9db4375a'),
(44, NULL, NULL, 9, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 19:08:41', '2020-03-04 19:08:41', NULL, 'a834ca7e-ea3b-4c10-b4c5-4cf4e6d17604'),
(45, NULL, NULL, 10, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 19:08:41', '2020-03-04 19:07:42', NULL, '12be1f09-ea1d-4fcd-864e-15f507e598e9'),
(46, NULL, 11, 11, 'craft\\elements\\Entry', 1, 0, '2020-03-04 19:23:51', '2020-03-04 19:23:51', NULL, '13daf4a5-f33e-47d3-b332-2b2d663c23f5'),
(47, NULL, NULL, 9, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 19:23:51', '2020-03-04 19:23:51', NULL, 'cae76ce2-83b2-4de3-a0d8-e8169078933b'),
(48, NULL, 12, 11, 'craft\\elements\\Entry', 1, 0, '2020-03-04 19:24:32', '2020-03-04 19:24:32', NULL, 'a0c22c3f-4336-4eb3-9587-576edc8aa9f7'),
(49, NULL, NULL, 9, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 19:24:32', '2020-03-04 19:24:32', NULL, '14a88317-f458-4185-a071-f41dfd18703f'),
(50, NULL, 13, 11, 'craft\\elements\\Entry', 1, 0, '2020-03-04 19:31:45', '2020-03-04 19:31:45', NULL, '4a4a185c-79c8-4c4a-908b-19a6609f960a'),
(51, NULL, NULL, 9, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 19:31:45', '2020-03-04 19:31:45', NULL, '35ed35eb-8abf-45b2-9ef1-253c8fefb4d4'),
(52, NULL, NULL, NULL, 'craft\\elements\\Asset', 1, 0, '2020-03-04 19:35:36', '2020-03-04 19:35:36', '2020-03-04 20:01:39', '728b09b1-6d40-452c-b2db-63ed9f70ccf2'),
(53, NULL, 14, 11, 'craft\\elements\\Entry', 1, 0, '2020-03-04 19:35:52', '2020-03-04 19:35:52', NULL, 'b2e0c5e8-50ea-4021-8919-cbabd73e22af'),
(54, NULL, NULL, 9, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 19:35:52', '2020-03-04 19:35:52', NULL, '289526b4-6aed-4680-ae4b-5acc4452d02e'),
(55, NULL, NULL, NULL, 'craft\\elements\\Asset', 1, 0, '2020-03-04 19:39:31', '2020-03-04 19:39:31', '2020-03-04 20:02:25', '29f7e8fd-7934-453b-b8d8-d07c07770db8'),
(56, NULL, 15, 11, 'craft\\elements\\Entry', 1, 0, '2020-03-04 19:39:41', '2020-03-04 19:39:41', NULL, 'f2bd458b-c7c5-4462-ad26-1f3d54a14009'),
(57, NULL, NULL, 9, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 19:39:41', '2020-03-04 19:39:41', NULL, '57460367-1648-4a23-82ff-24f6df901aaf'),
(58, NULL, 16, 11, 'craft\\elements\\Entry', 1, 0, '2020-03-04 19:47:30', '2020-03-04 19:47:30', NULL, '7bea6764-bfe7-40c3-a64d-8f544cfb4026'),
(59, NULL, NULL, 9, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 19:47:30', '2020-03-04 19:47:30', NULL, '8f9e72a7-83d4-48de-b7de-cbc9d9f1b564'),
(60, NULL, NULL, NULL, 'craft\\elements\\Asset', 1, 0, '2020-03-04 19:54:50', '2020-03-04 19:54:50', '2020-03-04 20:02:25', '98ddb34e-d481-497d-b84a-dde0e009217b'),
(61, NULL, 17, 11, 'craft\\elements\\Entry', 1, 0, '2020-03-04 19:54:56', '2020-03-04 19:54:56', NULL, 'b8f99ce6-6a6b-4901-b7e9-3e36c0790197'),
(62, NULL, NULL, 9, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 19:54:56', '2020-03-04 19:54:56', NULL, '603d291c-51aa-4358-9500-f7b8ac346f48'),
(63, NULL, NULL, NULL, 'craft\\elements\\Asset', 1, 0, '2020-03-04 20:03:50', '2020-03-04 20:03:50', NULL, 'cf4aaeba-13fc-42c7-9541-1c738d0a9871'),
(64, NULL, 18, 11, 'craft\\elements\\Entry', 1, 0, '2020-03-04 20:03:56', '2020-03-04 20:03:56', NULL, '73652248-d6f9-4aba-a34c-16abdb5d080b'),
(65, NULL, NULL, 9, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 20:03:56', '2020-03-04 20:03:56', NULL, '6f5580e1-140d-47d5-a334-e184849851d8'),
(66, NULL, NULL, NULL, 'craft\\elements\\Asset', 1, 0, '2020-03-04 20:10:34', '2020-03-04 20:10:34', NULL, 'df8faca6-7d51-48a4-a6c1-36083c7026cc'),
(67, NULL, 19, 11, 'craft\\elements\\Entry', 1, 0, '2020-03-04 20:10:40', '2020-03-04 20:10:40', NULL, '3814d240-6fff-4ad4-9eb7-9d7317abeb97'),
(68, NULL, NULL, 9, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 20:10:40', '2020-03-04 20:10:40', NULL, 'da1ae421-0eba-437e-863f-3d3063bdf002'),
(69, NULL, NULL, 12, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 20:18:50', '2020-03-04 20:18:50', NULL, '938d72b1-b2a6-42d2-a4b8-537080c83b24'),
(70, NULL, 20, 11, 'craft\\elements\\Entry', 1, 0, '2020-03-04 20:18:50', '2020-03-04 20:18:50', NULL, '3e36c2e6-201a-498d-99da-087aee8df6b2'),
(71, NULL, NULL, 9, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 20:18:50', '2020-03-04 20:10:40', NULL, 'c6dcc0a0-a203-4bf3-a1a3-9ff9e31704b5'),
(72, NULL, NULL, 12, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 20:18:50', '2020-03-04 20:18:50', NULL, 'f994d922-8e76-47f9-b074-75114f2cf90d'),
(73, NULL, NULL, 13, 'craft\\elements\\Entry', 1, 0, '2020-03-04 20:19:53', '2020-03-04 20:24:25', NULL, '861d0596-e64a-427f-9aca-ccf65d65a7bb'),
(74, NULL, 21, NULL, 'craft\\elements\\Entry', 1, 0, '2020-03-04 20:19:53', '2020-03-04 20:19:53', NULL, '58bb66fe-8e96-4ac3-aa74-6285675eadd8'),
(75, NULL, 22, 13, 'craft\\elements\\Entry', 1, 0, '2020-03-04 20:20:10', '2020-03-04 20:20:10', NULL, '29acb048-67cf-49f0-afe8-f55f1bfac3e2'),
(76, NULL, NULL, NULL, 'craft\\elements\\Asset', 1, 0, '2020-03-04 20:21:02', '2020-03-04 20:21:02', NULL, '0e364e63-e343-480d-8ff0-66fd1626e90f'),
(77, NULL, NULL, NULL, 'craft\\elements\\Asset', 1, 0, '2020-03-04 20:22:13', '2020-03-04 20:22:13', NULL, '01a2dd25-edaa-4af1-ab1d-05c0cd458a6d'),
(78, NULL, NULL, 12, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 20:22:19', '2020-03-04 20:24:25', NULL, '31b8bf68-22ee-427d-84ae-63016d02d6d7'),
(79, NULL, NULL, 9, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 20:22:19', '2020-03-04 20:24:25', NULL, 'b2f82bef-1378-4f9b-be46-4d6453d647d4'),
(80, NULL, 23, 13, 'craft\\elements\\Entry', 1, 0, '2020-03-04 20:22:19', '2020-03-04 20:22:19', NULL, '7423cd98-daa7-426f-aa9d-a7bfa3c9033f'),
(81, NULL, NULL, 12, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 20:22:19', '2020-03-04 20:22:19', NULL, '19b3ec78-e107-4720-9615-07f7536ba21f'),
(82, NULL, NULL, 9, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 20:22:19', '2020-03-04 20:22:19', NULL, 'c728aaf4-e184-44d7-810a-2545ab806cd2'),
(83, NULL, 24, 13, 'craft\\elements\\Entry', 1, 0, '2020-03-04 20:23:23', '2020-03-04 20:23:23', NULL, '6d01bcd4-e806-456c-9bfe-e5a615605719'),
(84, NULL, NULL, 12, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 20:23:23', '2020-03-04 20:23:23', NULL, '8a35cf3d-3012-4a79-8aff-db5c51bef975'),
(85, NULL, NULL, 9, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 20:23:23', '2020-03-04 20:23:23', NULL, 'daa8693b-0509-4115-a32a-cea66d8bb7c9'),
(86, NULL, 25, 13, 'craft\\elements\\Entry', 1, 0, '2020-03-04 20:24:25', '2020-03-04 20:24:25', NULL, '4f94b644-4b04-4f34-a3ff-e913c19566ad'),
(87, NULL, NULL, 12, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 20:24:25', '2020-03-04 20:24:25', NULL, 'f2956b76-4e43-4f3d-ad56-2feb4bf61b07'),
(88, NULL, NULL, 9, 'craft\\elements\\MatrixBlock', 1, 0, '2020-03-04 20:24:25', '2020-03-04 20:24:25', NULL, 'a65768e1-968a-41be-ae09-377059dca8e7');

-- --------------------------------------------------------

--
-- Table structure for table `elements_sites`
--

CREATE TABLE `elements_sites` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `elements_sites`
--

INSERT INTO `elements_sites` (`id`, `elementId`, `siteId`, `slug`, `uri`, `enabled`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 1, NULL, NULL, 1, '2020-03-04 09:23:46', '2020-03-04 09:23:46', 'c2950012-35a3-4e2f-b717-a43156a7f3b6'),
(2, 2, 1, 'home', '__home__', 1, '2020-03-04 18:32:28', '2020-03-04 18:32:28', 'edda87b7-f6df-44f7-868a-025d7b401758'),
(3, 3, 1, 'home', '__home__', 1, '2020-03-04 18:32:28', '2020-03-04 18:32:28', '4803a2b4-105b-47a0-893b-bd4c6e19d66c'),
(4, 4, 1, 'home', '__home__', 1, '2020-03-04 18:32:36', '2020-03-04 18:32:36', '307cba2a-657e-47dc-b967-5300660e7ac0'),
(5, 5, 1, 'homepage', '__home__', 1, '2020-03-04 18:40:40', '2020-03-04 18:40:40', '23f37709-b702-4a6a-b92a-215be6d83ea5'),
(6, 6, 1, 'homepage', '__home__', 1, '2020-03-04 18:40:40', '2020-03-04 18:40:40', 'ae521fe7-2b96-4bbe-b0d5-4d88ece5aeb2'),
(7, 7, 1, 'homepage', '__home__', 1, '2020-03-04 18:41:06', '2020-03-04 18:41:06', '830a79d9-5b11-42b3-b8b4-1625c2e76201'),
(8, 8, 1, NULL, NULL, 1, '2020-03-04 18:42:26', '2020-03-04 18:42:26', 'd74cc2e0-3c7f-47ff-857c-e8de8ff81592'),
(9, 9, 1, NULL, NULL, 1, '2020-03-04 18:42:26', '2020-03-04 18:42:26', '764a6283-d548-45a2-b22b-3dd1e2b248d8'),
(10, 10, 1, NULL, NULL, 1, '2020-03-04 18:42:26', '2020-03-04 18:42:26', '8353e33c-e26b-43dc-bb69-90e4e9f89df9'),
(11, 11, 1, NULL, NULL, 1, '2020-03-04 18:42:26', '2020-03-04 18:42:26', 'cde16f43-842d-4d58-9668-4a3e15bec3c0'),
(12, 12, 1, 'homepage', '__home__', 1, '2020-03-04 18:42:26', '2020-03-04 18:42:26', '8a3a4475-8911-4a87-a174-13086821ec78'),
(13, 13, 1, NULL, NULL, 1, '2020-03-04 18:42:26', '2020-03-04 18:42:26', 'ed2d5675-bd99-408f-b167-9647db61cae3'),
(14, 14, 1, NULL, NULL, 1, '2020-03-04 18:42:26', '2020-03-04 18:42:26', '0e01ecfb-0852-40f9-bf33-e0c30c92a367'),
(15, 15, 1, NULL, NULL, 1, '2020-03-04 18:42:26', '2020-03-04 18:42:26', 'ad535c41-d23a-462d-8963-8999adae7e14'),
(16, 16, 1, NULL, NULL, 1, '2020-03-04 18:42:26', '2020-03-04 18:42:26', 'e2d946a8-398f-4e08-bcde-36ab8f2af9f3'),
(17, 17, 1, NULL, NULL, 1, '2020-03-04 18:44:05', '2020-03-04 18:44:05', '53d84f11-c858-4c7d-97b8-8417d80c762e'),
(24, 24, 1, NULL, NULL, 1, '2020-03-04 18:44:39', '2020-03-04 18:44:39', '2458175a-007e-423f-a37e-5e94b469c47f'),
(25, 25, 1, NULL, NULL, 1, '2020-03-04 18:44:39', '2020-03-04 18:44:39', 'ee80880a-ff35-495c-8d89-9797cc378e75'),
(26, 26, 1, NULL, NULL, 1, '2020-03-04 18:44:39', '2020-03-04 18:44:39', '4f70fcec-d213-4efd-985d-3b8ae991fb46'),
(27, 27, 1, NULL, NULL, 1, '2020-03-04 18:44:39', '2020-03-04 18:44:39', '283b78d1-9278-46ea-8dfc-060ff08ac9d1'),
(28, 28, 1, NULL, NULL, 1, '2020-03-04 18:44:39', '2020-03-04 18:44:39', '26500029-7a9c-42d5-aaa5-88af589e5a23'),
(29, 29, 1, 'homepage', '__home__', 1, '2020-03-04 18:44:39', '2020-03-04 18:44:39', 'eb4a2a73-6e46-4425-ac78-6b7f5111d9fd'),
(30, 30, 1, NULL, NULL, 1, '2020-03-04 18:44:39', '2020-03-04 18:44:39', '4eecb209-792f-4d9c-b104-098f7de66cb6'),
(31, 31, 1, NULL, NULL, 1, '2020-03-04 18:44:39', '2020-03-04 18:44:39', '4a284f0f-3801-4554-8b49-5ab1c9472081'),
(32, 32, 1, NULL, NULL, 1, '2020-03-04 18:44:39', '2020-03-04 18:44:39', '99cfbe5f-8a7b-4c79-897d-436c2fb0ff94'),
(33, 33, 1, NULL, NULL, 1, '2020-03-04 18:44:39', '2020-03-04 18:44:39', 'ddda074c-e74e-4788-bb33-81bb484eea34'),
(34, 34, 1, NULL, NULL, 1, '2020-03-04 18:44:39', '2020-03-04 18:44:39', '56d6ab6e-e58f-4f75-95a7-91c5b8b6e480'),
(35, 35, 1, 'homepage', '__home__', 1, '2020-03-04 19:02:23', '2020-03-04 19:02:23', '3a868e04-abd9-4634-8cb1-ad1092b308f1'),
(36, 36, 1, 'homepage', '__home__', 1, '2020-03-04 19:02:23', '2020-03-04 19:02:23', '3bcc74b0-e967-46e9-9926-b0ab8074ea94'),
(37, 37, 1, 'homepage', '__home__', 1, '2020-03-04 19:04:17', '2020-03-04 19:04:17', '6aa1e3cc-a27a-4f0f-9060-324b94d3609d'),
(38, 38, 1, NULL, NULL, 1, '2020-03-04 19:07:42', '2020-03-04 19:07:42', '512f752e-2cdf-4e10-a9b0-254a72f595a0'),
(39, 39, 1, NULL, NULL, 1, '2020-03-04 19:07:42', '2020-03-04 19:07:42', '47b423b6-ba14-4f2f-920c-29ba59a98bc3'),
(40, 40, 1, 'homepage', '__home__', 1, '2020-03-04 19:07:42', '2020-03-04 19:07:42', 'ddc108b5-d366-41f1-a990-c74f14bab765'),
(41, 41, 1, NULL, NULL, 1, '2020-03-04 19:07:42', '2020-03-04 19:07:42', 'a72abb2d-1286-4e34-addd-aa391230b290'),
(42, 42, 1, NULL, NULL, 1, '2020-03-04 19:07:42', '2020-03-04 19:07:42', '5705cc3b-91c8-474d-84d7-d84fcf0a4a18'),
(43, 43, 1, 'homepage', '__home__', 1, '2020-03-04 19:08:41', '2020-03-04 19:08:41', '9b7d95c6-6582-4305-8f67-78dd16dda815'),
(44, 44, 1, NULL, NULL, 1, '2020-03-04 19:08:41', '2020-03-04 19:08:41', '0e99db43-adb2-4566-8f78-cc1e13a07ae0'),
(45, 45, 1, NULL, NULL, 1, '2020-03-04 19:08:41', '2020-03-04 19:08:41', 'dce1f03d-bf71-4777-a7b7-a6eb39a98082'),
(46, 46, 1, 'homepage', '__home__', 1, '2020-03-04 19:23:51', '2020-03-04 19:23:51', 'eaee9ba4-2824-40bc-99b3-5c2cf2285765'),
(47, 47, 1, NULL, NULL, 1, '2020-03-04 19:23:51', '2020-03-04 19:23:51', '421849c7-8199-4d6f-bf0d-bdf2ff8862be'),
(48, 48, 1, 'homepage', '__home__', 1, '2020-03-04 19:24:32', '2020-03-04 19:24:32', '529b7a58-fd2c-4f85-9a79-3284d599e917'),
(49, 49, 1, NULL, NULL, 1, '2020-03-04 19:24:32', '2020-03-04 19:24:32', '1afe6467-eb83-4a57-a754-108ecb3eced6'),
(50, 50, 1, 'homepage', '__home__', 1, '2020-03-04 19:31:45', '2020-03-04 19:31:45', '93fcbd68-97a6-4218-a15b-2347591c01e5'),
(51, 51, 1, NULL, NULL, 1, '2020-03-04 19:31:45', '2020-03-04 19:31:45', '63f8b5ab-db2b-4aea-9219-9e852a14a038'),
(52, 52, 1, NULL, NULL, 1, '2020-03-04 19:35:36', '2020-03-04 19:35:36', 'db8020bf-1546-4e3d-890e-39b99af7eafe'),
(53, 53, 1, 'homepage', '__home__', 1, '2020-03-04 19:35:52', '2020-03-04 19:35:52', '6be3c1a0-6505-4002-af44-9da4e1ab15d3'),
(54, 54, 1, NULL, NULL, 1, '2020-03-04 19:35:52', '2020-03-04 19:35:52', '5872db5a-ac3c-4f71-a4e3-542318169bad'),
(55, 55, 1, NULL, NULL, 1, '2020-03-04 19:39:31', '2020-03-04 19:39:31', 'dc7131f0-997d-4f1a-8760-b42b7bb59f68'),
(56, 56, 1, 'homepage', '__home__', 1, '2020-03-04 19:39:41', '2020-03-04 19:39:41', '12c267f6-62e6-4e5c-b9c4-a86fe7557586'),
(57, 57, 1, NULL, NULL, 1, '2020-03-04 19:39:41', '2020-03-04 19:39:41', 'fcbbc00e-3652-4144-ac61-49c00887abb3'),
(58, 58, 1, 'homepage', '__home__', 1, '2020-03-04 19:47:30', '2020-03-04 19:47:30', '0c419aca-f677-4ec1-92bc-9b6f75db25e8'),
(59, 59, 1, NULL, NULL, 1, '2020-03-04 19:47:30', '2020-03-04 19:47:30', '0abe4b12-ca09-4c9d-ae5e-07233123de5f'),
(60, 60, 1, NULL, NULL, 1, '2020-03-04 19:54:50', '2020-03-04 19:54:50', 'aea18b8f-f914-4251-a4a8-f47c99b17733'),
(61, 61, 1, 'homepage', '__home__', 1, '2020-03-04 19:54:56', '2020-03-04 19:54:56', 'ca4215ab-5e99-4c94-8d19-8f2c869a3a67'),
(62, 62, 1, NULL, NULL, 1, '2020-03-04 19:54:56', '2020-03-04 19:54:56', 'dc5ec665-15ab-473d-aeb2-cb3ad65f130a'),
(63, 63, 1, NULL, NULL, 1, '2020-03-04 20:03:50', '2020-03-04 20:03:50', 'd640c1e7-26c3-4218-b366-ce79ec734adf'),
(64, 64, 1, 'homepage', '__home__', 1, '2020-03-04 20:03:56', '2020-03-04 20:03:56', 'bb68b9f9-9c0c-4973-96ed-f55dc59862c6'),
(65, 65, 1, NULL, NULL, 1, '2020-03-04 20:03:56', '2020-03-04 20:03:56', '3761feaf-8fdc-4521-9a02-0a02f78212db'),
(66, 66, 1, NULL, NULL, 1, '2020-03-04 20:10:34', '2020-03-04 20:10:34', '98abc6f5-24d8-4e7b-ad49-bcf423ec4f07'),
(67, 67, 1, 'homepage', '__home__', 1, '2020-03-04 20:10:40', '2020-03-04 20:10:40', '1d0b05b7-0a48-48a3-849f-3bfe5c5c742f'),
(68, 68, 1, NULL, NULL, 1, '2020-03-04 20:10:40', '2020-03-04 20:10:40', '0c5c9835-de41-4027-9bff-2524265fe0e1'),
(69, 69, 1, NULL, NULL, 1, '2020-03-04 20:18:50', '2020-03-04 20:18:50', '78d43252-c0ab-4af9-b046-3fe3983d01dd'),
(70, 70, 1, 'homepage', '__home__', 1, '2020-03-04 20:18:50', '2020-03-04 20:18:50', '05b7126c-b266-462c-a2fc-ead133c6f9a9'),
(71, 71, 1, NULL, NULL, 1, '2020-03-04 20:18:50', '2020-03-04 20:18:50', 'adb569f8-5862-4464-897b-03514fbe37b5'),
(72, 72, 1, NULL, NULL, 1, '2020-03-04 20:18:50', '2020-03-04 20:18:50', '7434811e-3069-447d-9d72-b8284a7570ef'),
(73, 73, 1, 'testpage', 'testpage', 1, '2020-03-04 20:19:53', '2020-03-04 20:19:53', 'dae0cde1-40ae-4d1c-82a2-b9df83d73410'),
(74, 74, 1, 'testpage', 'testpage', 1, '2020-03-04 20:19:53', '2020-03-04 20:19:53', '2e3e61a5-a67e-46f8-839c-28966b4550c5'),
(75, 75, 1, 'testpage', 'testpage', 1, '2020-03-04 20:20:10', '2020-03-04 20:20:10', '51ec489f-e77f-4d85-b03c-ba0fbf4427db'),
(76, 76, 1, NULL, NULL, 1, '2020-03-04 20:21:02', '2020-03-04 20:21:02', '6682b92b-9a44-4b8c-b073-eb40b4154231'),
(77, 77, 1, NULL, NULL, 1, '2020-03-04 20:22:13', '2020-03-04 20:22:13', 'f1471192-c57a-4c80-8d2e-17ec27dbe332'),
(78, 78, 1, NULL, NULL, 1, '2020-03-04 20:22:19', '2020-03-04 20:22:19', 'e736a6b1-ced0-4969-8223-c9f662fc2ccf'),
(79, 79, 1, NULL, NULL, 1, '2020-03-04 20:22:19', '2020-03-04 20:22:19', '99d821a2-06c5-4629-978a-da8fce668728'),
(80, 80, 1, 'testpage', 'testpage', 1, '2020-03-04 20:22:19', '2020-03-04 20:22:19', 'a86397a1-939f-4e94-a7e9-e731aeed905f'),
(81, 81, 1, NULL, NULL, 1, '2020-03-04 20:22:19', '2020-03-04 20:22:19', 'a99fa2d3-e872-471f-be19-0fdebb9d20a2'),
(82, 82, 1, NULL, NULL, 1, '2020-03-04 20:22:19', '2020-03-04 20:22:19', 'ab4f117a-2dbc-4d5b-9e6b-0fd2470d1b1e'),
(83, 83, 1, 'testpage', 'testpage', 1, '2020-03-04 20:23:23', '2020-03-04 20:23:23', 'dcb331ae-2dd4-4b5f-93b2-71fd1c5cdbad'),
(84, 84, 1, NULL, NULL, 1, '2020-03-04 20:23:23', '2020-03-04 20:23:23', '3ad29417-6ac1-43da-9530-9d96e560a3f3'),
(85, 85, 1, NULL, NULL, 1, '2020-03-04 20:23:23', '2020-03-04 20:23:23', '39ce3f5a-b310-4e99-b019-dab88cc01003'),
(86, 86, 1, 'testpage', 'testpage', 1, '2020-03-04 20:24:25', '2020-03-04 20:24:25', '9667ed69-0b73-4a6c-a707-4b4856a8d9a2'),
(87, 87, 1, NULL, NULL, 1, '2020-03-04 20:24:25', '2020-03-04 20:24:25', 'afb218de-6408-49dc-abae-d1fc4087595e'),
(88, 88, 1, NULL, NULL, 1, '2020-03-04 20:24:25', '2020-03-04 20:24:25', 'dd9481f2-1143-41a3-9965-73853800fb45');

-- --------------------------------------------------------

--
-- Table structure for table `entries`
--

CREATE TABLE `entries` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `typeId` int(11) NOT NULL,
  `authorId` int(11) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `deletedWithEntryType` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `entries`
--

INSERT INTO `entries` (`id`, `sectionId`, `parentId`, `typeId`, `authorId`, `postDate`, `expiryDate`, `deletedWithEntryType`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(2, 1, NULL, 1, NULL, '2020-03-04 18:32:00', NULL, 1, '2020-03-04 18:32:28', '2020-03-04 18:32:28', '9fe5608e-df76-4928-9e17-c0acfd05dd4b'),
(3, 1, NULL, 1, NULL, '2020-03-04 18:32:00', NULL, NULL, '2020-03-04 18:32:28', '2020-03-04 18:32:28', '8d2bc833-8649-4185-b901-e59db99a6261'),
(4, 1, NULL, 1, NULL, '2020-03-04 18:32:00', NULL, NULL, '2020-03-04 18:32:36', '2020-03-04 18:32:36', '7c7c025a-dc44-41dd-9072-1e7795db50e0'),
(5, 2, NULL, 2, NULL, '2020-03-04 18:40:00', NULL, 1, '2020-03-04 18:40:40', '2020-03-04 18:40:40', 'ac9bf8a0-15ee-4836-928c-5c3ea337b14d'),
(6, 2, NULL, 2, NULL, '2020-03-04 18:40:00', NULL, NULL, '2020-03-04 18:40:40', '2020-03-04 18:40:40', 'dff03cb9-3cfe-409d-a9d8-ccda7ac2bf19'),
(7, 2, NULL, 2, NULL, '2020-03-04 18:40:00', NULL, NULL, '2020-03-04 18:41:06', '2020-03-04 18:41:06', '69f8620a-1c7c-40d1-8ceb-029f80f7e49e'),
(12, 2, NULL, 2, NULL, '2020-03-04 18:40:00', NULL, NULL, '2020-03-04 18:42:26', '2020-03-04 18:42:26', 'ea5355e6-d499-483b-a721-239559e844b9'),
(29, 2, NULL, 2, NULL, '2020-03-04 18:40:00', NULL, NULL, '2020-03-04 18:44:39', '2020-03-04 18:44:39', 'c2d277a8-6ce0-4d8e-9397-f67051c20c38'),
(35, 3, NULL, 3, NULL, '2020-03-04 19:02:00', NULL, NULL, '2020-03-04 19:02:23', '2020-03-04 19:02:23', '204db703-7353-4eb0-86a3-0b7a1ea094fb'),
(36, 3, NULL, 3, NULL, '2020-03-04 19:02:00', NULL, NULL, '2020-03-04 19:02:23', '2020-03-04 19:02:23', '67318fe1-fae0-4ddb-9362-2712492bb64f'),
(37, 3, NULL, 3, NULL, '2020-03-04 19:02:00', NULL, NULL, '2020-03-04 19:04:17', '2020-03-04 19:04:17', '89b823ca-c7c9-4a3f-8bef-998cbb96d42c'),
(40, 3, NULL, 3, NULL, '2020-03-04 19:02:00', NULL, NULL, '2020-03-04 19:07:42', '2020-03-04 19:07:42', 'e30c74e4-14ae-4082-a524-d39f902deebe'),
(43, 3, NULL, 3, NULL, '2020-03-04 19:02:00', NULL, NULL, '2020-03-04 19:08:41', '2020-03-04 19:08:41', '3455e340-0bb1-4de0-a8d8-3f4e6f941f02'),
(46, 3, NULL, 3, NULL, '2020-03-04 19:02:00', NULL, NULL, '2020-03-04 19:23:51', '2020-03-04 19:23:51', 'c643dfc2-9830-426d-a338-27c83df4975f'),
(48, 3, NULL, 3, NULL, '2020-03-04 19:02:00', NULL, NULL, '2020-03-04 19:24:32', '2020-03-04 19:24:32', '5bba84f7-eefa-420d-8d6c-dc11aa6089f7'),
(50, 3, NULL, 3, NULL, '2020-03-04 19:02:00', NULL, NULL, '2020-03-04 19:31:45', '2020-03-04 19:31:45', 'ddb375b0-ffd0-4134-b296-0e9079afed74'),
(53, 3, NULL, 3, NULL, '2020-03-04 19:02:00', NULL, NULL, '2020-03-04 19:35:52', '2020-03-04 19:35:52', 'e6499d2a-ad7a-432e-ad67-dfaca9f4bddd'),
(56, 3, NULL, 3, NULL, '2020-03-04 19:02:00', NULL, NULL, '2020-03-04 19:39:41', '2020-03-04 19:39:41', '337e62ab-1372-41de-9961-3421c942b5c3'),
(58, 3, NULL, 3, NULL, '2020-03-04 19:02:00', NULL, NULL, '2020-03-04 19:47:30', '2020-03-04 19:47:30', '8414668f-f5ba-401b-8fd3-dabcae24109c'),
(61, 3, NULL, 3, NULL, '2020-03-04 19:02:00', NULL, NULL, '2020-03-04 19:54:56', '2020-03-04 19:54:56', 'a1d8ea0c-0ba2-4380-a9cd-c1e8ff77e78a'),
(64, 3, NULL, 3, NULL, '2020-03-04 19:02:00', NULL, NULL, '2020-03-04 20:03:56', '2020-03-04 20:03:56', '42a7b044-bc62-4769-928b-1be5fa5a55ad'),
(67, 3, NULL, 3, NULL, '2020-03-04 19:02:00', NULL, NULL, '2020-03-04 20:10:40', '2020-03-04 20:10:40', '90981403-2252-4a10-984b-7301d1e2b858'),
(70, 3, NULL, 3, NULL, '2020-03-04 19:02:00', NULL, NULL, '2020-03-04 20:18:50', '2020-03-04 20:18:50', '10591b0b-9167-4e60-9d1c-d5f8efbca7fd'),
(73, 4, NULL, 4, NULL, '2020-03-04 20:19:00', NULL, NULL, '2020-03-04 20:19:53', '2020-03-04 20:19:53', 'd00344be-bea4-4c07-a254-09db5b76016c'),
(74, 4, NULL, 4, NULL, '2020-03-04 20:19:00', NULL, NULL, '2020-03-04 20:19:53', '2020-03-04 20:19:53', '53fb710b-a079-4eba-bf8f-d1ff2281d848'),
(75, 4, NULL, 4, NULL, '2020-03-04 20:19:00', NULL, NULL, '2020-03-04 20:20:10', '2020-03-04 20:20:10', '2b453bb6-b800-42cc-a5f0-dace3d8ee337'),
(80, 4, NULL, 4, NULL, '2020-03-04 20:19:00', NULL, NULL, '2020-03-04 20:22:19', '2020-03-04 20:22:19', '05eac008-4f70-48a4-955e-6847f45ddca8'),
(83, 4, NULL, 4, NULL, '2020-03-04 20:19:00', NULL, NULL, '2020-03-04 20:23:23', '2020-03-04 20:23:23', '77d3c666-e557-4efb-aef2-410df0abde95'),
(86, 4, NULL, 4, NULL, '2020-03-04 20:19:00', NULL, NULL, '2020-03-04 20:24:25', '2020-03-04 20:24:25', '635ad7e3-f3f0-4714-be43-b7cdd7a2cf83');

-- --------------------------------------------------------

--
-- Table structure for table `entrytypes`
--

CREATE TABLE `entrytypes` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `hasTitleField` tinyint(1) NOT NULL DEFAULT '1',
  `titleLabel` varchar(255) DEFAULT 'Title',
  `titleFormat` varchar(255) DEFAULT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `entrytypes`
--

INSERT INTO `entrytypes` (`id`, `sectionId`, `fieldLayoutId`, `name`, `handle`, `hasTitleField`, `titleLabel`, `titleFormat`, `sortOrder`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 1, 4, 'Home', 'home', 0, '', '{section.name|raw}', 1, '2020-03-04 18:32:27', '2020-03-04 18:32:36', '2020-03-04 18:34:01', '187273f5-b86c-46d8-93d5-ca58540cfac2'),
(2, 2, 8, 'Homepage', 'homepage', 0, '', '{section.name|raw}', 1, '2020-03-04 18:40:40', '2020-03-04 18:41:06', '2020-03-04 18:57:27', 'aef6b9df-f615-4102-b2d9-7e220c6b6df9'),
(3, 3, 11, 'Homepage', 'homepage', 0, '', '{section.name|raw}', 1, '2020-03-04 19:02:23', '2020-03-04 19:04:17', NULL, '0d810b10-a6dd-4a5c-8f2d-832860cf5cc0'),
(4, 4, 13, 'TestPage', 'testpage', 0, '', '{section.name|raw}', 1, '2020-03-04 20:19:53', '2020-03-04 20:20:09', NULL, 'b77fddb6-e4e7-4ba5-bc16-b9afbbfd2597');

-- --------------------------------------------------------

--
-- Table structure for table `fieldgroups`
--

CREATE TABLE `fieldgroups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fieldgroups`
--

INSERT INTO `fieldgroups` (`id`, `name`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'Common', '2020-03-04 09:23:46', '2020-03-04 09:23:46', '071a28b4-3792-4ece-813d-89a2d7c01022');

-- --------------------------------------------------------

--
-- Table structure for table `fieldlayoutfields`
--

CREATE TABLE `fieldlayoutfields` (
  `id` int(11) NOT NULL,
  `layoutId` int(11) NOT NULL,
  `tabId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fieldlayoutfields`
--

INSERT INTO `fieldlayoutfields` (`id`, `layoutId`, `tabId`, `fieldId`, `required`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(15, 11, 15, 9, 0, 1, '2020-03-04 19:04:17', '2020-03-04 19:04:17', '7b179c1f-65c1-408d-8397-a3faf485f0e0'),
(52, 9, 33, 10, 0, 1, '2020-03-04 20:15:01', '2020-03-04 20:15:01', 'dff082be-3154-4fb7-a384-0ea451074317'),
(53, 9, 33, 13, 0, 2, '2020-03-04 20:15:02', '2020-03-04 20:15:02', '5e96ad89-0d03-41b5-9dc1-7f168c481638'),
(54, 9, 33, 15, 0, 3, '2020-03-04 20:15:02', '2020-03-04 20:15:02', '69bf834f-3b28-40ca-b355-7de757db6881'),
(55, 12, 34, 16, 0, 2, '2020-03-04 20:15:02', '2020-03-04 20:15:02', '850eb156-ca37-4d44-aa80-1a42a889d30e'),
(56, 12, 34, 17, 0, 3, '2020-03-04 20:15:02', '2020-03-04 20:15:02', 'bd88e767-cc12-4939-8b23-2c21a45ff797'),
(57, 12, 34, 18, 0, 1, '2020-03-04 20:15:02', '2020-03-04 20:15:02', '136d2cce-41ea-4f8b-bce8-be8917bf5649'),
(58, 13, 35, 9, 0, 1, '2020-03-04 20:20:09', '2020-03-04 20:20:09', '51f63587-4b3b-4703-bc9d-b3cbd0ae0337');

-- --------------------------------------------------------

--
-- Table structure for table `fieldlayouts`
--

CREATE TABLE `fieldlayouts` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fieldlayouts`
--

INSERT INTO `fieldlayouts` (`id`, `type`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 'craft\\elements\\MatrixBlock', '2020-03-04 18:31:01', '2020-03-04 18:31:01', '2020-03-04 18:33:52', 'b98a9e41-267e-4881-adb2-f19e812075a2'),
(2, 'craft\\elements\\MatrixBlock', '2020-03-04 18:32:03', '2020-03-04 18:32:03', '2020-03-04 18:33:53', '00630241-afcd-423b-b7b2-ba40c43f0e00'),
(3, 'craft\\elements\\MatrixBlock', '2020-03-04 18:32:03', '2020-03-04 18:32:03', '2020-03-04 18:33:53', '8934ef9e-253e-4790-8bc0-72d8b7919c2e'),
(4, 'craft\\elements\\Entry', '2020-03-04 18:32:36', '2020-03-04 18:32:36', '2020-03-04 18:34:01', 'fdfaf20b-3a33-4c1d-8893-474c04d973ff'),
(5, 'craft\\elements\\MatrixBlock', '2020-03-04 18:40:13', '2020-03-04 18:40:13', '2020-03-04 18:57:20', '9ba53e96-b8d7-40af-b3fe-2956453cf1b1'),
(6, 'craft\\elements\\MatrixBlock', '2020-03-04 18:40:13', '2020-03-04 18:40:13', '2020-03-04 18:57:20', 'fa457854-7413-4b17-9e35-942613da1ac7'),
(7, 'craft\\elements\\MatrixBlock', '2020-03-04 18:40:13', '2020-03-04 18:40:13', '2020-03-04 18:57:20', '45d98249-cde3-465b-b482-1dc07347d80d'),
(8, 'craft\\elements\\Entry', '2020-03-04 18:41:06', '2020-03-04 18:41:06', '2020-03-04 18:57:27', '2bf3d7ae-dc32-40be-9177-a8e96d0674d4'),
(9, 'craft\\elements\\MatrixBlock', '2020-03-04 19:03:57', '2020-03-04 19:03:57', NULL, '4525a308-dc8c-4ea6-954c-18880c00462d'),
(10, 'craft\\elements\\MatrixBlock', '2020-03-04 19:03:57', '2020-03-04 19:03:57', '2020-03-04 19:15:23', '721b368c-df7f-4882-a23d-669d8fdc3931'),
(11, 'craft\\elements\\Entry', '2020-03-04 19:04:17', '2020-03-04 19:04:17', NULL, '559900ea-3109-46a4-8e4c-7ddc33db6970'),
(12, 'craft\\elements\\MatrixBlock', '2020-03-04 20:15:02', '2020-03-04 20:15:02', NULL, 'ef542c16-16ac-4b88-8459-e3ce4af376fa'),
(13, 'craft\\elements\\Entry', '2020-03-04 20:20:09', '2020-03-04 20:20:09', NULL, '77e8d53f-6fbf-42ea-b4fc-04408468c5f5');

-- --------------------------------------------------------

--
-- Table structure for table `fieldlayouttabs`
--

CREATE TABLE `fieldlayouttabs` (
  `id` int(11) NOT NULL,
  `layoutId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fieldlayouttabs`
--

INSERT INTO `fieldlayouttabs` (`id`, `layoutId`, `name`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(2, 1, 'Content', 1, '2020-03-04 18:32:03', '2020-03-04 18:32:03', 'd2e6f178-c1f7-4b1b-939f-f418b4f34d1f'),
(3, 2, 'Content', 1, '2020-03-04 18:32:03', '2020-03-04 18:32:03', 'f4d907b4-85b0-4f12-8a85-42e90513d6bc'),
(4, 3, 'Content', 1, '2020-03-04 18:32:03', '2020-03-04 18:32:03', '7012fc23-6093-46b1-9065-8103112868fa'),
(5, 4, 'Common', 1, '2020-03-04 18:32:36', '2020-03-04 18:32:36', '690b7374-76fc-4260-a6d5-eb68d879d68f'),
(9, 8, 'Common', 1, '2020-03-04 18:41:06', '2020-03-04 18:41:06', '3d4c03cc-2c63-4273-80e9-91f7dc50a054'),
(10, 5, 'Content', 1, '2020-03-04 18:43:51', '2020-03-04 18:43:51', 'f9da9fa4-7958-4c7f-be84-d0116823d34a'),
(11, 6, 'Content', 1, '2020-03-04 18:43:51', '2020-03-04 18:43:51', '78976ccb-b724-46fc-a1c0-bf2938e6a5a0'),
(12, 7, 'Content', 1, '2020-03-04 18:43:52', '2020-03-04 18:43:52', 'cb0fee06-18bd-4689-abc5-dd11fc9b9af6'),
(15, 11, 'Common', 1, '2020-03-04 19:04:17', '2020-03-04 19:04:17', '88906ad7-47a0-4b61-913b-8d8b37ddc3d2'),
(19, 10, 'Content', 1, '2020-03-04 19:13:40', '2020-03-04 19:13:40', 'd347e0f1-4ab3-40fd-9705-7451c3498773'),
(33, 9, 'Content', 1, '2020-03-04 20:15:01', '2020-03-04 20:15:01', '651c66fe-7ab3-4eeb-ab64-6166dac1c129'),
(34, 12, 'Content', 1, '2020-03-04 20:15:02', '2020-03-04 20:15:02', 'dc0642f2-4bee-4d82-a3b0-9c9c5752db9e'),
(35, 13, 'Common', 1, '2020-03-04 20:20:09', '2020-03-04 20:20:09', 'f0a3fe72-baf0-44c2-a813-164618a9ef73');

-- --------------------------------------------------------

--
-- Table structure for table `fields`
--

CREATE TABLE `fields` (
  `id` int(11) NOT NULL,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(64) NOT NULL,
  `context` varchar(255) NOT NULL DEFAULT 'global',
  `instructions` text,
  `searchable` tinyint(1) NOT NULL DEFAULT '1',
  `translationMethod` varchar(255) NOT NULL DEFAULT 'none',
  `translationKeyFormat` text,
  `type` varchar(255) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fields`
--

INSERT INTO `fields` (`id`, `groupId`, `name`, `handle`, `context`, `instructions`, `searchable`, `translationMethod`, `translationKeyFormat`, `type`, `settings`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(9, 1, 'Modules', 'modules', 'global', '', 1, 'site', NULL, 'craft\\fields\\Matrix', '{\"contentTable\":\"{{%matrixcontent_modules}}\",\"maxBlocks\":\"\",\"minBlocks\":\"\",\"propagationMethod\":\"all\"}', '2020-03-04 19:03:56', '2020-03-04 19:03:56', 'a74dc681-f6a1-4f6b-9351-705706dc6382'),
(10, NULL, 'ImageTitle', 'imagetitle', 'matrixBlockType:e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9', '', 1, 'none', NULL, 'craft\\fields\\PlainText', '{\"byteLimit\":null,\"charLimit\":null,\"code\":\"\",\"columnType\":null,\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\"}', '2020-03-04 19:03:57', '2020-03-04 19:19:08', 'b35d435d-04fb-48fa-ae07-b14561903b91'),
(13, NULL, 'ImageText', 'imagetext', 'matrixBlockType:e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9', '', 1, 'none', NULL, 'froala\\craftfroalawysiwyg\\Field', '{\"assetsFilesSource\":\"folder:2\",\"assetsFilesSubPath\":\"\",\"assetsImagesSource\":\"folder:2\",\"assetsImagesSubPath\":\"\",\"editorConfig\":\"\"}', '2020-03-04 19:19:31', '2020-03-04 20:03:38', 'eba9d534-08de-41fa-8b06-d827343215ae'),
(15, NULL, 'Images', 'images', 'matrixBlockType:e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9', '', 1, 'site', NULL, 'craft\\fields\\Assets', '{\"allowedKinds\":null,\"defaultUploadLocationSource\":\"volume:6f33e3a2-877c-4050-b3ec-b0510164719c\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"1\",\"localizeRelations\":false,\"restrictFiles\":\"\",\"selectionLabel\":\"\",\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"singleUploadLocationSource\":\"volume:6f33e3a2-877c-4050-b3ec-b0510164719c\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":\"*\",\"targetSiteId\":null,\"useSingleFolder\":false,\"validateRelatedElements\":\"\",\"viewMode\":\"list\"}', '2020-03-04 20:03:38', '2020-03-04 20:03:38', 'f5896462-57d8-4023-a12f-a63ae565209a'),
(16, NULL, 'ImageText', 'imagetext', 'matrixBlockType:ef7681b1-796e-4622-9627-55bab78e30e7', '', 1, 'none', NULL, 'froala\\craftfroalawysiwyg\\Field', '{\"assetsFilesSource\":\"folder:2\",\"assetsFilesSubPath\":\"\",\"assetsImagesSource\":\"folder:2\",\"assetsImagesSubPath\":\"\",\"editorConfig\":\"\"}', '2020-03-04 20:15:02', '2020-03-04 20:15:02', '0550a20d-4cff-48bc-a99b-11f7d0c745a3'),
(17, NULL, 'Images', 'images', 'matrixBlockType:ef7681b1-796e-4622-9627-55bab78e30e7', '', 1, 'site', NULL, 'craft\\fields\\Assets', '{\"allowedKinds\":null,\"defaultUploadLocationSource\":\"volume:6f33e3a2-877c-4050-b3ec-b0510164719c\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"1\",\"localizeRelations\":false,\"restrictFiles\":\"\",\"selectionLabel\":\"\",\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"singleUploadLocationSource\":\"volume:6f33e3a2-877c-4050-b3ec-b0510164719c\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":\"*\",\"targetSiteId\":null,\"useSingleFolder\":false,\"validateRelatedElements\":\"\",\"viewMode\":\"list\"}', '2020-03-04 20:15:02', '2020-03-04 20:15:02', '7dfcd464-05e7-4173-8d20-36b21b4051a4'),
(18, NULL, 'ImageTitle', 'imagetitle', 'matrixBlockType:ef7681b1-796e-4622-9627-55bab78e30e7', '', 1, 'none', NULL, 'craft\\fields\\PlainText', '{\"byteLimit\":null,\"charLimit\":null,\"code\":\"\",\"columnType\":null,\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\"}', '2020-03-04 20:15:02', '2020-03-04 20:15:02', 'bd8ed2bd-2beb-411b-b757-a77d36940d8d');

-- --------------------------------------------------------

--
-- Table structure for table `globalsets`
--

CREATE TABLE `globalsets` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gqlschemas`
--

CREATE TABLE `gqlschemas` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `scope` text,
  `isPublic` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gqltokens`
--

CREATE TABLE `gqltokens` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `accessToken` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `expiryDate` datetime DEFAULT NULL,
  `lastUsed` datetime DEFAULT NULL,
  `schemaId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE `info` (
  `id` int(11) NOT NULL,
  `version` varchar(50) NOT NULL,
  `schemaVersion` varchar(15) NOT NULL,
  `maintenance` tinyint(1) NOT NULL DEFAULT '0',
  `configMap` mediumtext,
  `fieldVersion` char(12) NOT NULL DEFAULT '000000000000',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`id`, `version`, `schemaVersion`, `maintenance`, `configMap`, `fieldVersion`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, '3.4.9', '3.4.10', 0, '[]', '58fYO5jF4h1j', '2020-03-04 09:23:46', '2020-03-04 20:20:10', '3ecf323f-b063-4615-afdb-2babf848603e');

-- --------------------------------------------------------

--
-- Table structure for table `matrixblocks`
--

CREATE TABLE `matrixblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `deletedWithOwner` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `matrixblocks`
--

INSERT INTO `matrixblocks` (`id`, `ownerId`, `fieldId`, `typeId`, `sortOrder`, `deletedWithOwner`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(38, 35, 9, 7, 1, NULL, '2020-03-04 19:07:42', '2020-03-04 19:07:42', 'f19f3483-8c6d-4270-83bb-6e3613dca2b8'),
(41, 40, 9, 7, 1, NULL, '2020-03-04 19:07:42', '2020-03-04 19:07:42', '95e960a9-6a57-4821-b853-36ae26de61c9'),
(44, 43, 9, 7, 1, NULL, '2020-03-04 19:08:41', '2020-03-04 19:08:41', '11e66d72-f95b-4342-9f50-777819b90fef'),
(47, 46, 9, 7, 1, NULL, '2020-03-04 19:23:51', '2020-03-04 19:23:51', '68eab471-f7b0-448d-81d2-ab023965410f'),
(49, 48, 9, 7, 1, NULL, '2020-03-04 19:24:32', '2020-03-04 19:24:32', 'bf49e0d7-cd8d-460d-89f9-fb840d3f1fdc'),
(51, 50, 9, 7, 1, NULL, '2020-03-04 19:31:45', '2020-03-04 19:31:45', '13c3a593-1b40-40fb-8124-746521270d67'),
(54, 53, 9, 7, 1, NULL, '2020-03-04 19:35:52', '2020-03-04 19:35:52', 'f3e35650-4dae-4513-8790-7dccccbf72ce'),
(57, 56, 9, 7, 1, NULL, '2020-03-04 19:39:41', '2020-03-04 19:39:41', '6ab3f10d-0417-4d94-a04c-24f3ecd2c1b4'),
(59, 58, 9, 7, 1, NULL, '2020-03-04 19:47:30', '2020-03-04 19:47:30', 'e36ea96b-e77e-45cd-8443-27039416c661'),
(62, 61, 9, 7, 1, NULL, '2020-03-04 19:54:56', '2020-03-04 19:54:56', '824e7748-522b-45e0-a961-e4e5ffb08abd'),
(65, 64, 9, 7, 1, NULL, '2020-03-04 20:03:56', '2020-03-04 20:03:56', '41f572fe-eed2-458a-9837-7882627178e5'),
(68, 67, 9, 7, 1, NULL, '2020-03-04 20:10:40', '2020-03-04 20:10:40', '62327fbd-b498-4a7c-9b1c-14a6a09b2578'),
(69, 35, 9, 9, 2, NULL, '2020-03-04 20:18:50', '2020-03-04 20:18:50', '93d54b61-2213-4202-a789-31908debc2a1'),
(71, 70, 9, 7, 1, NULL, '2020-03-04 20:18:50', '2020-03-04 20:18:50', 'e684f496-ea55-4363-9b5f-b93be2366cb4'),
(72, 70, 9, 9, 2, NULL, '2020-03-04 20:18:50', '2020-03-04 20:18:50', 'd230726e-9bd1-411d-84a2-75e154eb7e0c'),
(78, 73, 9, 9, 1, NULL, '2020-03-04 20:22:19', '2020-03-04 20:22:19', 'af8b2819-cefa-4211-9404-6e025bfeb8f2'),
(79, 73, 9, 7, 2, NULL, '2020-03-04 20:22:19', '2020-03-04 20:22:19', '59313ce4-3637-47cc-8051-3095a27c1f29'),
(81, 80, 9, 9, 1, NULL, '2020-03-04 20:22:19', '2020-03-04 20:22:19', 'b22f1069-3c52-4acb-8bc2-fd0288126aa5'),
(82, 80, 9, 7, 2, NULL, '2020-03-04 20:22:19', '2020-03-04 20:22:19', 'ff001804-1304-45ef-b65e-87487b2470e9'),
(84, 83, 9, 9, 1, NULL, '2020-03-04 20:23:23', '2020-03-04 20:23:23', 'fd51862a-def0-4b20-94af-4ce035ae041b'),
(85, 83, 9, 7, 2, NULL, '2020-03-04 20:23:23', '2020-03-04 20:23:23', 'f66210cc-c71b-4637-a81a-2d3e6dd96414'),
(87, 86, 9, 9, 1, NULL, '2020-03-04 20:24:25', '2020-03-04 20:24:25', '893e01aa-d291-4a9c-b526-7eb6677d562b'),
(88, 86, 9, 7, 2, NULL, '2020-03-04 20:24:25', '2020-03-04 20:24:25', 'a123576a-8ca2-4bda-a410-c25edf94c2b6');

-- --------------------------------------------------------

--
-- Table structure for table `matrixblocktypes`
--

CREATE TABLE `matrixblocktypes` (
  `id` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `matrixblocktypes`
--

INSERT INTO `matrixblocktypes` (`id`, `fieldId`, `fieldLayoutId`, `name`, `handle`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(7, 9, 9, 'DefaultDesign', 'defaultdesign', 1, '2020-03-04 19:03:57', '2020-03-04 19:27:41', 'e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9'),
(9, 9, 12, 'SecondaryDesign', 'secondarydesign', 2, '2020-03-04 20:15:02', '2020-03-04 20:15:02', 'ef7681b1-796e-4622-9627-55bab78e30e7');

-- --------------------------------------------------------

--
-- Table structure for table `matrixcontent_modules`
--

CREATE TABLE `matrixcontent_modules` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_defaultdesign_imagetitle` text,
  `field_defaultdesign_imagetext` text,
  `field_secondarydesign_imagetext` text,
  `field_secondarydesign_imagetitle` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `matrixcontent_modules`
--

INSERT INTO `matrixcontent_modules` (`id`, `elementId`, `siteId`, `dateCreated`, `dateUpdated`, `uid`, `field_defaultdesign_imagetitle`, `field_defaultdesign_imagetext`, `field_secondarydesign_imagetext`, `field_secondarydesign_imagetitle`) VALUES
(1, 38, 1, '2020-03-04 19:07:42', '2020-03-04 20:10:40', 'd400e6ee-d9d1-4f82-9232-e1ea817285c4', 'Mooie Lenzen!', '<p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat urna vitae ante hendrerit condimentum. In hac habitasse platea dictumst. Cras eu tortor sit amet magna eleifend egestas et varius orci. Aliquam accumsan purus elit, nec tincidunt odio consequat tincidunt. Aenean viverra leo velit. In nisi nulla, pellentesque nec libero at, lobortis euismod dolor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nunc eget ornare tellus. Mauris ultrices sapien scelerisque leo pretium condimentum. Nulla id lacus in ipsum efficitur semper et id nibh. Curabitur purus enim, elementum blandit fringilla in, pretium vitae leo. Morbi id consectetur justo, in suscipit risus. Praesent id molestie quam. Sed vitae varius neque. Vestibulum tincidunt fermentum justo, nec scelerisque erat sollicitudin eget. Suspendisse auctor mollis ipsum vel egestas. Duis eu massa venenatis, suscipit massa at, posuere eros. Mauris lacinia turpis quis rutrum semper. Donec erat leo, luctus sit amet imperdiet in, sodales vel massa. Nunc nec sem ornare, sodales urna quis, vulputate odio. Fusce finibus elit et diam consequat, ac gravida neque tincidunt. Quisque facilisis sodales risus ut iaculis.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Maecenas quis sagittis erat, eget dapibus risus. Duis luctus, massa in consequat interdum, ligula dolor efficitur justo, sed congue nibh nisl in dolor. Fusce ante libero, scelerisque sed posuere sed, condimentum at magna. Sed ultricies sem semper, commodo justo vitae, vehicula arcu. Quisque cursus lacus non dictum maximus. Aenean quis mollis purus. Duis eros lectus, mattis eget lorem vel, tempor pretium odio.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nullam vel sodales magna. Vivamus sollicitudin ut ipsum sed blandit. Donec non dui bibendum, scelerisque lectus ut, faucibus tellus. Suspendisse tristique mi vitae elit dignissim tempus. Curabitur eget quam vitae mi hendrerit auctor. Donec vehicula ex quam, semper interdum diam vulputate et. Sed semper lorem sed mauris aliquam, sit amet mattis massa vestibulum. Vestibulum placerat hendrerit ipsum, eu mattis dui dignissim eu. Sed ultricies tortor eu ornare mattis.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Maecenas sodales aliquam venenatis. In ut tellus sem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec dictum accumsan libero, nec rutrum eros vulputate ac. Proin at nisi ut lacus efficitur convallis et id nulla. Maecenas aliquam est non scelerisque lobortis. Aliquam tempus, nibh eget bibendum vulputate, odio orci ultricies magna, a finibus neque risus a orci. Suspendisse aliquet ante ut velit sodales laoreet. Vivamus euismod lacus orci, ac ultricies nunc volutpat a. Morbi accumsan laoreet sem, et tempor nulla volutpat id. Aliquam erat volutpat. Suspendisse commodo orci nec ultrices vehicula. Praesent dictum sem sit amet sapien congue ullamcorper. Aenean id odio bibendum, eleifend tellus at, condimentum erat. Nunc vel ullamcorper justo.</p>', NULL, NULL),
(2, 39, 1, '2020-03-04 19:07:42', '2020-03-04 19:07:42', 'dc0f05d4-add1-40bd-b3ab-47ad18d8a26e', NULL, NULL, NULL, NULL),
(3, 41, 1, '2020-03-04 19:07:42', '2020-03-04 19:07:42', '107c4a3f-4cb6-40ee-826e-0608f75a61ab', 'Mooie Lenzen!', NULL, NULL, NULL),
(4, 42, 1, '2020-03-04 19:07:42', '2020-03-04 19:07:42', '439fbde9-fe85-4153-8ddc-43097dbb10b4', NULL, NULL, NULL, NULL),
(5, 44, 1, '2020-03-04 19:08:41', '2020-03-04 19:08:41', 'ecc93d7a-b9fe-4d4a-ab56-04f89c33a3fd', 'Mooie Lenzen!', NULL, NULL, NULL),
(6, 45, 1, '2020-03-04 19:08:41', '2020-03-04 19:08:41', '0d6fd36c-df74-450e-91d5-3e3487f03fbd', NULL, NULL, NULL, NULL),
(7, 47, 1, '2020-03-04 19:23:51', '2020-03-04 19:23:51', 'de8d87be-1b48-4196-9197-7bef0fd90253', 'Mooie Lenzen!', '<h1 style=\"margin:0px;padding:0px;font-family:arial, sans-serif;font-size:14px;font-weight:normal;height:1px;white-space:nowrap;width:1px;color:rgb(34,34,34);font-style:normal;letter-spacing:normal;text-indent:0px;text-transform:none;word-spacing:0px;background-color:rgb(255,255,255);\">Search Results</h1><div style=\"margin-top:6px;color:rgb(34,34,34);font-family:arial, sans-serif;font-size:medium;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\"><div lang=\"en-nl\" style=\"line-height:1.2;text-align:left;font-family:arial, sans-serif;font-size:14px;margin-top:0px;margin-bottom:27px;\" xml:lang=\"en-nl\"><div style=\"border:1px solid rgb(223,225,229);margin-left:-20px;margin-right:-20px;\"><h2 style=\"height:1px;margin:0px;padding:0px;white-space:nowrap;width:1px;\">Featured snippet from the web</h2><div lang=\"en-nl\" style=\"clear:none;padding-top:16px;padding-left:16px;padding-right:16px;\" xml:lang=\"en-nl\"><div style=\"padding-bottom:20px;\"><span style=\"font-size:16px;line-height:1.375;\"></span><div style=\"padding:0px 0px 10px;\">The <strong>Format</strong> Document Command</div><span style=\"padding:0px 8px 0px 0px;\">To open the command pallette, you can use Command **+ Shift + P** on Mac or Control **+ Shift + P** on <strong>Windows</strong>. In the command pallette search <strong>format</strong>, then choose** <strong>Format</strong> Document**. You may then be prompted by to choose which formatter to use.</span><span style=\"color:rgb(112,117,122);font-size:12px;line-height:1.33;white-space:nowrap;\">Dec 12, 2019</span></div></div></div></div></div>', NULL, NULL),
(8, 49, 1, '2020-03-04 19:24:32', '2020-03-04 19:24:32', '6419768d-a8b6-4f59-a978-ae3168e0d61e', 'Mooie Lenzen!', '<p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat urna vitae ante hendrerit condimentum. In hac habitasse platea dictumst. Cras eu tortor sit amet magna eleifend egestas et varius orci. Aliquam accumsan purus elit, nec tincidunt odio consequat tincidunt. Aenean viverra leo velit. In nisi nulla, pellentesque nec libero at, lobortis euismod dolor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nunc eget ornare tellus. Mauris ultrices sapien scelerisque leo pretium condimentum. Nulla id lacus in ipsum efficitur semper et id nibh. Curabitur purus enim, elementum blandit fringilla in, pretium vitae leo. Morbi id consectetur justo, in suscipit risus. Praesent id molestie quam. Sed vitae varius neque. Vestibulum tincidunt fermentum justo, nec scelerisque erat sollicitudin eget. Suspendisse auctor mollis ipsum vel egestas. Duis eu massa venenatis, suscipit massa at, posuere eros. Mauris lacinia turpis quis rutrum semper. Donec erat leo, luctus sit amet imperdiet in, sodales vel massa. Nunc nec sem ornare, sodales urna quis, vulputate odio. Fusce finibus elit et diam consequat, ac gravida neque tincidunt. Quisque facilisis sodales risus ut iaculis.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Maecenas quis sagittis erat, eget dapibus risus. Duis luctus, massa in consequat interdum, ligula dolor efficitur justo, sed congue nibh nisl in dolor. Fusce ante libero, scelerisque sed posuere sed, condimentum at magna. Sed ultricies sem semper, commodo justo vitae, vehicula arcu. Quisque cursus lacus non dictum maximus. Aenean quis mollis purus. Duis eros lectus, mattis eget lorem vel, tempor pretium odio.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nullam vel sodales magna. Vivamus sollicitudin ut ipsum sed blandit. Donec non dui bibendum, scelerisque lectus ut, faucibus tellus. Suspendisse tristique mi vitae elit dignissim tempus. Curabitur eget quam vitae mi hendrerit auctor. Donec vehicula ex quam, semper interdum diam vulputate et. Sed semper lorem sed mauris aliquam, sit amet mattis massa vestibulum. Vestibulum placerat hendrerit ipsum, eu mattis dui dignissim eu. Sed ultricies tortor eu ornare mattis.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Maecenas sodales aliquam venenatis. In ut tellus sem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec dictum accumsan libero, nec rutrum eros vulputate ac. Proin at nisi ut lacus efficitur convallis et id nulla. Maecenas aliquam est non scelerisque lobortis. Aliquam tempus, nibh eget bibendum vulputate, odio orci ultricies magna, a finibus neque risus a orci. Suspendisse aliquet ante ut velit sodales laoreet. Vivamus euismod lacus orci, ac ultricies nunc volutpat a. Morbi accumsan laoreet sem, et tempor nulla volutpat id. Aliquam erat volutpat. Suspendisse commodo orci nec ultrices vehicula. Praesent dictum sem sit amet sapien congue ullamcorper. Aenean id odio bibendum, eleifend tellus at, condimentum erat. Nunc vel ullamcorper justo.</p>', NULL, NULL),
(9, 51, 1, '2020-03-04 19:31:45', '2020-03-04 19:31:45', '9952b518-45c2-459c-ae5f-76ea52225629', 'Mooie Lenzen!', '<p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat urna vitae ante hendrerit condimentum. In hac habitasse platea dictumst. Cras eu tortor sit amet magna eleifend egestas et varius orci. Aliquam accumsan purus elit, nec tincidunt odio consequat tincidunt. Aenean viverra leo velit. In nisi nulla, pellentesque nec libero at, lobortis euismod dolor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nunc eget ornare tellus. Mauris ultrices sapien scelerisque leo pretium condimentum. Nulla id lacus in ipsum efficitur semper et id nibh. Curabitur purus enim, elementum blandit fringilla in, pretium vitae leo. Morbi id consectetur justo, in suscipit risus. Praesent id molestie quam. Sed vitae varius neque. Vestibulum tincidunt fermentum justo, nec scelerisque erat sollicitudin eget. Suspendisse auctor mollis ipsum vel egestas. Duis eu massa venenatis, suscipit massa at, posuere eros. Mauris lacinia turpis quis rutrum semper. Donec erat leo, luctus sit amet imperdiet in, sodales vel massa. Nunc nec sem ornare, sodales urna quis, vulputate odio. Fusce finibus elit et diam consequat, ac gravida neque tincidunt. Quisque facilisis sodales risus ut iaculis.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Maecenas quis sagittis erat, eget dapibus risus. Duis luctus, massa in consequat interdum, ligula dolor efficitur justo, sed congue nibh nisl in dolor. Fusce ante libero, scelerisque sed posuere sed, condimentum at magna. Sed ultricies sem semper, commodo justo vitae, vehicula arcu. Quisque cursus lacus non dictum maximus. Aenean quis mollis purus. Duis eros lectus, mattis eget lorem vel, tempor pretium odio.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nullam vel sodales magna. Vivamus sollicitudin ut ipsum sed blandit. Donec non dui bibendum, scelerisque lectus ut, faucibus tellus. Suspendisse tristique mi vitae elit dignissim tempus. Curabitur eget quam vitae mi hendrerit auctor. Donec vehicula ex quam, semper interdum diam vulputate et. Sed semper lorem sed mauris aliquam, sit amet mattis massa vestibulum. Vestibulum placerat hendrerit ipsum, eu mattis dui dignissim eu. Sed ultricies tortor eu ornare mattis.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Maecenas sodales aliquam venenatis. In ut tellus sem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec dictum accumsan libero, nec rutrum eros vulputate ac. Proin at nisi ut lacus efficitur convallis et id nulla. Maecenas aliquam est non scelerisque lobortis. Aliquam tempus, nibh eget bibendum vulputate, odio orci ultricies magna, a finibus neque risus a orci. Suspendisse aliquet ante ut velit sodales laoreet. Vivamus euismod lacus orci, ac ultricies nunc volutpat a. Morbi accumsan laoreet sem, et tempor nulla volutpat id. Aliquam erat volutpat. Suspendisse commodo orci nec ultrices vehicula. Praesent dictum sem sit amet sapien congue ullamcorper. Aenean id odio bibendum, eleifend tellus at, condimentum erat. Nunc vel ullamcorper justo.</p>', NULL, NULL),
(10, 54, 1, '2020-03-04 19:35:52', '2020-03-04 19:35:52', 'a42ce4b0-8b07-43ac-b1fc-589d469ff99e', 'Mooie Lenzen!', '<p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat urna vitae ante hendrerit condimentum. In hac habitasse platea dictumst. Cras eu tortor sit amet magna eleifend egestas et varius orci. Aliquam accumsan purus elit, nec tincidunt odio consequat tincidunt. Aenean viverra leo velit. In nisi nulla, pellentesque nec libero at, lobortis euismod dolor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nunc eget ornare tellus. Mauris ultrices sapien scelerisque leo pretium condimentum. Nulla id lacus in ipsum efficitur semper et id nibh. Curabitur purus enim, elementum blandit fringilla in, pretium vitae leo. Morbi id consectetur justo, in suscipit risus. Praesent id molestie quam. Sed vitae varius neque. Vestibulum tincidunt fermentum justo, nec scelerisque erat sollicitudin eget. Suspendisse auctor mollis ipsum vel egestas. Duis eu massa venenatis, suscipit massa at, posuere eros. Mauris lacinia turpis quis rutrum semper. Donec erat leo, luctus sit amet imperdiet in, sodales vel massa. Nunc nec sem ornare, sodales urna quis, vulputate odio. Fusce finibus elit et diam consequat, ac gravida neque tincidunt. Quisque facilisis sodales risus ut iaculis.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Maecenas quis sagittis erat, eget dapibus risus. Duis luctus, massa in consequat interdum, ligula dolor efficitur justo, sed congue nibh nisl in dolor. Fusce ante libero, scelerisque sed posuere sed, condimentum at magna. Sed ultricies sem semper, commodo justo vitae, vehicula arcu. Quisque cursus lacus non dictum maximus. Aenean quis mollis purus. Duis eros lectus, mattis eget lorem vel, tempor pretium odio.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nullam vel sodales magna. Vivamus sollicitudin ut ipsum sed blandit. Donec non dui bibendum, scelerisque lectus ut, faucibus tellus. Suspendisse tristique mi vitae elit dignissim tempus. Curabitur eget quam vitae mi hendrerit auctor. Donec vehicula ex quam, semper interdum diam vulputate et. Sed semper lorem sed mauris aliquam, sit amet mattis massa vestibulum. Vestibulum placerat hendrerit ipsum, eu mattis dui dignissim eu. Sed ultricies tortor eu ornare mattis.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Maecenas sodales aliquam venenatis. In ut tellus sem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec dictum accumsan libero, nec rutrum eros vulputate ac. Proin at nisi ut lacus efficitur convallis et id nulla. Maecenas aliquam est non scelerisque lobortis. Aliquam tempus, nibh eget bibendum vulputate, odio orci ultricies magna, a finibus neque risus a orci. Suspendisse aliquet ante ut velit sodales laoreet. Vivamus euismod lacus orci, ac ultricies nunc volutpat a. Morbi accumsan laoreet sem, et tempor nulla volutpat id. Aliquam erat volutpat. Suspendisse commodo orci nec ultrices vehicula. Praesent dictum sem sit amet sapien congue ullamcorper. Aenean id odio bibendum, eleifend tellus at, condimentum erat. Nunc vel ullamcorper justo.</p>', NULL, NULL),
(11, 57, 1, '2020-03-04 19:39:41', '2020-03-04 19:39:41', '69c9312c-203a-42cc-b280-3506ddeb3cfe', 'Mooie Lenzen!', '<p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat urna vitae ante hendrerit condimentum. In hac habitasse platea dictumst. Cras eu tortor sit amet magna eleifend egestas et varius orci. Aliquam accumsan purus elit, nec tincidunt odio consequat tincidunt. Aenean viverra leo velit. In nisi nulla, pellentesque nec libero at, lobortis euismod dolor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nunc eget ornare tellus. Mauris ultrices sapien scelerisque leo pretium condimentum. Nulla id lacus in ipsum efficitur semper et id nibh. Curabitur purus enim, elementum blandit fringilla in, pretium vitae leo. Morbi id consectetur justo, in suscipit risus. Praesent id molestie quam. Sed vitae varius neque. Vestibulum tincidunt fermentum justo, nec scelerisque erat sollicitudin eget. Suspendisse auctor mollis ipsum vel egestas. Duis eu massa venenatis, suscipit massa at, posuere eros. Mauris lacinia turpis quis rutrum semper. Donec erat leo, luctus sit amet imperdiet in, sodales vel massa. Nunc nec sem ornare, sodales urna quis, vulputate odio. Fusce finibus elit et diam consequat, ac gravida neque tincidunt. Quisque facilisis sodales risus ut iaculis.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Maecenas quis sagittis erat, eget dapibus risus. Duis luctus, massa in consequat interdum, ligula dolor efficitur justo, sed congue nibh nisl in dolor. Fusce ante libero, scelerisque sed posuere sed, condimentum at magna. Sed ultricies sem semper, commodo justo vitae, vehicula arcu. Quisque cursus lacus non dictum maximus. Aenean quis mollis purus. Duis eros lectus, mattis eget lorem vel, tempor pretium odio.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nullam vel sodales magna. Vivamus sollicitudin ut ipsum sed blandit. Donec non dui bibendum, scelerisque lectus ut, faucibus tellus. Suspendisse tristique mi vitae elit dignissim tempus. Curabitur eget quam vitae mi hendrerit auctor. Donec vehicula ex quam, semper interdum diam vulputate et. Sed semper lorem sed mauris aliquam, sit amet mattis massa vestibulum. Vestibulum placerat hendrerit ipsum, eu mattis dui dignissim eu. Sed ultricies tortor eu ornare mattis.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Maecenas sodales aliquam venenatis. In ut tellus sem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec dictum accumsan libero, nec rutrum eros vulputate ac. Proin at nisi ut lacus efficitur convallis et id nulla. Maecenas aliquam est non scelerisque lobortis. Aliquam tempus, nibh eget bibendum vulputate, odio orci ultricies magna, a finibus neque risus a orci. Suspendisse aliquet ante ut velit sodales laoreet. Vivamus euismod lacus orci, ac ultricies nunc volutpat a. Morbi accumsan laoreet sem, et tempor nulla volutpat id. Aliquam erat volutpat. Suspendisse commodo orci nec ultrices vehicula. Praesent dictum sem sit amet sapien congue ullamcorper. Aenean id odio bibendum, eleifend tellus at, condimentum erat. Nunc vel ullamcorper justo.</p>', NULL, NULL),
(12, 59, 1, '2020-03-04 19:47:30', '2020-03-04 19:47:30', 'a8ad5ece-4daf-49ac-93dc-d3eea306820b', 'Mooie Lenzen!', '<p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat urna vitae ante hendrerit condimentum. In hac habitasse platea dictumst. Cras eu tortor sit amet magna eleifend egestas et varius orci. Aliquam accumsan purus elit, nec tincidunt odio consequat tincidunt. Aenean viverra leo velit. In nisi nulla, pellentesque nec libero at, lobortis euismod dolor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nunc eget ornare tellus. Mauris ultrices sapien scelerisque leo pretium condimentum. Nulla id lacus in ipsum efficitur semper et id nibh. Curabitur purus enim, elementum blandit fringilla in, pretium vitae leo. Morbi id consectetur justo, in suscipit risus. Praesent id molestie quam. Sed vitae varius neque. Vestibulum tincidunt fermentum justo, nec scelerisque erat sollicitudin eget. Suspendisse auctor mollis ipsum vel egestas. Duis eu massa venenatis, suscipit massa at, posuere eros. Mauris lacinia turpis quis rutrum semper. Donec erat leo, luctus sit amet imperdiet in, sodales vel massa. Nunc nec sem ornare, sodales urna quis, vulputate odio. Fusce finibus elit et diam consequat, ac gravida neque tincidunt. Quisque facilisis sodales risus ut iaculis.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Maecenas quis sagittis erat, eget dapibus risus. Duis luctus, massa in consequat interdum, ligula dolor efficitur justo, sed congue nibh nisl in dolor. Fusce ante libero, scelerisque sed posuere sed, condimentum at magna. Sed ultricies sem semper, commodo justo vitae, vehicula arcu. Quisque cursus lacus non dictum maximus. Aenean quis mollis purus. Duis eros lectus, mattis eget lorem vel, tempor pretium odio.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nullam vel sodales magna. Vivamus sollicitudin ut ipsum sed blandit. Donec non dui bibendum, scelerisque lectus ut, faucibus tellus. Suspendisse tristique mi vitae elit dignissim tempus. Curabitur eget quam vitae mi hendrerit auctor. Donec vehicula ex quam, semper interdum diam vulputate et. Sed semper lorem sed mauris aliquam, sit amet mattis massa vestibulum. Vestibulum placerat hendrerit ipsum, eu mattis dui dignissim eu. Sed ultricies tortor eu ornare mattis.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Maecenas sodales aliquam venenatis. In ut tellus sem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec dictum accumsan libero, nec rutrum eros vulputate ac. Proin at nisi ut lacus efficitur convallis et id nulla. Maecenas aliquam est non scelerisque lobortis. Aliquam tempus, nibh eget bibendum vulputate, odio orci ultricies magna, a finibus neque risus a orci. Suspendisse aliquet ante ut velit sodales laoreet. Vivamus euismod lacus orci, ac ultricies nunc volutpat a. Morbi accumsan laoreet sem, et tempor nulla volutpat id. Aliquam erat volutpat. Suspendisse commodo orci nec ultrices vehicula. Praesent dictum sem sit amet sapien congue ullamcorper. Aenean id odio bibendum, eleifend tellus at, condimentum erat. Nunc vel ullamcorper justo.</p>', NULL, NULL),
(13, 62, 1, '2020-03-04 19:54:56', '2020-03-04 19:54:56', 'dd712f77-1627-4991-8163-b99c0c24d697', 'Mooie Lenzen!', '<p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat urna vitae ante hendrerit condimentum. In hac habitasse platea dictumst. Cras eu tortor sit amet magna eleifend egestas et varius orci. Aliquam accumsan purus elit, nec tincidunt odio consequat tincidunt. Aenean viverra leo velit. In nisi nulla, pellentesque nec libero at, lobortis euismod dolor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nunc eget ornare tellus. Mauris ultrices sapien scelerisque leo pretium condimentum. Nulla id lacus in ipsum efficitur semper et id nibh. Curabitur purus enim, elementum blandit fringilla in, pretium vitae leo. Morbi id consectetur justo, in suscipit risus. Praesent id molestie quam. Sed vitae varius neque. Vestibulum tincidunt fermentum justo, nec scelerisque erat sollicitudin eget. Suspendisse auctor mollis ipsum vel egestas. Duis eu massa venenatis, suscipit massa at, posuere eros. Mauris lacinia turpis quis rutrum semper. Donec erat leo, luctus sit amet imperdiet in, sodales vel massa. Nunc nec sem ornare, sodales urna quis, vulputate odio. Fusce finibus elit et diam consequat, ac gravida neque tincidunt. Quisque facilisis sodales risus ut iaculis.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Maecenas quis sagittis erat, eget dapibus risus. Duis luctus, massa in consequat interdum, ligula dolor efficitur justo, sed congue nibh nisl in dolor. Fusce ante libero, scelerisque sed posuere sed, condimentum at magna. Sed ultricies sem semper, commodo justo vitae, vehicula arcu. Quisque cursus lacus non dictum maximus. Aenean quis mollis purus. Duis eros lectus, mattis eget lorem vel, tempor pretium odio.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nullam vel sodales magna. Vivamus sollicitudin ut ipsum sed blandit. Donec non dui bibendum, scelerisque lectus ut, faucibus tellus. Suspendisse tristique mi vitae elit dignissim tempus. Curabitur eget quam vitae mi hendrerit auctor. Donec vehicula ex quam, semper interdum diam vulputate et. Sed semper lorem sed mauris aliquam, sit amet mattis massa vestibulum. Vestibulum placerat hendrerit ipsum, eu mattis dui dignissim eu. Sed ultricies tortor eu ornare mattis.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Maecenas sodales aliquam venenatis. In ut tellus sem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec dictum accumsan libero, nec rutrum eros vulputate ac. Proin at nisi ut lacus efficitur convallis et id nulla. Maecenas aliquam est non scelerisque lobortis. Aliquam tempus, nibh eget bibendum vulputate, odio orci ultricies magna, a finibus neque risus a orci. Suspendisse aliquet ante ut velit sodales laoreet. Vivamus euismod lacus orci, ac ultricies nunc volutpat a. Morbi accumsan laoreet sem, et tempor nulla volutpat id. Aliquam erat volutpat. Suspendisse commodo orci nec ultrices vehicula. Praesent dictum sem sit amet sapien congue ullamcorper. Aenean id odio bibendum, eleifend tellus at, condimentum erat. Nunc vel ullamcorper justo.</p>', NULL, NULL),
(14, 65, 1, '2020-03-04 20:03:56', '2020-03-04 20:03:56', '6f4ab012-3455-4f3e-8146-76ec855cf9f4', 'Mooie Lenzen!', '<p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat urna vitae ante hendrerit condimentum. In hac habitasse platea dictumst. Cras eu tortor sit amet magna eleifend egestas et varius orci. Aliquam accumsan purus elit, nec tincidunt odio consequat tincidunt. Aenean viverra leo velit. In nisi nulla, pellentesque nec libero at, lobortis euismod dolor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nunc eget ornare tellus. Mauris ultrices sapien scelerisque leo pretium condimentum. Nulla id lacus in ipsum efficitur semper et id nibh. Curabitur purus enim, elementum blandit fringilla in, pretium vitae leo. Morbi id consectetur justo, in suscipit risus. Praesent id molestie quam. Sed vitae varius neque. Vestibulum tincidunt fermentum justo, nec scelerisque erat sollicitudin eget. Suspendisse auctor mollis ipsum vel egestas. Duis eu massa venenatis, suscipit massa at, posuere eros. Mauris lacinia turpis quis rutrum semper. Donec erat leo, luctus sit amet imperdiet in, sodales vel massa. Nunc nec sem ornare, sodales urna quis, vulputate odio. Fusce finibus elit et diam consequat, ac gravida neque tincidunt. Quisque facilisis sodales risus ut iaculis.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Maecenas quis sagittis erat, eget dapibus risus. Duis luctus, massa in consequat interdum, ligula dolor efficitur justo, sed congue nibh nisl in dolor. Fusce ante libero, scelerisque sed posuere sed, condimentum at magna. Sed ultricies sem semper, commodo justo vitae, vehicula arcu. Quisque cursus lacus non dictum maximus. Aenean quis mollis purus. Duis eros lectus, mattis eget lorem vel, tempor pretium odio.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nullam vel sodales magna. Vivamus sollicitudin ut ipsum sed blandit. Donec non dui bibendum, scelerisque lectus ut, faucibus tellus. Suspendisse tristique mi vitae elit dignissim tempus. Curabitur eget quam vitae mi hendrerit auctor. Donec vehicula ex quam, semper interdum diam vulputate et. Sed semper lorem sed mauris aliquam, sit amet mattis massa vestibulum. Vestibulum placerat hendrerit ipsum, eu mattis dui dignissim eu. Sed ultricies tortor eu ornare mattis.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Maecenas sodales aliquam venenatis. In ut tellus sem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec dictum accumsan libero, nec rutrum eros vulputate ac. Proin at nisi ut lacus efficitur convallis et id nulla. Maecenas aliquam est non scelerisque lobortis. Aliquam tempus, nibh eget bibendum vulputate, odio orci ultricies magna, a finibus neque risus a orci. Suspendisse aliquet ante ut velit sodales laoreet. Vivamus euismod lacus orci, ac ultricies nunc volutpat a. Morbi accumsan laoreet sem, et tempor nulla volutpat id. Aliquam erat volutpat. Suspendisse commodo orci nec ultrices vehicula. Praesent dictum sem sit amet sapien congue ullamcorper. Aenean id odio bibendum, eleifend tellus at, condimentum erat. Nunc vel ullamcorper justo.</p>', NULL, NULL),
(15, 68, 1, '2020-03-04 20:10:40', '2020-03-04 20:10:40', '0e55ea9d-bc44-45ca-8eba-8fddbb390b08', 'Mooie Lenzen!', '<p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat urna vitae ante hendrerit condimentum. In hac habitasse platea dictumst. Cras eu tortor sit amet magna eleifend egestas et varius orci. Aliquam accumsan purus elit, nec tincidunt odio consequat tincidunt. Aenean viverra leo velit. In nisi nulla, pellentesque nec libero at, lobortis euismod dolor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nunc eget ornare tellus. Mauris ultrices sapien scelerisque leo pretium condimentum. Nulla id lacus in ipsum efficitur semper et id nibh. Curabitur purus enim, elementum blandit fringilla in, pretium vitae leo. Morbi id consectetur justo, in suscipit risus. Praesent id molestie quam. Sed vitae varius neque. Vestibulum tincidunt fermentum justo, nec scelerisque erat sollicitudin eget. Suspendisse auctor mollis ipsum vel egestas. Duis eu massa venenatis, suscipit massa at, posuere eros. Mauris lacinia turpis quis rutrum semper. Donec erat leo, luctus sit amet imperdiet in, sodales vel massa. Nunc nec sem ornare, sodales urna quis, vulputate odio. Fusce finibus elit et diam consequat, ac gravida neque tincidunt. Quisque facilisis sodales risus ut iaculis.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Maecenas quis sagittis erat, eget dapibus risus. Duis luctus, massa in consequat interdum, ligula dolor efficitur justo, sed congue nibh nisl in dolor. Fusce ante libero, scelerisque sed posuere sed, condimentum at magna. Sed ultricies sem semper, commodo justo vitae, vehicula arcu. Quisque cursus lacus non dictum maximus. Aenean quis mollis purus. Duis eros lectus, mattis eget lorem vel, tempor pretium odio.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nullam vel sodales magna. Vivamus sollicitudin ut ipsum sed blandit. Donec non dui bibendum, scelerisque lectus ut, faucibus tellus. Suspendisse tristique mi vitae elit dignissim tempus. Curabitur eget quam vitae mi hendrerit auctor. Donec vehicula ex quam, semper interdum diam vulputate et. Sed semper lorem sed mauris aliquam, sit amet mattis massa vestibulum. Vestibulum placerat hendrerit ipsum, eu mattis dui dignissim eu. Sed ultricies tortor eu ornare mattis.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Maecenas sodales aliquam venenatis. In ut tellus sem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec dictum accumsan libero, nec rutrum eros vulputate ac. Proin at nisi ut lacus efficitur convallis et id nulla. Maecenas aliquam est non scelerisque lobortis. Aliquam tempus, nibh eget bibendum vulputate, odio orci ultricies magna, a finibus neque risus a orci. Suspendisse aliquet ante ut velit sodales laoreet. Vivamus euismod lacus orci, ac ultricies nunc volutpat a. Morbi accumsan laoreet sem, et tempor nulla volutpat id. Aliquam erat volutpat. Suspendisse commodo orci nec ultrices vehicula. Praesent dictum sem sit amet sapien congue ullamcorper. Aenean id odio bibendum, eleifend tellus at, condimentum erat. Nunc vel ullamcorper justo.</p>', NULL, NULL),
(16, 69, 1, '2020-03-04 20:18:50', '2020-03-04 20:18:50', '80ba2b0c-ddb1-4075-b557-cdd697cd2bcc', NULL, NULL, '<p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\"><u><em><strong>Maecenas sodales aliquam venenatis. In ut tellus sem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec dictum accumsan libero, nec rutrum eros vulputate ac. Proin at nisi ut lacus efficitur convallis et id nulla. Maecenas aliquam est non scelerisque lobortis. Aliquam tempus, nibh eget bibendum vulputate, odio orci ultricies magna, a finibus neque risus a orci. Suspendisse aliquet ante ut velit sodales laoreet. Vivamus euismod lacus orci, ac ultricies nunc volutpat a. Morbi accumsan laoreet sem, et tempor nulla volutpat id. Aliquam erat volutpat. Suspendisse commodo orci nec ultrices vehicula. Praesent dictum sem sit amet sapien congue ullamcorper. Aenean id odio bibendum, eleifend tellus at, condimentum erat. Nunc vel ullamcorper justo.</strong></em></u></p>', 'AMAZING TITLE #2'),
(17, 71, 1, '2020-03-04 20:18:50', '2020-03-04 20:18:50', '529209db-e73d-4511-a2cb-c21016786d89', 'Mooie Lenzen!', '<p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat urna vitae ante hendrerit condimentum. In hac habitasse platea dictumst. Cras eu tortor sit amet magna eleifend egestas et varius orci. Aliquam accumsan purus elit, nec tincidunt odio consequat tincidunt. Aenean viverra leo velit. In nisi nulla, pellentesque nec libero at, lobortis euismod dolor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nunc eget ornare tellus. Mauris ultrices sapien scelerisque leo pretium condimentum. Nulla id lacus in ipsum efficitur semper et id nibh. Curabitur purus enim, elementum blandit fringilla in, pretium vitae leo. Morbi id consectetur justo, in suscipit risus. Praesent id molestie quam. Sed vitae varius neque. Vestibulum tincidunt fermentum justo, nec scelerisque erat sollicitudin eget. Suspendisse auctor mollis ipsum vel egestas. Duis eu massa venenatis, suscipit massa at, posuere eros. Mauris lacinia turpis quis rutrum semper. Donec erat leo, luctus sit amet imperdiet in, sodales vel massa. Nunc nec sem ornare, sodales urna quis, vulputate odio. Fusce finibus elit et diam consequat, ac gravida neque tincidunt. Quisque facilisis sodales risus ut iaculis.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Maecenas quis sagittis erat, eget dapibus risus. Duis luctus, massa in consequat interdum, ligula dolor efficitur justo, sed congue nibh nisl in dolor. Fusce ante libero, scelerisque sed posuere sed, condimentum at magna. Sed ultricies sem semper, commodo justo vitae, vehicula arcu. Quisque cursus lacus non dictum maximus. Aenean quis mollis purus. Duis eros lectus, mattis eget lorem vel, tempor pretium odio.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nullam vel sodales magna. Vivamus sollicitudin ut ipsum sed blandit. Donec non dui bibendum, scelerisque lectus ut, faucibus tellus. Suspendisse tristique mi vitae elit dignissim tempus. Curabitur eget quam vitae mi hendrerit auctor. Donec vehicula ex quam, semper interdum diam vulputate et. Sed semper lorem sed mauris aliquam, sit amet mattis massa vestibulum. Vestibulum placerat hendrerit ipsum, eu mattis dui dignissim eu. Sed ultricies tortor eu ornare mattis.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Maecenas sodales aliquam venenatis. In ut tellus sem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec dictum accumsan libero, nec rutrum eros vulputate ac. Proin at nisi ut lacus efficitur convallis et id nulla. Maecenas aliquam est non scelerisque lobortis. Aliquam tempus, nibh eget bibendum vulputate, odio orci ultricies magna, a finibus neque risus a orci. Suspendisse aliquet ante ut velit sodales laoreet. Vivamus euismod lacus orci, ac ultricies nunc volutpat a. Morbi accumsan laoreet sem, et tempor nulla volutpat id. Aliquam erat volutpat. Suspendisse commodo orci nec ultrices vehicula. Praesent dictum sem sit amet sapien congue ullamcorper. Aenean id odio bibendum, eleifend tellus at, condimentum erat. Nunc vel ullamcorper justo.</p>', NULL, NULL);
INSERT INTO `matrixcontent_modules` (`id`, `elementId`, `siteId`, `dateCreated`, `dateUpdated`, `uid`, `field_defaultdesign_imagetitle`, `field_defaultdesign_imagetext`, `field_secondarydesign_imagetext`, `field_secondarydesign_imagetitle`) VALUES
(18, 72, 1, '2020-03-04 20:18:50', '2020-03-04 20:18:50', 'fdf40f3f-1671-44ad-ba02-e3f7b8cda119', NULL, NULL, '<p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\"><u><em><strong>Maecenas sodales aliquam venenatis. In ut tellus sem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec dictum accumsan libero, nec rutrum eros vulputate ac. Proin at nisi ut lacus efficitur convallis et id nulla. Maecenas aliquam est non scelerisque lobortis. Aliquam tempus, nibh eget bibendum vulputate, odio orci ultricies magna, a finibus neque risus a orci. Suspendisse aliquet ante ut velit sodales laoreet. Vivamus euismod lacus orci, ac ultricies nunc volutpat a. Morbi accumsan laoreet sem, et tempor nulla volutpat id. Aliquam erat volutpat. Suspendisse commodo orci nec ultrices vehicula. Praesent dictum sem sit amet sapien congue ullamcorper. Aenean id odio bibendum, eleifend tellus at, condimentum erat. Nunc vel ullamcorper justo.</strong></em></u></p>', 'AMAZING TITLE #2'),
(19, 78, 1, '2020-03-04 20:22:19', '2020-03-04 20:24:25', '1656d99c-8af0-4a72-8f85-e4a15cb0b7dd', NULL, NULL, '<p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras elementum urna tincidunt sem hendrerit pretium. Vivamus in sapien pretium, rhoncus orci eget, suscipit diam. Nunc viverra lorem mauris, et aliquet odio luctus nec. Duis scelerisque tellus nec leo condimentum, id iaculis neque porttitor. Etiam quis faucibus orci. Curabitur pharetra dictum maximus. Quisque in imperdiet tellus, ac porta enim. Suspendisse rhoncus convallis neque, ac volutpat est cursus et. Sed eget lacus eget nunc ornare feugiat. Pellentesque vel ante quis lectus fermentum sodales. Curabitur orci felis, varius eget volutpat non, interdum sed nulla. Proin eu congue sapien, non euismod nisi. Nam lorem libero, ullamcorper vitae gravida luctus, mattis eget orci.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Fusce ac sapien a nunc ultrices interdum vitae at erat. Mauris convallis sagittis euismod. Vestibulum porttitor, mi in finibus pharetra, ex nulla lobortis leo, luctus vestibulum diam erat nec libero. Maecenas ornare accumsan commodo. Fusce molestie malesuada nulla in varius. Aenean mollis eros interdum sem molestie posuere. Duis fermentum tincidunt bibendum. Integer dignissim ullamcorper viverra. Aenean eget enim dignissim, hendrerit ex nec, volutpat tellus. Maecenas venenatis nunc dolor, ac pharetra leo lacinia vel. Donec nibh urna, varius in condimentum id, pharetra facilisis ligula. Sed egestas ipsum et gravida condimentum.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nullam ut urna ac lectus tempor lacinia vel nec ligula. Curabitur eget nunc augue. Cras et interdum sapien, a feugiat mi. Quisque eleifend, lectus sit amet pulvinar suscipit, dui sem sagittis ex, sed posuere orci est at libero. Sed ornare augue purus, eget consectetur massa cursus eu. Maecenas ut enim a odio gravida varius semper at libero. Phasellus tincidunt magna arcu, eget imperdiet ligula aliquam nec.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Suspendisse viverra nulla et mauris sodales dictum. Donec id ligula purus. Sed id tellus justo. Suspendisse gravida dapibus enim ac auctor. Sed pretium semper metus a suscipit. Nulla tortor risus, ornare in congue non, malesuada ut magna. Quisque lacinia, massa a vehicula molestie, mi nisi vestibulum enim, sit amet molestie nibh neque in orci. Sed tempus consectetur placerat. Sed in mauris id tortor pulvinar dictum ac ac magna. Ut quis ipsum a leo dapibus interdum. Aliquam et scelerisque turpis, nec vestibulum mauris. Morbi pretium nisi non lectus dictum, nec auctor turpis maximus. Aliquam consequat fringilla odio vitae varius.</p><blockquote>Wow!</blockquote>', 'This is the 2nd page that uses this template!'),
(20, 79, 1, '2020-03-04 20:22:19', '2020-03-04 20:24:25', '0ae1390d-d1fd-4b6b-b3a8-b8b4e64b640c', 'The two sections have been switched around!', '<p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras elementum urna tincidunt sem hendrerit pretium. Vivamus in sapien pretium, rhoncus orci eget, suscipit diam. Nunc viverra lorem mauris, et aliquet odio luctus nec. Duis scelerisque tellus nec leo condimentum, id iaculis neque porttitor. Etiam quis faucibus orci. Curabitur pharetra dictum maximus. Quisque in imperdiet tellus, ac porta enim. Suspendisse rhoncus convallis neque, ac volutpat est cursus et. Sed eget lacus eget nunc ornare feugiat. Pellentesque vel ante quis lectus fermentum sodales. Curabitur orci felis, varius eget volutpat non, interdum sed nulla. Proin eu congue sapien, non euismod nisi. Nam lorem libero, ullamcorper vitae gravida luctus, mattis eget orci.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Fusce ac sapien a nunc ultrices interdum vitae at erat. Mauris convallis sagittis euismod. Vestibulum porttitor, mi in finibus pharetra, ex nulla lobortis leo, luctus vestibulum diam erat nec libero. Maecenas ornare accumsan commodo. Fusce molestie malesuada nulla in varius. Aenean mollis eros interdum sem molestie posuere. Duis fermentum tincidunt bibendum. Integer dignissim ullamcorper viverra. Aenean eget enim dignissim, hendrerit ex nec, volutpat tellus. Maecenas venenatis nunc dolor, ac pharetra leo lacinia vel. Donec nibh urna, varius in condimentum id, pharetra facilisis ligula. Sed egestas ipsum et gravida condimentum.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nullam ut urna ac lectus tempor lacinia vel nec ligula. Curabitur eget nunc augue. Cras et interdum sapien, a feugiat mi. Quisque eleifend, lectus sit amet pulvinar suscipit, dui sem sagittis ex, sed posuere orci est at libero. Sed ornare augue purus, eget consectetur massa cursus eu. Maecenas ut enim a odio gravida varius semper at libero. Phasellus tincidunt magna arcu, eget imperdiet ligula aliquam nec.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Suspendisse viverra nulla et mauris sodales dictum. Donec id ligula purus. Sed id tellus justo. Suspendisse gravida dapibus enim ac auctor. Sed pretium semper metus a suscipit. Nulla tortor risus, ornare in congue non, malesuada ut magna. Quisque lacinia, massa a vehicula molestie, mi nisi vestibulum enim, sit amet molestie nibh neque in orci. Sed tempus consectetur placerat. Sed in mauris id tortor pulvinar dictum ac ac magna. Ut quis ipsum a leo dapibus interdum. Aliquam et scelerisque turpis, nec vestibulum mauris. Morbi pretium nisi non lectus dictum, nec auctor turpis maximus. Aliquam consequat fringilla odio vitae varius.</p>', NULL, NULL),
(21, 81, 1, '2020-03-04 20:22:19', '2020-03-04 20:22:19', '0f4a0f41-3f58-482b-b585-ee11e7ef861c', NULL, NULL, '<p>Would you look at that!</p>', 'This is the 2nd page that uses this template!'),
(22, 82, 1, '2020-03-04 20:22:19', '2020-03-04 20:22:19', 'a80784e7-a3b8-4e52-92a3-a859725cc1bc', 'The two sections have been switched around!', '<p><span style=\"color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-align:justify;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);float:none;\">In euismod odio a malesuada interdum. Aenean scelerisque dolor turpis, a aliquam massa porta sit amet. Mauris vel ultrices lacus. Integer rutrum, diam et cursus rhoncus, dui orci interdum lacus, quis congue diam justo vel nisi. Integer iaculis eleifend ipsum non rhoncus. Donec eget metus vitae libero viverra molestie. Phasellus semper velit in mattis vestibulum. Aliquam pharetra mi purus, a consectetur eros vestibulum a. Praesent eu aliquet felis. Sed maximus ultricies interdum. Donec sodales pretium scelerisque. Vestibulum sit amet velit ac risus dapibus aliquam quis non neque.</span> </p>', NULL, NULL),
(23, 84, 1, '2020-03-04 20:23:23', '2020-03-04 20:23:23', 'a22367a0-107f-4abb-bd67-277b20b65490', NULL, NULL, '<p>Would you look at that!</p><p><br /></p><blockquote><p><span style=\"color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-align:justify;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);float:none;\">In euismod odio a malesuada interdum. Aenean scelerisque dolor turpis, a aliquam massa porta sit amet. Mauris vel ultrices lacus. Integer rutrum, diam et cursus rhoncus, dui orci interdum lacus, quis congue diam justo vel nisi. Integer iaculis eleifend ipsum non rhoncus. Donec eget metus vitae libero viverra molestie. Phasellus semper velit in mattis vestibulum. Aliquam pharetra mi purus, a consectetur eros vestibulum a. Praesent eu aliquet felis. Sed maximus ultricies interdum. Donec sodales pretium scelerisque. Vestibulum sit amet velit ac risus dapibus aliquam quis non neque.</span></p></blockquote>', 'This is the 2nd page that uses this template!'),
(24, 85, 1, '2020-03-04 20:23:23', '2020-03-04 20:23:23', '9c2432ff-5194-4420-b063-9fd2a70fa15b', 'The two sections have been switched around!', '<p><span style=\"color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-align:justify;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);float:none;\"><strong>In euismod odio a malesuada interdum. Aenean scelerisque dolor turpis, a aliquam massa porta sit amet. Mauris vel ultrices lacus. Integer rutrum, diam et cursus rhoncus, dui orci interdum lacus, quis congue diam justo vel nisi. Integer iaculis eleifend ipsum non rhoncus. Donec eget metus vitae libero viverra molestie. Phasellus semper velit in mattis vestibulum. Aliquam pharetra mi purus, a consectetur eros vestibulum a. Praesent eu aliquet felis. Sed maximus ultricies interdum. Donec sodales pretium scelerisque. Vestibulum sit amet velit ac risus dapibus aliquam quis non neque.</strong></span></p>', NULL, NULL),
(25, 87, 1, '2020-03-04 20:24:25', '2020-03-04 20:24:25', '6a8f0406-3cbd-4aae-8f26-6a595eb789dd', NULL, NULL, '<p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras elementum urna tincidunt sem hendrerit pretium. Vivamus in sapien pretium, rhoncus orci eget, suscipit diam. Nunc viverra lorem mauris, et aliquet odio luctus nec. Duis scelerisque tellus nec leo condimentum, id iaculis neque porttitor. Etiam quis faucibus orci. Curabitur pharetra dictum maximus. Quisque in imperdiet tellus, ac porta enim. Suspendisse rhoncus convallis neque, ac volutpat est cursus et. Sed eget lacus eget nunc ornare feugiat. Pellentesque vel ante quis lectus fermentum sodales. Curabitur orci felis, varius eget volutpat non, interdum sed nulla. Proin eu congue sapien, non euismod nisi. Nam lorem libero, ullamcorper vitae gravida luctus, mattis eget orci.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Fusce ac sapien a nunc ultrices interdum vitae at erat. Mauris convallis sagittis euismod. Vestibulum porttitor, mi in finibus pharetra, ex nulla lobortis leo, luctus vestibulum diam erat nec libero. Maecenas ornare accumsan commodo. Fusce molestie malesuada nulla in varius. Aenean mollis eros interdum sem molestie posuere. Duis fermentum tincidunt bibendum. Integer dignissim ullamcorper viverra. Aenean eget enim dignissim, hendrerit ex nec, volutpat tellus. Maecenas venenatis nunc dolor, ac pharetra leo lacinia vel. Donec nibh urna, varius in condimentum id, pharetra facilisis ligula. Sed egestas ipsum et gravida condimentum.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nullam ut urna ac lectus tempor lacinia vel nec ligula. Curabitur eget nunc augue. Cras et interdum sapien, a feugiat mi. Quisque eleifend, lectus sit amet pulvinar suscipit, dui sem sagittis ex, sed posuere orci est at libero. Sed ornare augue purus, eget consectetur massa cursus eu. Maecenas ut enim a odio gravida varius semper at libero. Phasellus tincidunt magna arcu, eget imperdiet ligula aliquam nec.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Suspendisse viverra nulla et mauris sodales dictum. Donec id ligula purus. Sed id tellus justo. Suspendisse gravida dapibus enim ac auctor. Sed pretium semper metus a suscipit. Nulla tortor risus, ornare in congue non, malesuada ut magna. Quisque lacinia, massa a vehicula molestie, mi nisi vestibulum enim, sit amet molestie nibh neque in orci. Sed tempus consectetur placerat. Sed in mauris id tortor pulvinar dictum ac ac magna. Ut quis ipsum a leo dapibus interdum. Aliquam et scelerisque turpis, nec vestibulum mauris. Morbi pretium nisi non lectus dictum, nec auctor turpis maximus. Aliquam consequat fringilla odio vitae varius.</p><blockquote>Wow!</blockquote>', 'This is the 2nd page that uses this template!'),
(26, 88, 1, '2020-03-04 20:24:25', '2020-03-04 20:24:25', '5d7404a3-4c6c-4925-8965-e68ef18b1d06', 'The two sections have been switched around!', '<p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras elementum urna tincidunt sem hendrerit pretium. Vivamus in sapien pretium, rhoncus orci eget, suscipit diam. Nunc viverra lorem mauris, et aliquet odio luctus nec. Duis scelerisque tellus nec leo condimentum, id iaculis neque porttitor. Etiam quis faucibus orci. Curabitur pharetra dictum maximus. Quisque in imperdiet tellus, ac porta enim. Suspendisse rhoncus convallis neque, ac volutpat est cursus et. Sed eget lacus eget nunc ornare feugiat. Pellentesque vel ante quis lectus fermentum sodales. Curabitur orci felis, varius eget volutpat non, interdum sed nulla. Proin eu congue sapien, non euismod nisi. Nam lorem libero, ullamcorper vitae gravida luctus, mattis eget orci.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Fusce ac sapien a nunc ultrices interdum vitae at erat. Mauris convallis sagittis euismod. Vestibulum porttitor, mi in finibus pharetra, ex nulla lobortis leo, luctus vestibulum diam erat nec libero. Maecenas ornare accumsan commodo. Fusce molestie malesuada nulla in varius. Aenean mollis eros interdum sem molestie posuere. Duis fermentum tincidunt bibendum. Integer dignissim ullamcorper viverra. Aenean eget enim dignissim, hendrerit ex nec, volutpat tellus. Maecenas venenatis nunc dolor, ac pharetra leo lacinia vel. Donec nibh urna, varius in condimentum id, pharetra facilisis ligula. Sed egestas ipsum et gravida condimentum.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Nullam ut urna ac lectus tempor lacinia vel nec ligula. Curabitur eget nunc augue. Cras et interdum sapien, a feugiat mi. Quisque eleifend, lectus sit amet pulvinar suscipit, dui sem sagittis ex, sed posuere orci est at libero. Sed ornare augue purus, eget consectetur massa cursus eu. Maecenas ut enim a odio gravida varius semper at libero. Phasellus tincidunt magna arcu, eget imperdiet ligula aliquam nec.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;color:rgb(0,0,0);font-family:\'Open Sans\', Arial, sans-serif;font-size:14px;font-style:normal;font-weight:400;letter-spacing:normal;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);\">Suspendisse viverra nulla et mauris sodales dictum. Donec id ligula purus. Sed id tellus justo. Suspendisse gravida dapibus enim ac auctor. Sed pretium semper metus a suscipit. Nulla tortor risus, ornare in congue non, malesuada ut magna. Quisque lacinia, massa a vehicula molestie, mi nisi vestibulum enim, sit amet molestie nibh neque in orci. Sed tempus consectetur placerat. Sed in mauris id tortor pulvinar dictum ac ac magna. Ut quis ipsum a leo dapibus interdum. Aliquam et scelerisque turpis, nec vestibulum mauris. Morbi pretium nisi non lectus dictum, nec auctor turpis maximus. Aliquam consequat fringilla odio vitae varius.</p>', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(11) NOT NULL,
  `pluginId` int(11) DEFAULT NULL,
  `type` enum('app','plugin','content') NOT NULL DEFAULT 'app',
  `name` varchar(255) NOT NULL,
  `applyTime` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `pluginId`, `type`, `name`, `applyTime`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, NULL, 'app', 'Install', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'ae84ee16-2a0c-4863-8573-92f7a2148ddc'),
(2, NULL, 'app', 'm150403_183908_migrations_table_changes', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '4461ce47-b2cd-47a4-9dd0-f30992ef29e6'),
(3, NULL, 'app', 'm150403_184247_plugins_table_changes', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'c69eef4f-0d0a-4bdc-9f9c-c607d6340d11'),
(4, NULL, 'app', 'm150403_184533_field_version', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'e51ed87c-329c-4f97-8ff3-186f4c8c12ed'),
(5, NULL, 'app', 'm150403_184729_type_columns', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '14b706a9-284f-49e4-9500-a2a0fda993d5'),
(6, NULL, 'app', 'm150403_185142_volumes', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'abd4d7e8-2867-4c04-b669-3f925103df3b'),
(7, NULL, 'app', 'm150428_231346_userpreferences', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '6b97ad12-708b-4dbd-ab8b-dc9fabe94212'),
(8, NULL, 'app', 'm150519_150900_fieldversion_conversion', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'ab24c7b3-da55-427b-aac2-32b934ad0b18'),
(9, NULL, 'app', 'm150617_213829_update_email_settings', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '677d8a09-c417-4a9d-9fee-67c53afab352'),
(10, NULL, 'app', 'm150721_124739_templatecachequeries', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'b905e3d9-a5f3-4db2-b6e4-f76b25b5db2f'),
(11, NULL, 'app', 'm150724_140822_adjust_quality_settings', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '8a206d6f-e441-45f9-8d9d-852f493fdd27'),
(12, NULL, 'app', 'm150815_133521_last_login_attempt_ip', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '11221ad2-aa44-4d43-84fc-a1b39725b745'),
(13, NULL, 'app', 'm151002_095935_volume_cache_settings', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '435c7e22-1d34-4ed0-b084-d6b1f048ce39'),
(14, NULL, 'app', 'm151005_142750_volume_s3_storage_settings', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'a08f3bf3-f0b3-4888-b055-4d2653bc9721'),
(15, NULL, 'app', 'm151016_133600_delete_asset_thumbnails', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '16273e22-a290-4af8-be8a-8369bdda951b'),
(16, NULL, 'app', 'm151209_000000_move_logo', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '9f53b121-4c94-4a78-ad0f-4a7ef8405ea7'),
(17, NULL, 'app', 'm151211_000000_rename_fileId_to_assetId', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'bc592aed-92be-422d-89f2-eefd56e08524'),
(18, NULL, 'app', 'm151215_000000_rename_asset_permissions', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '9b40b90e-0568-4ccc-9ee1-46c1b4c6d757'),
(19, NULL, 'app', 'm160707_000001_rename_richtext_assetsource_setting', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'e912c50c-3dfa-4ff0-950d-6811613a3632'),
(20, NULL, 'app', 'm160708_185142_volume_hasUrls_setting', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'b0300f7f-e1d6-42fb-b512-d6453e37fc1d'),
(21, NULL, 'app', 'm160714_000000_increase_max_asset_filesize', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '20a74c3d-c31f-4d02-8f3e-18694e0a3314'),
(22, NULL, 'app', 'm160727_194637_column_cleanup', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'ec2ddba6-faac-4064-b62a-1126dcb5ccd1'),
(23, NULL, 'app', 'm160804_110002_userphotos_to_assets', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '92c538d0-2712-45a8-bd8a-32ae1b27c15c'),
(24, NULL, 'app', 'm160807_144858_sites', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '3064fa9a-6ecd-45b7-9e33-656ee1496733'),
(25, NULL, 'app', 'm160829_000000_pending_user_content_cleanup', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '575dc084-c4d6-4577-858f-a7837fc53cb9'),
(26, NULL, 'app', 'm160830_000000_asset_index_uri_increase', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'fc722763-e70c-427b-a0b2-1976094c0155'),
(27, NULL, 'app', 'm160912_230520_require_entry_type_id', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '1e918e00-650e-49ad-a0b4-2a1c30cc9948'),
(28, NULL, 'app', 'm160913_134730_require_matrix_block_type_id', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '722fcb71-27ef-482b-8772-d790661ff4a5'),
(29, NULL, 'app', 'm160920_174553_matrixblocks_owner_site_id_nullable', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '89eb6828-6665-4cf0-a4dd-a64b88170bac'),
(30, NULL, 'app', 'm160920_231045_usergroup_handle_title_unique', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '7e040e67-7e2c-4948-a5f1-960170767919'),
(31, NULL, 'app', 'm160925_113941_route_uri_parts', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '15ebc168-62fe-41e5-a66d-315c3d41c634'),
(32, NULL, 'app', 'm161006_205918_schemaVersion_not_null', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'cab8c382-2266-47a5-bf33-fd4fa3f7cb0a'),
(33, NULL, 'app', 'm161007_130653_update_email_settings', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '84a6ed7e-331a-4e9c-a4df-847f0ae83636'),
(34, NULL, 'app', 'm161013_175052_newParentId', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'edf9c9ed-2a6b-4b09-8dae-9aad9570e264'),
(35, NULL, 'app', 'm161021_102916_fix_recent_entries_widgets', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'ecb3159b-621f-4ac4-8b1e-0b61576d37fc'),
(36, NULL, 'app', 'm161021_182140_rename_get_help_widget', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '50673a84-4799-4c7f-8cd8-121784a30e0a'),
(37, NULL, 'app', 'm161025_000000_fix_char_columns', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'f295d662-294b-4e7a-b516-19ee3ecb106c'),
(38, NULL, 'app', 'm161029_124145_email_message_languages', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '085e6f0e-85cf-4979-a1f3-ea71388534d0'),
(39, NULL, 'app', 'm161108_000000_new_version_format', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'fd982bd6-c0bd-4bf0-958c-2c5b9439e899'),
(40, NULL, 'app', 'm161109_000000_index_shuffle', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '4f6b0278-9e9a-4dc4-b404-c1053399c489'),
(41, NULL, 'app', 'm161122_185500_no_craft_app', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '8a514e82-ea88-432e-bee2-46418ba8b73f'),
(42, NULL, 'app', 'm161125_150752_clear_urlmanager_cache', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '1cab586c-ac70-4b4e-86cf-dd7e692f2323'),
(43, NULL, 'app', 'm161220_000000_volumes_hasurl_notnull', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '40cb8629-cf62-4101-8392-0de7027984db'),
(44, NULL, 'app', 'm170114_161144_udates_permission', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'bcbd57b5-c48c-407f-996d-62d80ba6b180'),
(45, NULL, 'app', 'm170120_000000_schema_cleanup', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '815f6cdd-b917-47a3-81b0-51f10ae72589'),
(46, NULL, 'app', 'm170126_000000_assets_focal_point', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '781aa41b-5671-4803-8b87-cdd91f2f4361'),
(47, NULL, 'app', 'm170206_142126_system_name', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '9d710d98-6844-4aeb-9e43-264c08c22489'),
(48, NULL, 'app', 'm170217_044740_category_branch_limits', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '1bb32709-2898-4b88-aeef-48deaeef750c'),
(49, NULL, 'app', 'm170217_120224_asset_indexing_columns', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2ba66f24-1252-412b-b7f5-3fe29d06adcf'),
(50, NULL, 'app', 'm170223_224012_plain_text_settings', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'f2c45374-1989-4337-8485-5701dcdb817a'),
(51, NULL, 'app', 'm170227_120814_focal_point_percentage', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '45b8ab73-5f6f-424a-a1c3-e3975c720220'),
(52, NULL, 'app', 'm170228_171113_system_messages', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2a587d45-1881-4896-b54e-dbffc5731aca'),
(53, NULL, 'app', 'm170303_140500_asset_field_source_settings', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '3c280955-c32e-452d-9def-fd3105d72d8b'),
(54, NULL, 'app', 'm170306_150500_asset_temporary_uploads', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '37d76fb6-2ade-437a-bb21-ed1e48e46054'),
(55, NULL, 'app', 'm170523_190652_element_field_layout_ids', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '3a3cfecf-2314-4963-971f-fb9b949f39fc'),
(56, NULL, 'app', 'm170612_000000_route_index_shuffle', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '6c13ca28-f77d-4927-9111-4d7a3a1e40e3'),
(57, NULL, 'app', 'm170621_195237_format_plugin_handles', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '8236c425-b81c-48aa-9237-fa3442b5fe20'),
(58, NULL, 'app', 'm170630_161027_deprecation_line_nullable', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '47a57ec3-71aa-41fd-a419-3a1d2786dfd9'),
(59, NULL, 'app', 'm170630_161028_deprecation_changes', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'fe1ce57f-8500-4d5e-a28a-58bbdf72caac'),
(60, NULL, 'app', 'm170703_181539_plugins_table_tweaks', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '775deedd-c316-43ac-9a9e-9471f7f403ff'),
(61, NULL, 'app', 'm170704_134916_sites_tables', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'f564c58c-919e-4e3c-89a7-08b5cc3ee89c'),
(62, NULL, 'app', 'm170706_183216_rename_sequences', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'b36d0e3f-994e-4ba1-afef-0924cf47688a'),
(63, NULL, 'app', 'm170707_094758_delete_compiled_traits', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'f6edcb3a-4076-4c7e-bf67-a4169f0c50c3'),
(64, NULL, 'app', 'm170731_190138_drop_asset_packagist', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '37bd6866-bf42-4344-9b47-0f27ae22805d'),
(65, NULL, 'app', 'm170810_201318_create_queue_table', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'e82501c2-4d66-4f8d-a8db-f59e23a1a8a6'),
(66, NULL, 'app', 'm170903_192801_longblob_for_queue_jobs', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '99ee7856-c57c-4abd-839d-6cc72301637c'),
(67, NULL, 'app', 'm170914_204621_asset_cache_shuffle', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '1edd4dd5-8c24-411f-96f8-54e1cf98497b'),
(68, NULL, 'app', 'm171011_214115_site_groups', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'f8f0455d-cd9c-4b15-9583-3cfc75eea232'),
(69, NULL, 'app', 'm171012_151440_primary_site', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '0675d59f-d3f5-46b7-a1e7-eed93e20d061'),
(70, NULL, 'app', 'm171013_142500_transform_interlace', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'dee222ee-a3ba-4f88-a4a5-dee803b0c37a'),
(71, NULL, 'app', 'm171016_092553_drop_position_select', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'f44b1213-49a1-4ffd-bcfb-43cde181a517'),
(72, NULL, 'app', 'm171016_221244_less_strict_translation_method', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'cfa9d398-9a2b-49f0-a07f-0dd79890e28b'),
(73, NULL, 'app', 'm171107_000000_assign_group_permissions', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '0b7008f1-9225-4080-8330-cc21155327c3'),
(74, NULL, 'app', 'm171117_000001_templatecache_index_tune', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'd47c95c7-8121-4ee5-989e-cf73a1225860'),
(75, NULL, 'app', 'm171126_105927_disabled_plugins', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '7f393c1c-a6ec-4ac1-8d56-7b25901e0b5f'),
(76, NULL, 'app', 'm171130_214407_craftidtokens_table', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '58bb035d-a668-4fa9-b9f3-ae19543e66a8'),
(77, NULL, 'app', 'm171202_004225_update_email_settings', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'ce6fcd65-2669-4ea8-bbb6-4e31de4db47f'),
(78, NULL, 'app', 'm171204_000001_templatecache_index_tune_deux', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'f591641a-fd18-41db-9fb0-d0f1dabab5ec'),
(79, NULL, 'app', 'm171205_130908_remove_craftidtokens_refreshtoken_column', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'fac254f8-125d-4cf7-b034-d68c561db7f0'),
(80, NULL, 'app', 'm171218_143135_longtext_query_column', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'a8dd0954-56e4-4d9f-8967-4972ed959135'),
(81, NULL, 'app', 'm171231_055546_environment_variables_to_aliases', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'da8a5caa-8c0a-49dc-a045-a09e80cc8839'),
(82, NULL, 'app', 'm180113_153740_drop_users_archived_column', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '8d2e7a03-3426-4cd9-ae29-32b84a92fdf5'),
(83, NULL, 'app', 'm180122_213433_propagate_entries_setting', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'fe6ce16b-381a-4912-8441-7890513a8f8d'),
(84, NULL, 'app', 'm180124_230459_fix_propagate_entries_values', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2d67db8b-2c51-4a83-8b49-610d19b7b7d4'),
(85, NULL, 'app', 'm180128_235202_set_tag_slugs', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '48217c0b-d9e1-4a9b-ac14-0785cdf299a0'),
(86, NULL, 'app', 'm180202_185551_fix_focal_points', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '52b832f4-5df1-4d3d-93c9-ca8d901eb39e'),
(87, NULL, 'app', 'm180217_172123_tiny_ints', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'a6cb53b8-aef9-49ef-a416-905cde214339'),
(88, NULL, 'app', 'm180321_233505_small_ints', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '94bcc514-9c93-4c64-b0ce-ad75bd5b16ac'),
(89, NULL, 'app', 'm180328_115523_new_license_key_statuses', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '8dd93474-aaa9-45f9-902c-262e27794da5'),
(90, NULL, 'app', 'm180404_182320_edition_changes', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2abb7f1f-fe26-4cf4-a9f5-5bf960a65b0c'),
(91, NULL, 'app', 'm180411_102218_fix_db_routes', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '87928236-6f0a-4202-b6f2-89f973e051c2'),
(92, NULL, 'app', 'm180416_205628_resourcepaths_table', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'bea8a708-4c9c-44bc-9967-477975411f12'),
(93, NULL, 'app', 'm180418_205713_widget_cleanup', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '4a8d8483-0f25-4fec-8c4c-be1e744d6cfb'),
(94, NULL, 'app', 'm180425_203349_searchable_fields', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '40bf59dd-4d04-4f1e-95ef-0ef75a24c1c5'),
(95, NULL, 'app', 'm180516_153000_uids_in_field_settings', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2daf8129-4cf6-4035-a30d-36ec1be7b048'),
(96, NULL, 'app', 'm180517_173000_user_photo_volume_to_uid', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '9c633640-1f3e-4e13-b8cc-a8c43540023d'),
(97, NULL, 'app', 'm180518_173000_permissions_to_uid', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'ad468a67-8e22-4ac1-9abf-57806577da9f'),
(98, NULL, 'app', 'm180520_173000_matrix_context_to_uids', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'e9413708-ea3e-42e5-ac44-2ae7761c1b4e'),
(99, NULL, 'app', 'm180521_172900_project_config_table', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '17968cbc-c3c4-40e6-9cce-e3a00e6d106b'),
(100, NULL, 'app', 'm180521_173000_initial_yml_and_snapshot', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'ceb5e296-85f8-421f-8150-0545e817b01e'),
(101, NULL, 'app', 'm180731_162030_soft_delete_sites', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '4c233e42-50f9-4858-b3eb-355e5a7ef290'),
(102, NULL, 'app', 'm180810_214427_soft_delete_field_layouts', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'cc820d83-14ac-4026-bd4f-be5bf57ed78a'),
(103, NULL, 'app', 'm180810_214439_soft_delete_elements', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'f269e2fa-e12b-4f21-a8be-22c93e9a2d6b'),
(104, NULL, 'app', 'm180824_193422_case_sensitivity_fixes', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'fb8d9761-192f-4c7c-bb25-4418abde22e1'),
(105, NULL, 'app', 'm180901_151639_fix_matrixcontent_tables', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '59d5df7b-a7e3-4305-bb63-2def9f87eba6'),
(106, NULL, 'app', 'm180904_112109_permission_changes', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '1a317bb2-76f6-48b4-85f3-e5fea6f7e604'),
(107, NULL, 'app', 'm180910_142030_soft_delete_sitegroups', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '9f55d282-8ae6-43cc-b888-9357a18a209e'),
(108, NULL, 'app', 'm181011_160000_soft_delete_asset_support', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '0d19be36-369c-4992-9d5e-faa75045b4cf'),
(109, NULL, 'app', 'm181016_183648_set_default_user_settings', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '5f7c0db6-cbb2-4597-ba78-f550ec0240ce'),
(110, NULL, 'app', 'm181017_225222_system_config_settings', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '01ce7fb4-7f70-4cf6-93cf-6fb3eb192233'),
(111, NULL, 'app', 'm181018_222343_drop_userpermissions_from_config', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '95fc7508-4341-465b-9d12-1cea8b6877af'),
(112, NULL, 'app', 'm181029_130000_add_transforms_routes_to_config', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'ff052c19-1516-48ac-8cfc-9f32ee8809bc'),
(113, NULL, 'app', 'm181112_203955_sequences_table', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'c6ed8a04-5af3-4cde-9ac8-9e1b20732d81'),
(114, NULL, 'app', 'm181121_001712_cleanup_field_configs', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '41b9a656-2f00-4128-8dc3-e2c72c83a617'),
(115, NULL, 'app', 'm181128_193942_fix_project_config', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '40fc217a-4e7f-4971-8bee-5351b190fabb'),
(116, NULL, 'app', 'm181130_143040_fix_schema_version', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '9a370017-776d-4d9d-b3f4-13f0b28acdb3'),
(117, NULL, 'app', 'm181211_143040_fix_entry_type_uids', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'b43b1fe0-2a03-4ee4-aeb7-6ed426e3556c'),
(118, NULL, 'app', 'm181213_102500_config_map_aliases', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '634a1e8f-0196-417b-b802-6a950a2f0a6a'),
(119, NULL, 'app', 'm181217_153000_fix_structure_uids', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'bbe09125-8f16-4796-a05d-66cceaae0f8e'),
(120, NULL, 'app', 'm190104_152725_store_licensed_plugin_editions', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'c01bde98-aa36-45aa-85de-b02442c69a1c'),
(121, NULL, 'app', 'm190108_110000_cleanup_project_config', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '87b35d30-5131-4a0f-9f8f-3d1b8274204f'),
(122, NULL, 'app', 'm190108_113000_asset_field_setting_change', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'ccd355fa-bb9a-4876-9003-f79276daebde'),
(123, NULL, 'app', 'm190109_172845_fix_colspan', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'f3dea288-6001-4999-89ff-683e26a564d3'),
(124, NULL, 'app', 'm190110_150000_prune_nonexisting_sites', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '6892ba66-eaa0-4a0f-bfcf-3b30239987ba'),
(125, NULL, 'app', 'm190110_214819_soft_delete_volumes', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'ed12a154-9449-4cc0-b5ed-47090589a3b4'),
(126, NULL, 'app', 'm190112_124737_fix_user_settings', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '5ecd6f1f-99b5-4cb2-9d5b-46ca46fab639'),
(127, NULL, 'app', 'm190112_131225_fix_field_layouts', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '92e81de6-0713-42af-9d6b-76b1c74b3dba'),
(128, NULL, 'app', 'm190112_201010_more_soft_deletes', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '6c0ef5fe-5598-41a3-9aa2-24a071fdbff5'),
(129, NULL, 'app', 'm190114_143000_more_asset_field_setting_changes', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2825639f-0c49-4e70-ac22-c7a23c1ed8dc'),
(130, NULL, 'app', 'm190121_120000_rich_text_config_setting', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '4c7b2fcf-dd28-4dd6-a5e9-f3654c7936ec'),
(131, NULL, 'app', 'm190125_191628_fix_email_transport_password', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '4391abfe-77a2-4b27-845a-bc5099f0183b'),
(132, NULL, 'app', 'm190128_181422_cleanup_volume_folders', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '058aba95-5898-493b-941c-d092816ab184'),
(133, NULL, 'app', 'm190205_140000_fix_asset_soft_delete_index', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '4a543515-b5a4-4490-8d58-076099bf410c'),
(134, NULL, 'app', 'm190208_140000_reset_project_config_mapping', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '5f3e0fb0-d468-45f4-b565-92df554d3b6c'),
(135, NULL, 'app', 'm190218_143000_element_index_settings_uid', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'cc005cd6-d6ef-4348-935f-809e28190c69'),
(136, NULL, 'app', 'm190312_152740_element_revisions', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '77d7cb8c-eb9e-48dc-924c-580097bc5049'),
(137, NULL, 'app', 'm190327_235137_propagation_method', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'b358528a-8c32-4fea-89ed-60b8e25e790b'),
(138, NULL, 'app', 'm190401_223843_drop_old_indexes', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '32d57829-2c13-4da2-bee8-b956ff1c12cd'),
(139, NULL, 'app', 'm190416_014525_drop_unique_global_indexes', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'd8dcaca5-a7cf-444e-a214-23b82510ad9d'),
(140, NULL, 'app', 'm190417_085010_add_image_editor_permissions', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '65a151d9-a724-4551-84ed-bb7cd6a44215'),
(141, NULL, 'app', 'm190502_122019_store_default_user_group_uid', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '4a4e4e38-7842-468b-ba3b-452638f81055'),
(142, NULL, 'app', 'm190504_150349_preview_targets', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '114c84a5-093d-405c-a083-fade96f04d2e'),
(143, NULL, 'app', 'm190516_184711_job_progress_label', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '323c3971-a058-41be-854a-f08757924a04'),
(144, NULL, 'app', 'm190523_190303_optional_revision_creators', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'bd380a7e-63fa-496d-b0a7-4b86450dc6bc'),
(145, NULL, 'app', 'm190529_204501_fix_duplicate_uids', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'e3c45cf6-83f0-4e0c-853e-efdbceba2c89'),
(146, NULL, 'app', 'm190605_223807_unsaved_drafts', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'a1423f81-9aad-4460-bab7-e10dd0eb7243'),
(147, NULL, 'app', 'm190607_230042_entry_revision_error_tables', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '50fcafe3-33c0-4648-807d-2a6fd4a41193'),
(148, NULL, 'app', 'm190608_033429_drop_elements_uid_idx', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '6284299f-3d06-4fc5-b7dc-1695175c08cf'),
(149, NULL, 'app', 'm190617_164400_add_gqlschemas_table', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '17945df3-2bd5-4cff-a223-757150f13c09'),
(150, NULL, 'app', 'm190624_234204_matrix_propagation_method', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '47132ce8-16ed-42dd-80fd-bb0b7be79a39'),
(151, NULL, 'app', 'm190711_153020_drop_snapshots', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'e3c3009f-fd15-432d-9c4f-8bbbc161dc07'),
(152, NULL, 'app', 'm190712_195914_no_draft_revisions', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'f3eac28c-d959-466f-aa48-ef3f9ccaf099'),
(153, NULL, 'app', 'm190723_140314_fix_preview_targets_column', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '202c598c-724b-4225-8234-4460a50048bb'),
(154, NULL, 'app', 'm190820_003519_flush_compiled_templates', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '3b6947c1-b609-43a5-815c-26d0a440aa5b'),
(155, NULL, 'app', 'm190823_020339_optional_draft_creators', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '0bdfd787-df2e-4b87-82d3-62638719da83'),
(156, NULL, 'app', 'm190913_152146_update_preview_targets', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '330eda86-9aef-4066-8a5d-f58e3ee971a2'),
(157, NULL, 'app', 'm191107_122000_add_gql_project_config_support', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'c12a0a08-e667-4766-8a0f-a105a36f101a'),
(158, NULL, 'app', 'm191204_085100_pack_savable_component_settings', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '86357f6f-5dfc-4eae-8f65-95817c124486'),
(159, NULL, 'app', 'm191206_001148_change_tracking', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '7f15ad3c-a867-49db-a6f7-af876449d52b'),
(160, NULL, 'app', 'm191216_191635_asset_upload_tracking', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'c1c50558-2a5c-49e2-9933-bc4586a3f039'),
(161, NULL, 'app', 'm191222_002848_peer_asset_permissions', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '5ca97481-d101-41ba-b098-cd908804aa01'),
(162, NULL, 'app', 'm200127_172522_queue_channels', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', 'c2805064-7846-4233-bbfc-870a55de7c5d'),
(163, NULL, 'app', 'm200211_175048_truncate_element_query_cache', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2acd8efa-1628-4caf-af14-6ee5e25a93d8'),
(164, NULL, 'app', 'm200213_172522_new_elements_index', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '5a917bb6-f906-47ad-ae96-865e7cd4aca0'),
(165, NULL, 'app', 'm200228_195211_long_deprecation_messages', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 09:23:47', '859fb5eb-4f2d-4334-b363-cf3ae85485f1');

-- --------------------------------------------------------

--
-- Table structure for table `plugins`
--

CREATE TABLE `plugins` (
  `id` int(11) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `schemaVersion` varchar(255) NOT NULL,
  `licenseKeyStatus` enum('valid','invalid','mismatched','astray','unknown') NOT NULL DEFAULT 'unknown',
  `licensedEdition` varchar(255) DEFAULT NULL,
  `installDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plugins`
--

INSERT INTO `plugins` (`id`, `handle`, `version`, `schemaVersion`, `licenseKeyStatus`, `licensedEdition`, `installDate`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'froala-editor', '3.1.0', '1.0.0', 'unknown', NULL, '2020-03-04 18:53:39', '2020-03-04 18:53:39', '2020-03-04 18:53:44', '034e7333-ce23-49b4-a012-eaa20b7c52f0');

-- --------------------------------------------------------

--
-- Table structure for table `projectconfig`
--

CREATE TABLE `projectconfig` (
  `path` varchar(255) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projectconfig`
--

INSERT INTO `projectconfig` (`path`, `value`) VALUES
('dateModified', '1583353209'),
('email.fromEmail', '\"vanleipsigpatrick@gmail.com\"'),
('email.fromName', '\"Brik\"'),
('email.transportType', '\"craft\\\\mail\\\\transportadapters\\\\Sendmail\"'),
('fieldGroups.071a28b4-3792-4ece-813d-89a2d7c01022.name', '\"Common\"'),
('fields.a74dc681-f6a1-4f6b-9351-705706dc6382.contentColumnType', '\"string\"'),
('fields.a74dc681-f6a1-4f6b-9351-705706dc6382.fieldGroup', '\"071a28b4-3792-4ece-813d-89a2d7c01022\"'),
('fields.a74dc681-f6a1-4f6b-9351-705706dc6382.handle', '\"modules\"'),
('fields.a74dc681-f6a1-4f6b-9351-705706dc6382.instructions', '\"\"'),
('fields.a74dc681-f6a1-4f6b-9351-705706dc6382.name', '\"Modules\"'),
('fields.a74dc681-f6a1-4f6b-9351-705706dc6382.searchable', 'true'),
('fields.a74dc681-f6a1-4f6b-9351-705706dc6382.settings.contentTable', '\"{{%matrixcontent_modules}}\"'),
('fields.a74dc681-f6a1-4f6b-9351-705706dc6382.settings.maxBlocks', '\"\"'),
('fields.a74dc681-f6a1-4f6b-9351-705706dc6382.settings.minBlocks', '\"\"'),
('fields.a74dc681-f6a1-4f6b-9351-705706dc6382.settings.propagationMethod', '\"all\"'),
('fields.a74dc681-f6a1-4f6b-9351-705706dc6382.translationKeyFormat', 'null'),
('fields.a74dc681-f6a1-4f6b-9351-705706dc6382.translationMethod', '\"site\"'),
('fields.a74dc681-f6a1-4f6b-9351-705706dc6382.type', '\"craft\\\\fields\\\\Matrix\"'),
('imageTransforms.2cd64a25-250b-4eb1-803f-1a9fd25f5a70.format', 'null'),
('imageTransforms.2cd64a25-250b-4eb1-803f-1a9fd25f5a70.handle', '\"x488692\"'),
('imageTransforms.2cd64a25-250b-4eb1-803f-1a9fd25f5a70.height', '692'),
('imageTransforms.2cd64a25-250b-4eb1-803f-1a9fd25f5a70.interlace', '\"none\"'),
('imageTransforms.2cd64a25-250b-4eb1-803f-1a9fd25f5a70.mode', '\"crop\"'),
('imageTransforms.2cd64a25-250b-4eb1-803f-1a9fd25f5a70.name', '\"488x692\"'),
('imageTransforms.2cd64a25-250b-4eb1-803f-1a9fd25f5a70.position', '\"center-center\"'),
('imageTransforms.2cd64a25-250b-4eb1-803f-1a9fd25f5a70.quality', 'null'),
('imageTransforms.2cd64a25-250b-4eb1-803f-1a9fd25f5a70.width', '488'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.field', '\"a74dc681-f6a1-4f6b-9351-705706dc6382\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fieldLayouts.4525a308-dc8c-4ea6-954c-18880c00462d.tabs.0.fields.b35d435d-04fb-48fa-ae07-b14561903b91.required', 'false'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fieldLayouts.4525a308-dc8c-4ea6-954c-18880c00462d.tabs.0.fields.b35d435d-04fb-48fa-ae07-b14561903b91.sortOrder', '1'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fieldLayouts.4525a308-dc8c-4ea6-954c-18880c00462d.tabs.0.fields.eba9d534-08de-41fa-8b06-d827343215ae.required', 'false'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fieldLayouts.4525a308-dc8c-4ea6-954c-18880c00462d.tabs.0.fields.eba9d534-08de-41fa-8b06-d827343215ae.sortOrder', '2'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fieldLayouts.4525a308-dc8c-4ea6-954c-18880c00462d.tabs.0.fields.f5896462-57d8-4023-a12f-a63ae565209a.required', 'false'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fieldLayouts.4525a308-dc8c-4ea6-954c-18880c00462d.tabs.0.fields.f5896462-57d8-4023-a12f-a63ae565209a.sortOrder', '3'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fieldLayouts.4525a308-dc8c-4ea6-954c-18880c00462d.tabs.0.name', '\"Content\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fieldLayouts.4525a308-dc8c-4ea6-954c-18880c00462d.tabs.0.sortOrder', '1'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.b35d435d-04fb-48fa-ae07-b14561903b91.contentColumnType', '\"text\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.b35d435d-04fb-48fa-ae07-b14561903b91.fieldGroup', 'null'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.b35d435d-04fb-48fa-ae07-b14561903b91.handle', '\"imagetitle\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.b35d435d-04fb-48fa-ae07-b14561903b91.instructions', '\"\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.b35d435d-04fb-48fa-ae07-b14561903b91.name', '\"ImageTitle\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.b35d435d-04fb-48fa-ae07-b14561903b91.searchable', 'true'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.b35d435d-04fb-48fa-ae07-b14561903b91.settings.byteLimit', 'null'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.b35d435d-04fb-48fa-ae07-b14561903b91.settings.charLimit', 'null'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.b35d435d-04fb-48fa-ae07-b14561903b91.settings.code', '\"\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.b35d435d-04fb-48fa-ae07-b14561903b91.settings.columnType', 'null'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.b35d435d-04fb-48fa-ae07-b14561903b91.settings.initialRows', '\"4\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.b35d435d-04fb-48fa-ae07-b14561903b91.settings.multiline', '\"\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.b35d435d-04fb-48fa-ae07-b14561903b91.settings.placeholder', '\"\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.b35d435d-04fb-48fa-ae07-b14561903b91.translationKeyFormat', 'null'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.b35d435d-04fb-48fa-ae07-b14561903b91.translationMethod', '\"none\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.b35d435d-04fb-48fa-ae07-b14561903b91.type', '\"craft\\\\fields\\\\PlainText\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.eba9d534-08de-41fa-8b06-d827343215ae.contentColumnType', '\"text\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.eba9d534-08de-41fa-8b06-d827343215ae.fieldGroup', 'null'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.eba9d534-08de-41fa-8b06-d827343215ae.handle', '\"imagetext\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.eba9d534-08de-41fa-8b06-d827343215ae.instructions', '\"\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.eba9d534-08de-41fa-8b06-d827343215ae.name', '\"ImageText\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.eba9d534-08de-41fa-8b06-d827343215ae.searchable', 'true'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.eba9d534-08de-41fa-8b06-d827343215ae.settings.assetsFilesSource', '\"folder:2\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.eba9d534-08de-41fa-8b06-d827343215ae.settings.assetsFilesSubPath', '\"\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.eba9d534-08de-41fa-8b06-d827343215ae.settings.assetsImagesSource', '\"folder:2\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.eba9d534-08de-41fa-8b06-d827343215ae.settings.assetsImagesSubPath', '\"\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.eba9d534-08de-41fa-8b06-d827343215ae.settings.editorConfig', '\"\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.eba9d534-08de-41fa-8b06-d827343215ae.translationKeyFormat', 'null'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.eba9d534-08de-41fa-8b06-d827343215ae.translationMethod', '\"none\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.eba9d534-08de-41fa-8b06-d827343215ae.type', '\"froala\\\\craftfroalawysiwyg\\\\Field\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.contentColumnType', '\"string\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.fieldGroup', 'null'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.handle', '\"images\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.instructions', '\"\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.name', '\"Images\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.searchable', 'true'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.settings.allowedKinds', 'null'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.settings.defaultUploadLocationSource', '\"volume:6f33e3a2-877c-4050-b3ec-b0510164719c\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.settings.defaultUploadLocationSubpath', '\"\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.settings.limit', '\"1\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.settings.localizeRelations', 'false'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.settings.restrictFiles', '\"\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.settings.selectionLabel', '\"\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.settings.showUnpermittedFiles', 'false'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.settings.showUnpermittedVolumes', 'false'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.settings.singleUploadLocationSource', '\"volume:6f33e3a2-877c-4050-b3ec-b0510164719c\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.settings.singleUploadLocationSubpath', '\"\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.settings.source', 'null'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.settings.sources', '\"*\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.settings.targetSiteId', 'null'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.settings.useSingleFolder', 'false'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.settings.validateRelatedElements', '\"\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.settings.viewMode', '\"list\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.translationKeyFormat', 'null'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.translationMethod', '\"site\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.fields.f5896462-57d8-4023-a12f-a63ae565209a.type', '\"craft\\\\fields\\\\Assets\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.handle', '\"defaultdesign\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.name', '\"DefaultDesign\"'),
('matrixBlockTypes.e48af7bb-4282-4f2a-aa9d-5ccdf105bdd9.sortOrder', '1'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.field', '\"a74dc681-f6a1-4f6b-9351-705706dc6382\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fieldLayouts.ef542c16-16ac-4b88-8459-e3ce4af376fa.tabs.0.fields.0550a20d-4cff-48bc-a99b-11f7d0c745a3.required', 'false'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fieldLayouts.ef542c16-16ac-4b88-8459-e3ce4af376fa.tabs.0.fields.0550a20d-4cff-48bc-a99b-11f7d0c745a3.sortOrder', '2'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fieldLayouts.ef542c16-16ac-4b88-8459-e3ce4af376fa.tabs.0.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.required', 'false'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fieldLayouts.ef542c16-16ac-4b88-8459-e3ce4af376fa.tabs.0.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.sortOrder', '3'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fieldLayouts.ef542c16-16ac-4b88-8459-e3ce4af376fa.tabs.0.fields.bd8ed2bd-2beb-411b-b757-a77d36940d8d.required', 'false'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fieldLayouts.ef542c16-16ac-4b88-8459-e3ce4af376fa.tabs.0.fields.bd8ed2bd-2beb-411b-b757-a77d36940d8d.sortOrder', '1'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fieldLayouts.ef542c16-16ac-4b88-8459-e3ce4af376fa.tabs.0.name', '\"Content\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fieldLayouts.ef542c16-16ac-4b88-8459-e3ce4af376fa.tabs.0.sortOrder', '1'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.0550a20d-4cff-48bc-a99b-11f7d0c745a3.contentColumnType', '\"text\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.0550a20d-4cff-48bc-a99b-11f7d0c745a3.fieldGroup', 'null'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.0550a20d-4cff-48bc-a99b-11f7d0c745a3.handle', '\"imagetext\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.0550a20d-4cff-48bc-a99b-11f7d0c745a3.instructions', '\"\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.0550a20d-4cff-48bc-a99b-11f7d0c745a3.name', '\"ImageText\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.0550a20d-4cff-48bc-a99b-11f7d0c745a3.searchable', 'true'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.0550a20d-4cff-48bc-a99b-11f7d0c745a3.settings.assetsFilesSource', '\"folder:2\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.0550a20d-4cff-48bc-a99b-11f7d0c745a3.settings.assetsFilesSubPath', '\"\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.0550a20d-4cff-48bc-a99b-11f7d0c745a3.settings.assetsImagesSource', '\"folder:2\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.0550a20d-4cff-48bc-a99b-11f7d0c745a3.settings.assetsImagesSubPath', '\"\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.0550a20d-4cff-48bc-a99b-11f7d0c745a3.settings.editorConfig', '\"\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.0550a20d-4cff-48bc-a99b-11f7d0c745a3.translationKeyFormat', 'null'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.0550a20d-4cff-48bc-a99b-11f7d0c745a3.translationMethod', '\"none\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.0550a20d-4cff-48bc-a99b-11f7d0c745a3.type', '\"froala\\\\craftfroalawysiwyg\\\\Field\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.contentColumnType', '\"string\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.fieldGroup', 'null'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.handle', '\"images\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.instructions', '\"\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.name', '\"Images\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.searchable', 'true'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.settings.allowedKinds', 'null'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.settings.defaultUploadLocationSource', '\"volume:6f33e3a2-877c-4050-b3ec-b0510164719c\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.settings.defaultUploadLocationSubpath', '\"\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.settings.limit', '\"1\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.settings.localizeRelations', 'false'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.settings.restrictFiles', '\"\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.settings.selectionLabel', '\"\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.settings.showUnpermittedFiles', 'false'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.settings.showUnpermittedVolumes', 'false'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.settings.singleUploadLocationSource', '\"volume:6f33e3a2-877c-4050-b3ec-b0510164719c\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.settings.singleUploadLocationSubpath', '\"\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.settings.source', 'null'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.settings.sources', '\"*\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.settings.targetSiteId', 'null'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.settings.useSingleFolder', 'false'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.settings.validateRelatedElements', '\"\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.settings.viewMode', '\"list\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.translationKeyFormat', 'null'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.translationMethod', '\"site\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.7dfcd464-05e7-4173-8d20-36b21b4051a4.type', '\"craft\\\\fields\\\\Assets\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.bd8ed2bd-2beb-411b-b757-a77d36940d8d.contentColumnType', '\"text\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.bd8ed2bd-2beb-411b-b757-a77d36940d8d.fieldGroup', 'null'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.bd8ed2bd-2beb-411b-b757-a77d36940d8d.handle', '\"imagetitle\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.bd8ed2bd-2beb-411b-b757-a77d36940d8d.instructions', '\"\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.bd8ed2bd-2beb-411b-b757-a77d36940d8d.name', '\"ImageTitle\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.bd8ed2bd-2beb-411b-b757-a77d36940d8d.searchable', 'true'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.bd8ed2bd-2beb-411b-b757-a77d36940d8d.settings.byteLimit', 'null'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.bd8ed2bd-2beb-411b-b757-a77d36940d8d.settings.charLimit', 'null'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.bd8ed2bd-2beb-411b-b757-a77d36940d8d.settings.code', '\"\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.bd8ed2bd-2beb-411b-b757-a77d36940d8d.settings.columnType', 'null'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.bd8ed2bd-2beb-411b-b757-a77d36940d8d.settings.initialRows', '\"4\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.bd8ed2bd-2beb-411b-b757-a77d36940d8d.settings.multiline', '\"\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.bd8ed2bd-2beb-411b-b757-a77d36940d8d.settings.placeholder', '\"\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.bd8ed2bd-2beb-411b-b757-a77d36940d8d.translationKeyFormat', 'null'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.bd8ed2bd-2beb-411b-b757-a77d36940d8d.translationMethod', '\"none\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.fields.bd8ed2bd-2beb-411b-b757-a77d36940d8d.type', '\"craft\\\\fields\\\\PlainText\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.handle', '\"secondarydesign\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.name', '\"SecondaryDesign\"'),
('matrixBlockTypes.ef7681b1-796e-4622-9627-55bab78e30e7.sortOrder', '2'),
('plugins.froala-editor.edition', '\"standard\"'),
('plugins.froala-editor.enabled', 'true'),
('plugins.froala-editor.schemaVersion', '\"1.0.0\"'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.enableVersioning', 'true'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.entryTypes.0d810b10-a6dd-4a5c-8f2d-832860cf5cc0.fieldLayouts.559900ea-3109-46a4-8e4c-7ddc33db6970.tabs.0.fields.a74dc681-f6a1-4f6b-9351-705706dc6382.required', 'false'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.entryTypes.0d810b10-a6dd-4a5c-8f2d-832860cf5cc0.fieldLayouts.559900ea-3109-46a4-8e4c-7ddc33db6970.tabs.0.fields.a74dc681-f6a1-4f6b-9351-705706dc6382.sortOrder', '1'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.entryTypes.0d810b10-a6dd-4a5c-8f2d-832860cf5cc0.fieldLayouts.559900ea-3109-46a4-8e4c-7ddc33db6970.tabs.0.name', '\"Common\"'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.entryTypes.0d810b10-a6dd-4a5c-8f2d-832860cf5cc0.fieldLayouts.559900ea-3109-46a4-8e4c-7ddc33db6970.tabs.0.sortOrder', '1'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.entryTypes.0d810b10-a6dd-4a5c-8f2d-832860cf5cc0.handle', '\"homepage\"'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.entryTypes.0d810b10-a6dd-4a5c-8f2d-832860cf5cc0.hasTitleField', 'false'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.entryTypes.0d810b10-a6dd-4a5c-8f2d-832860cf5cc0.name', '\"Homepage\"'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.entryTypes.0d810b10-a6dd-4a5c-8f2d-832860cf5cc0.sortOrder', '1'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.entryTypes.0d810b10-a6dd-4a5c-8f2d-832860cf5cc0.titleFormat', '\"{section.name|raw}\"'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.entryTypes.0d810b10-a6dd-4a5c-8f2d-832860cf5cc0.titleLabel', '\"\"'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.handle', '\"homepage\"'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.name', '\"Homepage\"'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.previewTargets.0.__assoc__.0.0', '\"label\"'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.previewTargets.0.__assoc__.0.1', '\"Primary entry page\"'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.previewTargets.0.__assoc__.1.0', '\"urlFormat\"'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.previewTargets.0.__assoc__.1.1', '\"{url}\"'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.previewTargets.0.__assoc__.2.0', '\"refresh\"'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.previewTargets.0.__assoc__.2.1', '\"1\"'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.propagationMethod', '\"all\"'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.siteSettings.f7434bb1-a09d-4f73-99dd-1b4a56a4cb49.enabledByDefault', 'true'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.siteSettings.f7434bb1-a09d-4f73-99dd-1b4a56a4cb49.hasUrls', 'true'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.siteSettings.f7434bb1-a09d-4f73-99dd-1b4a56a4cb49.template', '\"\"'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.siteSettings.f7434bb1-a09d-4f73-99dd-1b4a56a4cb49.uriFormat', '\"__home__\"'),
('sections.77418224-69b0-4f59-9d02-e2cfdf838ff1.type', '\"single\"'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.enableVersioning', 'true'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.entryTypes.b77fddb6-e4e7-4ba5-bc16-b9afbbfd2597.fieldLayouts.77e8d53f-6fbf-42ea-b4fc-04408468c5f5.tabs.0.fields.a74dc681-f6a1-4f6b-9351-705706dc6382.required', 'false'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.entryTypes.b77fddb6-e4e7-4ba5-bc16-b9afbbfd2597.fieldLayouts.77e8d53f-6fbf-42ea-b4fc-04408468c5f5.tabs.0.fields.a74dc681-f6a1-4f6b-9351-705706dc6382.sortOrder', '1'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.entryTypes.b77fddb6-e4e7-4ba5-bc16-b9afbbfd2597.fieldLayouts.77e8d53f-6fbf-42ea-b4fc-04408468c5f5.tabs.0.name', '\"Common\"'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.entryTypes.b77fddb6-e4e7-4ba5-bc16-b9afbbfd2597.fieldLayouts.77e8d53f-6fbf-42ea-b4fc-04408468c5f5.tabs.0.sortOrder', '1'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.entryTypes.b77fddb6-e4e7-4ba5-bc16-b9afbbfd2597.handle', '\"testpage\"'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.entryTypes.b77fddb6-e4e7-4ba5-bc16-b9afbbfd2597.hasTitleField', 'false'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.entryTypes.b77fddb6-e4e7-4ba5-bc16-b9afbbfd2597.name', '\"TestPage\"'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.entryTypes.b77fddb6-e4e7-4ba5-bc16-b9afbbfd2597.sortOrder', '1'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.entryTypes.b77fddb6-e4e7-4ba5-bc16-b9afbbfd2597.titleFormat', '\"{section.name|raw}\"'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.entryTypes.b77fddb6-e4e7-4ba5-bc16-b9afbbfd2597.titleLabel', '\"\"'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.handle', '\"testpage\"'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.name', '\"TestPage\"'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.previewTargets.0.__assoc__.0.0', '\"label\"'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.previewTargets.0.__assoc__.0.1', '\"Primary entry page\"'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.previewTargets.0.__assoc__.1.0', '\"urlFormat\"'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.previewTargets.0.__assoc__.1.1', '\"{url}\"'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.previewTargets.0.__assoc__.2.0', '\"refresh\"'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.previewTargets.0.__assoc__.2.1', '\"1\"'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.propagationMethod', '\"all\"'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.siteSettings.f7434bb1-a09d-4f73-99dd-1b4a56a4cb49.enabledByDefault', 'true'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.siteSettings.f7434bb1-a09d-4f73-99dd-1b4a56a4cb49.hasUrls', 'true'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.siteSettings.f7434bb1-a09d-4f73-99dd-1b4a56a4cb49.template', '\"index\"'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.siteSettings.f7434bb1-a09d-4f73-99dd-1b4a56a4cb49.uriFormat', '\"testpage\"'),
('sections.b6bd99cb-623f-43e9-a7bc-7cf3773438cb.type', '\"single\"'),
('siteGroups.d972a572-ba36-4e20-9e4b-bc9f97121a1e.name', '\"Brik\"'),
('sites.f7434bb1-a09d-4f73-99dd-1b4a56a4cb49.baseUrl', '\"$DEFAULT_SITE_URL\"'),
('sites.f7434bb1-a09d-4f73-99dd-1b4a56a4cb49.handle', '\"default\"'),
('sites.f7434bb1-a09d-4f73-99dd-1b4a56a4cb49.hasUrls', 'true'),
('sites.f7434bb1-a09d-4f73-99dd-1b4a56a4cb49.language', '\"en-US\"'),
('sites.f7434bb1-a09d-4f73-99dd-1b4a56a4cb49.name', '\"Brik\"'),
('sites.f7434bb1-a09d-4f73-99dd-1b4a56a4cb49.primary', 'true'),
('sites.f7434bb1-a09d-4f73-99dd-1b4a56a4cb49.siteGroup', '\"d972a572-ba36-4e20-9e4b-bc9f97121a1e\"'),
('sites.f7434bb1-a09d-4f73-99dd-1b4a56a4cb49.sortOrder', '1'),
('system.edition', '\"pro\"'),
('system.live', 'true'),
('system.name', '\"Brik\"'),
('system.schemaVersion', '\"3.4.10\"'),
('system.timeZone', '\"America/Los_Angeles\"'),
('users.allowPublicRegistration', 'false'),
('users.defaultGroup', 'null'),
('users.photoSubpath', '\"\"'),
('users.photoVolumeUid', 'null'),
('users.requireEmailVerification', 'true'),
('volumes.6f33e3a2-877c-4050-b3ec-b0510164719c.handle', '\"images\"'),
('volumes.6f33e3a2-877c-4050-b3ec-b0510164719c.hasUrls', 'true'),
('volumes.6f33e3a2-877c-4050-b3ec-b0510164719c.name', '\"Images\"'),
('volumes.6f33e3a2-877c-4050-b3ec-b0510164719c.settings.path', '\"@webroot/images/uploads\"'),
('volumes.6f33e3a2-877c-4050-b3ec-b0510164719c.sortOrder', '2'),
('volumes.6f33e3a2-877c-4050-b3ec-b0510164719c.type', '\"craft\\\\volumes\\\\Local\"'),
('volumes.6f33e3a2-877c-4050-b3ec-b0510164719c.url', '\"@web/images/uploads\"');

-- --------------------------------------------------------

--
-- Table structure for table `queue`
--

CREATE TABLE `queue` (
  `id` int(11) NOT NULL,
  `channel` varchar(255) NOT NULL DEFAULT 'queue',
  `job` longblob NOT NULL,
  `description` text,
  `timePushed` int(11) NOT NULL,
  `ttr` int(11) NOT NULL,
  `delay` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) UNSIGNED NOT NULL DEFAULT '1024',
  `dateReserved` datetime DEFAULT NULL,
  `timeUpdated` int(11) DEFAULT NULL,
  `progress` smallint(6) NOT NULL DEFAULT '0',
  `progressLabel` varchar(255) DEFAULT NULL,
  `attempt` int(11) DEFAULT NULL,
  `fail` tinyint(1) DEFAULT '0',
  `dateFailed` datetime DEFAULT NULL,
  `error` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `relations`
--

CREATE TABLE `relations` (
  `id` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `sourceSiteId` int(11) DEFAULT NULL,
  `targetId` int(11) NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `relations`
--

INSERT INTO `relations` (`id`, `fieldId`, `sourceId`, `sourceSiteId`, `targetId`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(16, 15, 65, NULL, 63, 1, '2020-03-04 20:03:56', '2020-03-04 20:03:56', '6e6d26ba-2f0b-4a30-af2f-2d9c1f773e9d'),
(17, 15, 38, NULL, 66, 1, '2020-03-04 20:10:40', '2020-03-04 20:10:40', '9f5887f0-f4b5-4adb-9dee-f449b73babd1'),
(18, 15, 68, NULL, 66, 1, '2020-03-04 20:10:40', '2020-03-04 20:10:40', 'fce121c4-9255-4e90-ad31-06b2f1d57671'),
(19, 17, 69, NULL, 63, 1, '2020-03-04 20:18:50', '2020-03-04 20:18:50', 'e3ec28b6-fda6-4560-a0f0-3c39af8e651a'),
(20, 15, 71, NULL, 66, 1, '2020-03-04 20:18:50', '2020-03-04 20:18:50', '02c95671-a061-461b-9fac-ec1952c6d755'),
(21, 17, 72, NULL, 63, 1, '2020-03-04 20:18:50', '2020-03-04 20:18:50', '99b833a2-2f15-44b9-a20d-2fc5c3da98fa'),
(24, 17, 81, NULL, 76, 1, '2020-03-04 20:22:19', '2020-03-04 20:22:19', '810ab857-8714-4eb8-9ff7-8d8e16b0cd43'),
(25, 15, 82, NULL, 77, 1, '2020-03-04 20:22:19', '2020-03-04 20:22:19', '1e469e31-e504-4001-8e6c-c22302ea42f7'),
(26, 17, 78, NULL, 77, 1, '2020-03-04 20:23:23', '2020-03-04 20:23:23', '8bd3c9fc-fa0a-4d90-910d-065ec161e997'),
(27, 15, 79, NULL, 76, 1, '2020-03-04 20:23:23', '2020-03-04 20:23:23', 'e75d4d66-5d94-4e05-ab80-4fa276eaa1fd'),
(28, 17, 84, NULL, 77, 1, '2020-03-04 20:23:23', '2020-03-04 20:23:23', 'cd76c6fe-c075-4588-b7c4-325d7a964ac4'),
(29, 15, 85, NULL, 76, 1, '2020-03-04 20:23:23', '2020-03-04 20:23:23', 'd7eb9eac-c5df-4205-9035-34004d84d3cc'),
(30, 17, 87, NULL, 77, 1, '2020-03-04 20:24:25', '2020-03-04 20:24:25', '9c724a73-d25a-4cb6-9db7-2146d8c6cb54'),
(31, 15, 88, NULL, 76, 1, '2020-03-04 20:24:25', '2020-03-04 20:24:25', 'd06f1cdc-7950-4940-877c-8b32ed2d1c86');

-- --------------------------------------------------------

--
-- Table structure for table `resourcepaths`
--

CREATE TABLE `resourcepaths` (
  `hash` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `resourcepaths`
--

INSERT INTO `resourcepaths` (`hash`, `path`) VALUES
('1e9308b5', '@lib/vue'),
('1ecf4bb8', '@app/web/assets/matrixsettings/dist'),
('245449e6', '@app/web/assets/tablesettings/dist'),
('24604903', '@lib/jquery.payment'),
('29c81196', '@bower/jquery/dist'),
('2d7e27b9', '@app/web/assets/utilities/dist'),
('2f051fd7', '@app/web/assets/feed/dist'),
('3ef8de45', '@froala/craftfroalawysiwyg/assets/field/dist'),
('52204117', '@app/web/assets/login/dist'),
('524842a5', '@app/web/assets/recententries/dist'),
('543d2803', '@app/web/assets/pluginstore/dist'),
('63eb75e4', '@app/web/assets/installer/dist'),
('658ba63', '@lib/element-resize-detector'),
('6cf200cb', '@app/web/assets/updateswidget/dist'),
('76ee7d4e', '@lib/jquery-ui'),
('7706702c', '@lib/velocity'),
('7b7b9d4b', '@app/web/assets/craftsupport/dist'),
('7c393d07', '@lib/axios'),
('7ca752f4', '@app/web/assets/updater/dist'),
('7d3307aa', '@app/web/assets/cp/dist'),
('809ff66a', '@vendor/froala/wysiwyg-editor'),
('8bce8aad', '@lib/timepicker'),
('8d3ccef8', '@lib/fabric'),
('9274346b', '@app/web/assets/fields/dist'),
('95131a25', '@app/web/assets/dashboard/dist'),
('987a1ac6', '@lib/garnishjs'),
('a2c611d3', '@lib/jquery-touch-events'),
('a5622d11', '@lib/picturefill'),
('abc70d40', '@lib/d3'),
('b431d2d8', '@lib/xregexp'),
('b5b5d825', '@app/web/assets/editentry/dist'),
('ba97fa11', '@app/web/assets/matrix/dist'),
('bd8a2cef', '@lib/fileupload'),
('be5d9652', '@lib/selectize'),
('dde79c0e', '@app/web/assets/editsection/dist'),
('e0fb8db6', '@app/web/assets/admintable/dist'),
('ed5f197c', '@app/web/assets/edittransform/dist'),
('ef018d9d', '@lib/prismjs');

-- --------------------------------------------------------

--
-- Table structure for table `revisions`
--

CREATE TABLE `revisions` (
  `id` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `num` int(11) NOT NULL,
  `notes` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `revisions`
--

INSERT INTO `revisions` (`id`, `sourceId`, `creatorId`, `num`, `notes`) VALUES
(1, 2, 1, 1, NULL),
(2, 2, 1, 2, NULL),
(3, 5, 1, 1, NULL),
(4, 5, 1, 2, NULL),
(5, 5, 1, 3, NULL),
(6, 5, 1, 4, 'Applied “Draft 1”'),
(7, 35, 1, 1, NULL),
(8, 35, 1, 2, NULL),
(9, 35, 1, 3, NULL),
(10, 35, 1, 4, NULL),
(11, 35, 1, 5, NULL),
(12, 35, 1, 6, NULL),
(13, 35, 1, 7, NULL),
(14, 35, 1, 8, NULL),
(15, 35, 1, 9, NULL),
(16, 35, 1, 10, NULL),
(17, 35, 1, 11, NULL),
(18, 35, 1, 12, NULL),
(19, 35, 1, 13, NULL),
(20, 35, 1, 14, NULL),
(21, 73, 1, 1, NULL),
(22, 73, 1, 2, NULL),
(23, 73, 1, 3, NULL),
(24, 73, 1, 4, NULL),
(25, 73, 1, 5, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `searchindex`
--

CREATE TABLE `searchindex` (
  `elementId` int(11) NOT NULL,
  `attribute` varchar(25) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `keywords` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `searchindex`
--

INSERT INTO `searchindex` (`elementId`, `attribute`, `fieldId`, `siteId`, `keywords`) VALUES
(1, 'username', 0, 1, ' patrickvanl '),
(1, 'firstname', 0, 1, ''),
(1, 'lastname', 0, 1, ''),
(1, 'fullname', 0, 1, ''),
(1, 'email', 0, 1, ' vanleipsigpatrick gmail com '),
(1, 'slug', 0, 1, ''),
(2, 'slug', 0, 1, ' home '),
(2, 'title', 0, 1, ' home '),
(5, 'slug', 0, 1, ' homepage '),
(5, 'title', 0, 1, ' homepage '),
(8, 'slug', 0, 1, ''),
(8, 'field', 6, 1, ' lorem ipsum dolor sit amet consectetur adipiscing elit aenean et eros ullamcorper ante sollicitudin iaculis vestibulum sit amet placerat justo nullam et urna eros curabitur scelerisque neque in dignissim viverra praesent dignissim rutrum tellus vel rhoncus sem fringilla vitae nulla ut urna at leo posuere accumsan nullam ut mi imperdiet dignissim purus vitae condimentum erat in arcu diam rutrum et nisi sit amet suscipit imperdiet sem vestibulum ullamcorper elementum turpis eu iaculis nam fermentum eu felis et dapibus nam convallis urna tellus at lobortis risus gravida vel integer vitae cursus est vestibulum dui tellus fringilla vitae neque vel dapibus vestibulum neque integer at mi vulputate facilisis sem non congue est duis placerat massa non euismod consectetur '),
(9, 'slug', 0, 1, ''),
(9, 'field', 7, 1, ''),
(10, 'slug', 0, 1, ''),
(10, 'field', 6, 1, ' lorem ipsum dolor sit amet consectetur adipiscing elit aenean et eros ullamcorper ante sollicitudin iaculis vestibulum sit amet placerat justo nullam et urna eros curabitur scelerisque neque in dignissim viverra praesent dignissim rutrum tellus vel rhoncus sem fringilla vitae nulla ut urna at leo posuere accumsan nullam ut mi imperdiet dignissim purus vitae condimentum erat in arcu diam rutrum et nisi sit amet suscipit imperdiet sem vestibulum ullamcorper elementum turpis eu iaculis nam fermentum eu felis et dapibus nam convallis urna tellus at lobortis risus gravida vel integer vitae cursus est vestibulum dui tellus fringilla vitae neque vel dapibus vestibulum neque integer at mi vulputate facilisis sem non congue est duis placerat massa non euismod consectetur '),
(11, 'slug', 0, 1, ''),
(5, 'field', 5, 1, ' lorem ipsum dolor sit amet consectetur adipiscing elit aenean et eros ullamcorper ante sollicitudin iaculis vestibulum sit amet placerat justo nullam et urna eros curabitur scelerisque neque in dignissim viverra praesent dignissim rutrum tellus vel rhoncus sem fringilla vitae nulla ut urna at leo posuere accumsan nullam ut mi imperdiet dignissim purus vitae condimentum erat in arcu diam rutrum et nisi sit amet suscipit imperdiet sem vestibulum ullamcorper elementum turpis eu iaculis nam fermentum eu felis et dapibus nam convallis urna tellus at lobortis risus gravida vel integer vitae cursus est vestibulum dui tellus fringilla vitae neque vel dapibus vestibulum neque integer at mi vulputate facilisis sem non congue est duis placerat massa non euismod consectetur lorem ipsum dolor sit amet consectetur adipiscing elit aenean et eros ullamcorper ante sollicitudin iaculis vestibulum sit amet placerat justo nullam et urna eros curabitur scelerisque neque in dignissim viverra praesent dignissim rutrum tellus vel rhoncus sem fringilla vitae nulla ut urna at leo posuere accumsan nullam ut mi imperdiet dignissim purus vitae condimentum erat in arcu diam rutrum et nisi sit amet suscipit imperdiet sem vestibulum ullamcorper elementum turpis eu iaculis nam fermentum eu felis et dapibus nam convallis urna tellus at lobortis risus gravida vel integer vitae cursus est vestibulum dui tellus fringilla vitae neque vel dapibus vestibulum neque integer at mi vulputate facilisis sem non congue est duis placerat massa non euismod consectetur chemex lorem ipsum dolor sit amet consectetur adipiscing elit aenean et eros ullamcorper ante sollicitudin iaculis vestibulum sit amet placerat justo nullam et urna eros curabitur scelerisque neque in dignissim viverra praesent dignissim rutrum tellus vel rhoncus sem fringilla vitae nulla ut urna at leo posuere accumsan nullam ut mi imperdiet dignissim purus vitae condimentum erat in arcu diam rutrum et nisi sit amet suscipit imperdiet sem vestibulum ullamcorper elementum turpis eu iaculis nam fermentum eu felis et dapibus nam convallis urna tellus at lobortis risus gravida vel integer vitae cursus est vestibulum dui tellus fringilla vitae neque vel dapibus vestibulum neque integer at mi vulputate facilisis sem non congue est duis placerat massa non euismod consectetur '),
(17, 'filename', 0, 1, ' chemex jpg '),
(17, 'extension', 0, 1, ' jpg '),
(17, 'kind', 0, 1, ' image '),
(17, 'slug', 0, 1, ''),
(17, 'title', 0, 1, ' chemex '),
(24, 'slug', 0, 1, ''),
(24, 'field', 6, 1, ' lorem ipsum dolor sit amet consectetur adipiscing elit aenean et eros ullamcorper ante sollicitudin iaculis vestibulum sit amet placerat justo nullam et urna eros curabitur scelerisque neque in dignissim viverra praesent dignissim rutrum tellus vel rhoncus sem fringilla vitae nulla ut urna at leo posuere accumsan nullam ut mi imperdiet dignissim purus vitae condimentum erat in arcu diam rutrum et nisi sit amet suscipit imperdiet sem vestibulum ullamcorper elementum turpis eu iaculis nam fermentum eu felis et dapibus nam convallis urna tellus at lobortis risus gravida vel integer vitae cursus est vestibulum dui tellus fringilla vitae neque vel dapibus vestibulum neque integer at mi vulputate facilisis sem non congue est duis placerat massa non euismod consectetur '),
(25, 'slug', 0, 1, ''),
(25, 'field', 7, 1, ''),
(26, 'slug', 0, 1, ''),
(26, 'field', 6, 1, ' lorem ipsum dolor sit amet consectetur adipiscing elit aenean et eros ullamcorper ante sollicitudin iaculis vestibulum sit amet placerat justo nullam et urna eros curabitur scelerisque neque in dignissim viverra praesent dignissim rutrum tellus vel rhoncus sem fringilla vitae nulla ut urna at leo posuere accumsan nullam ut mi imperdiet dignissim purus vitae condimentum erat in arcu diam rutrum et nisi sit amet suscipit imperdiet sem vestibulum ullamcorper elementum turpis eu iaculis nam fermentum eu felis et dapibus nam convallis urna tellus at lobortis risus gravida vel integer vitae cursus est vestibulum dui tellus fringilla vitae neque vel dapibus vestibulum neque integer at mi vulputate facilisis sem non congue est duis placerat massa non euismod consectetur '),
(27, 'slug', 0, 1, ''),
(27, 'field', 8, 1, ' chemex '),
(28, 'slug', 0, 1, ''),
(28, 'field', 6, 1, ' lorem ipsum dolor sit amet consectetur adipiscing elit aenean et eros ullamcorper ante sollicitudin iaculis vestibulum sit amet placerat justo nullam et urna eros curabitur scelerisque neque in dignissim viverra praesent dignissim rutrum tellus vel rhoncus sem fringilla vitae nulla ut urna at leo posuere accumsan nullam ut mi imperdiet dignissim purus vitae condimentum erat in arcu diam rutrum et nisi sit amet suscipit imperdiet sem vestibulum ullamcorper elementum turpis eu iaculis nam fermentum eu felis et dapibus nam convallis urna tellus at lobortis risus gravida vel integer vitae cursus est vestibulum dui tellus fringilla vitae neque vel dapibus vestibulum neque integer at mi vulputate facilisis sem non congue est duis placerat massa non euismod consectetur '),
(35, 'slug', 0, 1, ' homepage '),
(35, 'title', 0, 1, ' homepage '),
(38, 'slug', 0, 1, ''),
(38, 'field', 10, 1, ' mooie lenzen '),
(39, 'slug', 0, 1, ''),
(39, 'field', 11, 1, ' chemex '),
(38, 'field', 13, 1, ' lorem ipsum dolor sit amet consectetur adipiscing elit nam consequat urna vitae ante hendrerit condimentum in hac habitasse platea dictumst cras eu tortor sit amet magna eleifend egestas et varius orci aliquam accumsan purus elit nec tincidunt odio consequat tincidunt aenean viverra leo velit in nisi nulla pellentesque nec libero at lobortis euismod dolor orci varius natoque penatibus et magnis dis parturient montes nascetur ridiculus mus nunc eget ornare tellus mauris ultrices sapien scelerisque leo pretium condimentum nulla id lacus in ipsum efficitur semper et id nibh curabitur purus enim elementum blandit fringilla in pretium vitae leo morbi id consectetur justo in suscipit risus praesent id molestie quam sed vitae varius neque vestibulum tincidunt fermentum justo nec scelerisque erat sollicitudin eget suspendisse auctor mollis ipsum vel egestas duis eu massa venenatis suscipit massa at posuere eros mauris lacinia turpis quis rutrum semper donec erat leo luctus sit amet imperdiet in sodales vel massa nunc nec sem ornare sodales urna quis vulputate odio fusce finibus elit et diam consequat ac gravida neque tincidunt quisque facilisis sodales risus ut iaculis maecenas quis sagittis erat eget dapibus risus duis luctus massa in consequat interdum ligula dolor efficitur justo sed congue nibh nisl in dolor fusce ante libero scelerisque sed posuere sed condimentum at magna sed ultricies sem semper commodo justo vitae vehicula arcu quisque cursus lacus non dictum maximus aenean quis mollis purus duis eros lectus mattis eget lorem vel tempor pretium odio nullam vel sodales magna vivamus sollicitudin ut ipsum sed blandit donec non dui bibendum scelerisque lectus ut faucibus tellus suspendisse tristique mi vitae elit dignissim tempus curabitur eget quam vitae mi hendrerit auctor donec vehicula ex quam semper interdum diam vulputate et sed semper lorem sed mauris aliquam sit amet mattis massa vestibulum vestibulum placerat hendrerit ipsum eu mattis dui dignissim eu sed ultricies tortor eu ornare mattis maecenas sodales aliquam venenatis in ut tellus sem vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec dictum accumsan libero nec rutrum eros vulputate ac proin at nisi ut lacus efficitur convallis et id nulla maecenas aliquam est non scelerisque lobortis aliquam tempus nibh eget bibendum vulputate odio orci ultricies magna a finibus neque risus a orci suspendisse aliquet ante ut velit sodales laoreet vivamus euismod lacus orci ac ultricies nunc volutpat a morbi accumsan laoreet sem et tempor nulla volutpat id aliquam erat volutpat suspendisse commodo orci nec ultrices vehicula praesent dictum sem sit amet sapien congue ullamcorper aenean id odio bibendum eleifend tellus at condimentum erat nunc vel ullamcorper justo '),
(38, 'field', 15, 1, ' kevin mueller sw72 7 r seu0 unsplash '),
(52, 'filename', 0, 1, ' hakon sataoen qyfco1nfmtg unsplash jpg '),
(52, 'extension', 0, 1, ' jpg '),
(52, 'kind', 0, 1, ' image '),
(52, 'slug', 0, 1, ''),
(52, 'title', 0, 1, ' hakon sataoen qyfco1nf mtg unsplash '),
(60, 'filename', 0, 1, ' hakon sataoen qyfco1nfmtg unsplash jpg '),
(55, 'filename', 0, 1, ' meik schneider e9zsm8orifa unsplash jpg '),
(55, 'extension', 0, 1, ' jpg '),
(55, 'kind', 0, 1, ' image '),
(55, 'slug', 0, 1, ''),
(55, 'title', 0, 1, ' meik schneider e9z sm8or if a unsplash '),
(60, 'extension', 0, 1, ' jpg '),
(60, 'kind', 0, 1, ' image '),
(60, 'slug', 0, 1, ''),
(60, 'title', 0, 1, ' hakon sataoen qyfco1nf mtg unsplash '),
(35, 'field', 9, 1, ' kevin mueller sw72 7 r seu0 unsplash lorem ipsum dolor sit amet consectetur adipiscing elit nam consequat urna vitae ante hendrerit condimentum in hac habitasse platea dictumst cras eu tortor sit amet magna eleifend egestas et varius orci aliquam accumsan purus elit nec tincidunt odio consequat tincidunt aenean viverra leo velit in nisi nulla pellentesque nec libero at lobortis euismod dolor orci varius natoque penatibus et magnis dis parturient montes nascetur ridiculus mus nunc eget ornare tellus mauris ultrices sapien scelerisque leo pretium condimentum nulla id lacus in ipsum efficitur semper et id nibh curabitur purus enim elementum blandit fringilla in pretium vitae leo morbi id consectetur justo in suscipit risus praesent id molestie quam sed vitae varius neque vestibulum tincidunt fermentum justo nec scelerisque erat sollicitudin eget suspendisse auctor mollis ipsum vel egestas duis eu massa venenatis suscipit massa at posuere eros mauris lacinia turpis quis rutrum semper donec erat leo luctus sit amet imperdiet in sodales vel massa nunc nec sem ornare sodales urna quis vulputate odio fusce finibus elit et diam consequat ac gravida neque tincidunt quisque facilisis sodales risus ut iaculis maecenas quis sagittis erat eget dapibus risus duis luctus massa in consequat interdum ligula dolor efficitur justo sed congue nibh nisl in dolor fusce ante libero scelerisque sed posuere sed condimentum at magna sed ultricies sem semper commodo justo vitae vehicula arcu quisque cursus lacus non dictum maximus aenean quis mollis purus duis eros lectus mattis eget lorem vel tempor pretium odio nullam vel sodales magna vivamus sollicitudin ut ipsum sed blandit donec non dui bibendum scelerisque lectus ut faucibus tellus suspendisse tristique mi vitae elit dignissim tempus curabitur eget quam vitae mi hendrerit auctor donec vehicula ex quam semper interdum diam vulputate et sed semper lorem sed mauris aliquam sit amet mattis massa vestibulum vestibulum placerat hendrerit ipsum eu mattis dui dignissim eu sed ultricies tortor eu ornare mattis maecenas sodales aliquam venenatis in ut tellus sem vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec dictum accumsan libero nec rutrum eros vulputate ac proin at nisi ut lacus efficitur convallis et id nulla maecenas aliquam est non scelerisque lobortis aliquam tempus nibh eget bibendum vulputate odio orci ultricies magna a finibus neque risus a orci suspendisse aliquet ante ut velit sodales laoreet vivamus euismod lacus orci ac ultricies nunc volutpat a morbi accumsan laoreet sem et tempor nulla volutpat id aliquam erat volutpat suspendisse commodo orci nec ultrices vehicula praesent dictum sem sit amet sapien congue ullamcorper aenean id odio bibendum eleifend tellus at condimentum erat nunc vel ullamcorper justo mooie lenzen hakon sataoen qyfco1nf mtg unsplash maecenas sodales aliquam venenatis in ut tellus sem vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec dictum accumsan libero nec rutrum eros vulputate ac proin at nisi ut lacus efficitur convallis et id nulla maecenas aliquam est non scelerisque lobortis aliquam tempus nibh eget bibendum vulputate odio orci ultricies magna a finibus neque risus a orci suspendisse aliquet ante ut velit sodales laoreet vivamus euismod lacus orci ac ultricies nunc volutpat a morbi accumsan laoreet sem et tempor nulla volutpat id aliquam erat volutpat suspendisse commodo orci nec ultrices vehicula praesent dictum sem sit amet sapien congue ullamcorper aenean id odio bibendum eleifend tellus at condimentum erat nunc vel ullamcorper justo amazing title 2 '),
(63, 'filename', 0, 1, ' hakon sataoen qyfco1nfmtg unsplash jpg '),
(63, 'extension', 0, 1, ' jpg '),
(63, 'kind', 0, 1, ' image '),
(63, 'slug', 0, 1, ''),
(63, 'title', 0, 1, ' hakon sataoen qyfco1nf mtg unsplash '),
(66, 'filename', 0, 1, ' kevin mueller sw72 7rseu0 unsplash jpg '),
(66, 'extension', 0, 1, ' jpg '),
(66, 'kind', 0, 1, ' image '),
(66, 'slug', 0, 1, ''),
(66, 'title', 0, 1, ' kevin mueller sw72 7 r seu0 unsplash '),
(69, 'slug', 0, 1, ''),
(69, 'field', 18, 1, ' amazing title 2 '),
(69, 'field', 16, 1, ' maecenas sodales aliquam venenatis in ut tellus sem vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec dictum accumsan libero nec rutrum eros vulputate ac proin at nisi ut lacus efficitur convallis et id nulla maecenas aliquam est non scelerisque lobortis aliquam tempus nibh eget bibendum vulputate odio orci ultricies magna a finibus neque risus a orci suspendisse aliquet ante ut velit sodales laoreet vivamus euismod lacus orci ac ultricies nunc volutpat a morbi accumsan laoreet sem et tempor nulla volutpat id aliquam erat volutpat suspendisse commodo orci nec ultrices vehicula praesent dictum sem sit amet sapien congue ullamcorper aenean id odio bibendum eleifend tellus at condimentum erat nunc vel ullamcorper justo '),
(69, 'field', 17, 1, ' hakon sataoen qyfco1nf mtg unsplash '),
(73, 'title', 0, 1, ' testpage '),
(73, 'slug', 0, 1, ' testpage '),
(76, 'filename', 0, 1, ' ben white qdy9ahp0mto unsplash jpg '),
(76, 'extension', 0, 1, ' jpg '),
(76, 'kind', 0, 1, ' image '),
(76, 'slug', 0, 1, ''),
(76, 'title', 0, 1, ' ben white q dy9ahp0 mto unsplash '),
(77, 'filename', 0, 1, ' patrick ward z dlxnqg0jy unsplash jpg '),
(77, 'extension', 0, 1, ' jpg '),
(77, 'kind', 0, 1, ' image '),
(77, 'slug', 0, 1, ''),
(77, 'title', 0, 1, ' patrick ward z d l xn qg0 jy unsplash '),
(78, 'slug', 0, 1, ''),
(78, 'field', 18, 1, ' this is the 2nd page that uses this template '),
(79, 'slug', 0, 1, ''),
(79, 'field', 10, 1, ' the two sections have been switched around '),
(79, 'field', 13, 1, ' lorem ipsum dolor sit amet consectetur adipiscing elit cras elementum urna tincidunt sem hendrerit pretium vivamus in sapien pretium rhoncus orci eget suscipit diam nunc viverra lorem mauris et aliquet odio luctus nec duis scelerisque tellus nec leo condimentum id iaculis neque porttitor etiam quis faucibus orci curabitur pharetra dictum maximus quisque in imperdiet tellus ac porta enim suspendisse rhoncus convallis neque ac volutpat est cursus et sed eget lacus eget nunc ornare feugiat pellentesque vel ante quis lectus fermentum sodales curabitur orci felis varius eget volutpat non interdum sed nulla proin eu congue sapien non euismod nisi nam lorem libero ullamcorper vitae gravida luctus mattis eget orci fusce ac sapien a nunc ultrices interdum vitae at erat mauris convallis sagittis euismod vestibulum porttitor mi in finibus pharetra ex nulla lobortis leo luctus vestibulum diam erat nec libero maecenas ornare accumsan commodo fusce molestie malesuada nulla in varius aenean mollis eros interdum sem molestie posuere duis fermentum tincidunt bibendum integer dignissim ullamcorper viverra aenean eget enim dignissim hendrerit ex nec volutpat tellus maecenas venenatis nunc dolor ac pharetra leo lacinia vel donec nibh urna varius in condimentum id pharetra facilisis ligula sed egestas ipsum et gravida condimentum nullam ut urna ac lectus tempor lacinia vel nec ligula curabitur eget nunc augue cras et interdum sapien a feugiat mi quisque eleifend lectus sit amet pulvinar suscipit dui sem sagittis ex sed posuere orci est at libero sed ornare augue purus eget consectetur massa cursus eu maecenas ut enim a odio gravida varius semper at libero phasellus tincidunt magna arcu eget imperdiet ligula aliquam nec suspendisse viverra nulla et mauris sodales dictum donec id ligula purus sed id tellus justo suspendisse gravida dapibus enim ac auctor sed pretium semper metus a suscipit nulla tortor risus ornare in congue non malesuada ut magna quisque lacinia massa a vehicula molestie mi nisi vestibulum enim sit amet molestie nibh neque in orci sed tempus consectetur placerat sed in mauris id tortor pulvinar dictum ac ac magna ut quis ipsum a leo dapibus interdum aliquam et scelerisque turpis nec vestibulum mauris morbi pretium nisi non lectus dictum nec auctor turpis maximus aliquam consequat fringilla odio vitae varius '),
(79, 'field', 15, 1, ' ben white q dy9ahp0 mto unsplash '),
(78, 'field', 16, 1, ' lorem ipsum dolor sit amet consectetur adipiscing elit cras elementum urna tincidunt sem hendrerit pretium vivamus in sapien pretium rhoncus orci eget suscipit diam nunc viverra lorem mauris et aliquet odio luctus nec duis scelerisque tellus nec leo condimentum id iaculis neque porttitor etiam quis faucibus orci curabitur pharetra dictum maximus quisque in imperdiet tellus ac porta enim suspendisse rhoncus convallis neque ac volutpat est cursus et sed eget lacus eget nunc ornare feugiat pellentesque vel ante quis lectus fermentum sodales curabitur orci felis varius eget volutpat non interdum sed nulla proin eu congue sapien non euismod nisi nam lorem libero ullamcorper vitae gravida luctus mattis eget orci fusce ac sapien a nunc ultrices interdum vitae at erat mauris convallis sagittis euismod vestibulum porttitor mi in finibus pharetra ex nulla lobortis leo luctus vestibulum diam erat nec libero maecenas ornare accumsan commodo fusce molestie malesuada nulla in varius aenean mollis eros interdum sem molestie posuere duis fermentum tincidunt bibendum integer dignissim ullamcorper viverra aenean eget enim dignissim hendrerit ex nec volutpat tellus maecenas venenatis nunc dolor ac pharetra leo lacinia vel donec nibh urna varius in condimentum id pharetra facilisis ligula sed egestas ipsum et gravida condimentum nullam ut urna ac lectus tempor lacinia vel nec ligula curabitur eget nunc augue cras et interdum sapien a feugiat mi quisque eleifend lectus sit amet pulvinar suscipit dui sem sagittis ex sed posuere orci est at libero sed ornare augue purus eget consectetur massa cursus eu maecenas ut enim a odio gravida varius semper at libero phasellus tincidunt magna arcu eget imperdiet ligula aliquam nec suspendisse viverra nulla et mauris sodales dictum donec id ligula purus sed id tellus justo suspendisse gravida dapibus enim ac auctor sed pretium semper metus a suscipit nulla tortor risus ornare in congue non malesuada ut magna quisque lacinia massa a vehicula molestie mi nisi vestibulum enim sit amet molestie nibh neque in orci sed tempus consectetur placerat sed in mauris id tortor pulvinar dictum ac ac magna ut quis ipsum a leo dapibus interdum aliquam et scelerisque turpis nec vestibulum mauris morbi pretium nisi non lectus dictum nec auctor turpis maximus aliquam consequat fringilla odio vitae varius wow '),
(78, 'field', 17, 1, ' patrick ward z d l xn qg0 jy unsplash '),
(73, 'field', 9, 1, ' patrick ward z d l xn qg0 jy unsplash lorem ipsum dolor sit amet consectetur adipiscing elit cras elementum urna tincidunt sem hendrerit pretium vivamus in sapien pretium rhoncus orci eget suscipit diam nunc viverra lorem mauris et aliquet odio luctus nec duis scelerisque tellus nec leo condimentum id iaculis neque porttitor etiam quis faucibus orci curabitur pharetra dictum maximus quisque in imperdiet tellus ac porta enim suspendisse rhoncus convallis neque ac volutpat est cursus et sed eget lacus eget nunc ornare feugiat pellentesque vel ante quis lectus fermentum sodales curabitur orci felis varius eget volutpat non interdum sed nulla proin eu congue sapien non euismod nisi nam lorem libero ullamcorper vitae gravida luctus mattis eget orci fusce ac sapien a nunc ultrices interdum vitae at erat mauris convallis sagittis euismod vestibulum porttitor mi in finibus pharetra ex nulla lobortis leo luctus vestibulum diam erat nec libero maecenas ornare accumsan commodo fusce molestie malesuada nulla in varius aenean mollis eros interdum sem molestie posuere duis fermentum tincidunt bibendum integer dignissim ullamcorper viverra aenean eget enim dignissim hendrerit ex nec volutpat tellus maecenas venenatis nunc dolor ac pharetra leo lacinia vel donec nibh urna varius in condimentum id pharetra facilisis ligula sed egestas ipsum et gravida condimentum nullam ut urna ac lectus tempor lacinia vel nec ligula curabitur eget nunc augue cras et interdum sapien a feugiat mi quisque eleifend lectus sit amet pulvinar suscipit dui sem sagittis ex sed posuere orci est at libero sed ornare augue purus eget consectetur massa cursus eu maecenas ut enim a odio gravida varius semper at libero phasellus tincidunt magna arcu eget imperdiet ligula aliquam nec suspendisse viverra nulla et mauris sodales dictum donec id ligula purus sed id tellus justo suspendisse gravida dapibus enim ac auctor sed pretium semper metus a suscipit nulla tortor risus ornare in congue non malesuada ut magna quisque lacinia massa a vehicula molestie mi nisi vestibulum enim sit amet molestie nibh neque in orci sed tempus consectetur placerat sed in mauris id tortor pulvinar dictum ac ac magna ut quis ipsum a leo dapibus interdum aliquam et scelerisque turpis nec vestibulum mauris morbi pretium nisi non lectus dictum nec auctor turpis maximus aliquam consequat fringilla odio vitae varius wow this is the 2nd page that uses this template ben white q dy9ahp0 mto unsplash lorem ipsum dolor sit amet consectetur adipiscing elit cras elementum urna tincidunt sem hendrerit pretium vivamus in sapien pretium rhoncus orci eget suscipit diam nunc viverra lorem mauris et aliquet odio luctus nec duis scelerisque tellus nec leo condimentum id iaculis neque porttitor etiam quis faucibus orci curabitur pharetra dictum maximus quisque in imperdiet tellus ac porta enim suspendisse rhoncus convallis neque ac volutpat est cursus et sed eget lacus eget nunc ornare feugiat pellentesque vel ante quis lectus fermentum sodales curabitur orci felis varius eget volutpat non interdum sed nulla proin eu congue sapien non euismod nisi nam lorem libero ullamcorper vitae gravida luctus mattis eget orci fusce ac sapien a nunc ultrices interdum vitae at erat mauris convallis sagittis euismod vestibulum porttitor mi in finibus pharetra ex nulla lobortis leo luctus vestibulum diam erat nec libero maecenas ornare accumsan commodo fusce molestie malesuada nulla in varius aenean mollis eros interdum sem molestie posuere duis fermentum tincidunt bibendum integer dignissim ullamcorper viverra aenean eget enim dignissim hendrerit ex nec volutpat tellus maecenas venenatis nunc dolor ac pharetra leo lacinia vel donec nibh urna varius in condimentum id pharetra facilisis ligula sed egestas ipsum et gravida condimentum nullam ut urna ac lectus tempor lacinia vel nec ligula curabitur eget nunc augue cras et interdum sapien a feugiat mi quisque eleifend lectus sit amet pulvinar suscipit dui sem sagittis ex sed posuere orci est at libero sed ornare augue purus eget consectetur massa cursus eu maecenas ut enim a odio gravida varius semper at libero phasellus tincidunt magna arcu eget imperdiet ligula aliquam nec suspendisse viverra nulla et mauris sodales dictum donec id ligula purus sed id tellus justo suspendisse gravida dapibus enim ac auctor sed pretium semper metus a suscipit nulla tortor risus ornare in congue non malesuada ut magna quisque lacinia massa a vehicula molestie mi nisi vestibulum enim sit amet molestie nibh neque in orci sed tempus consectetur placerat sed in mauris id tortor pulvinar dictum ac ac magna ut quis ipsum a leo dapibus interdum aliquam et scelerisque turpis nec vestibulum mauris morbi pretium nisi non lectus dictum nec auctor turpis maximus aliquam consequat fringilla odio vitae varius the two sections have been switched around ');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(11) NOT NULL,
  `structureId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` enum('single','channel','structure') NOT NULL DEFAULT 'channel',
  `enableVersioning` tinyint(1) NOT NULL DEFAULT '0',
  `propagationMethod` varchar(255) NOT NULL DEFAULT 'all',
  `previewTargets` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `structureId`, `name`, `handle`, `type`, `enableVersioning`, `propagationMethod`, `previewTargets`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, NULL, 'Home', 'home', 'single', 1, 'all', '[{\"label\":\"Primary entry page\",\"urlFormat\":\"{url}\",\"refresh\":\"1\"}]', '2020-03-04 18:32:27', '2020-03-04 18:32:27', '2020-03-04 18:34:01', '86dae065-1263-4fd4-bd7a-e60d11f17fe7'),
(2, NULL, 'Homepage', 'homepage', 'single', 1, 'all', '[{\"label\":\"Primary entry page\",\"urlFormat\":\"{url}\",\"refresh\":\"1\"}]', '2020-03-04 18:40:40', '2020-03-04 18:40:40', '2020-03-04 18:57:27', '5fc2772a-64ab-4ef8-9f8a-a5157126856b'),
(3, NULL, 'Homepage', 'homepage', 'single', 1, 'all', '[{\"label\":\"Primary entry page\",\"urlFormat\":\"{url}\",\"refresh\":\"1\"}]', '2020-03-04 19:02:23', '2020-03-04 19:02:23', NULL, '77418224-69b0-4f59-9d02-e2cfdf838ff1'),
(4, NULL, 'TestPage', 'testpage', 'single', 1, 'all', '[{\"label\":\"Primary entry page\",\"urlFormat\":\"{url}\",\"refresh\":\"1\"}]', '2020-03-04 20:19:53', '2020-03-04 20:19:53', NULL, 'b6bd99cb-623f-43e9-a7bc-7cf3773438cb');

-- --------------------------------------------------------

--
-- Table structure for table `sections_sites`
--

CREATE TABLE `sections_sites` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text,
  `template` varchar(500) DEFAULT NULL,
  `enabledByDefault` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sections_sites`
--

INSERT INTO `sections_sites` (`id`, `sectionId`, `siteId`, `hasUrls`, `uriFormat`, `template`, `enabledByDefault`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 1, 1, '__home__', '', 1, '2020-03-04 18:32:27', '2020-03-04 18:32:27', '5902fa55-1e50-4f3e-844f-8f2e2faddd5a'),
(2, 2, 1, 1, '__home__', '', 1, '2020-03-04 18:40:40', '2020-03-04 18:40:40', '67f5e1d7-7b6d-4ef3-98ce-4717a4c0b0fc'),
(3, 3, 1, 1, '__home__', '', 1, '2020-03-04 19:02:23', '2020-03-04 19:02:23', '751cbc90-7603-4719-9247-38c95b63603c'),
(4, 4, 1, 1, 'testpage', 'index', 1, '2020-03-04 20:19:53', '2020-03-04 20:19:53', '4f0a5ac7-af94-4d08-af6b-528ca6dfa4fd');

-- --------------------------------------------------------

--
-- Table structure for table `sequences`
--

CREATE TABLE `sequences` (
  `name` varchar(255) NOT NULL,
  `next` int(11) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `token` char(100) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `userId`, `token`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, '_yDWDlfNNFZeIEQNkrRPOF9EFAP1OsPukJvRY42MBu5njngYVkUyxHFNCLilGdGhuklsNyUzd9ZwtDoLV7Gos3c7qNwtTUfM0qfH', '2020-03-04 09:23:47', '2020-03-04 09:24:30', '17dbd58e-b21e-4660-a69a-fc45f2c737e7'),
(2, 1, 'Cwy8wrJ0BHyTi_4kpbFcse4C4-R2WquBufJkP5yezOi1AvkhpKT5KJn7tnXvZ29yewz-p2tUt8HqiNHYxcoTwqzuhtjF9m8vKWCw', '2020-03-04 18:27:01', '2020-03-04 20:31:56', 'b76016cf-f4ac-4c7a-af37-852d4b09bb4f');

-- --------------------------------------------------------

--
-- Table structure for table `shunnedmessages`
--

CREATE TABLE `shunnedmessages` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sitegroups`
--

CREATE TABLE `sitegroups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sitegroups`
--

INSERT INTO `sitegroups` (`id`, `name`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 'Brik', '2020-03-04 09:23:46', '2020-03-04 09:23:46', NULL, 'd972a572-ba36-4e20-9e4b-bc9f97121a1e');

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE `sites` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `primary` tinyint(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `language` varchar(12) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '0',
  `baseUrl` varchar(255) DEFAULT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`id`, `groupId`, `primary`, `name`, `handle`, `language`, `hasUrls`, `baseUrl`, `sortOrder`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 1, 1, 'Brik', 'default', 'en-US', 1, '$DEFAULT_SITE_URL', 1, '2020-03-04 09:23:46', '2020-03-04 09:23:46', NULL, 'f7434bb1-a09d-4f73-99dd-1b4a56a4cb49');

-- --------------------------------------------------------

--
-- Table structure for table `structureelements`
--

CREATE TABLE `structureelements` (
  `id` int(11) NOT NULL,
  `structureId` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `root` int(11) UNSIGNED DEFAULT NULL,
  `lft` int(11) UNSIGNED NOT NULL,
  `rgt` int(11) UNSIGNED NOT NULL,
  `level` smallint(6) UNSIGNED NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `structures`
--

CREATE TABLE `structures` (
  `id` int(11) NOT NULL,
  `maxLevels` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `systemmessages`
--

CREATE TABLE `systemmessages` (
  `id` int(11) NOT NULL,
  `language` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `subject` text NOT NULL,
  `body` text NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `taggroups`
--

CREATE TABLE `taggroups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `deletedWithGroup` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `templatecacheelements`
--

CREATE TABLE `templatecacheelements` (
  `cacheId` int(11) NOT NULL,
  `elementId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `templatecachequeries`
--

CREATE TABLE `templatecachequeries` (
  `id` int(11) NOT NULL,
  `cacheId` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `query` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `templatecaches`
--

CREATE TABLE `templatecaches` (
  `id` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `cacheKey` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `body` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `id` int(11) NOT NULL,
  `token` char(32) NOT NULL,
  `route` text,
  `usageLimit` tinyint(3) UNSIGNED DEFAULT NULL,
  `usageCount` tinyint(3) UNSIGNED DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tokens`
--

INSERT INTO `tokens` (`id`, `token`, `route`, `usageLimit`, `usageCount`, `expiryDate`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'mLjPTP5dDMF0FMXSQI9lXVjolPjy6cmQ', '[\"preview/preview\",{\"elementType\":\"craft\\\\elements\\\\Entry\",\"sourceId\":5,\"siteId\":1,\"draftId\":1,\"revisionId\":null}]', NULL, NULL, '2020-03-05 18:44:35', '2020-03-04 18:44:35', '2020-03-04 18:44:35', '061d0f77-e2e3-477c-aa73-bdbbf48bf650');

-- --------------------------------------------------------

--
-- Table structure for table `usergroups`
--

CREATE TABLE `usergroups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usergroups_users`
--

CREATE TABLE `usergroups_users` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `userpermissions`
--

CREATE TABLE `userpermissions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `userpermissions_usergroups`
--

CREATE TABLE `userpermissions_usergroups` (
  `id` int(11) NOT NULL,
  `permissionId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `userpermissions_users`
--

CREATE TABLE `userpermissions_users` (
  `id` int(11) NOT NULL,
  `permissionId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `userpreferences`
--

CREATE TABLE `userpreferences` (
  `userId` int(11) NOT NULL,
  `preferences` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userpreferences`
--

INSERT INTO `userpreferences` (`userId`, `preferences`) VALUES
(1, '{\"language\":\"en-US\"}');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `photoId` int(11) DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `pending` tinyint(1) NOT NULL DEFAULT '0',
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginAttemptIp` varchar(45) DEFAULT NULL,
  `invalidLoginWindowStart` datetime DEFAULT NULL,
  `invalidLoginCount` tinyint(3) UNSIGNED DEFAULT NULL,
  `lastInvalidLoginDate` datetime DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `hasDashboard` tinyint(1) NOT NULL DEFAULT '0',
  `verificationCode` varchar(255) DEFAULT NULL,
  `verificationCodeIssuedDate` datetime DEFAULT NULL,
  `unverifiedEmail` varchar(255) DEFAULT NULL,
  `passwordResetRequired` tinyint(1) NOT NULL DEFAULT '0',
  `lastPasswordChangeDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `photoId`, `firstName`, `lastName`, `email`, `password`, `admin`, `locked`, `suspended`, `pending`, `lastLoginDate`, `lastLoginAttemptIp`, `invalidLoginWindowStart`, `invalidLoginCount`, `lastInvalidLoginDate`, `lockoutDate`, `hasDashboard`, `verificationCode`, `verificationCodeIssuedDate`, `unverifiedEmail`, `passwordResetRequired`, `lastPasswordChangeDate`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'patrickvanl', NULL, NULL, NULL, 'vanleipsigpatrick@gmail.com', '$2y$13$AkXDdKiHJvyP.QD7m6HrgOECvq7ICM3OABBv5ziPaxekzoR/lMez6', 1, 0, 0, 0, '2020-03-04 18:27:01', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 0, '2020-03-04 09:23:47', '2020-03-04 09:23:47', '2020-03-04 18:27:01', '458073c7-d494-4e67-b033-95919e5c1e04');

-- --------------------------------------------------------

--
-- Table structure for table `volumefolders`
--

CREATE TABLE `volumefolders` (
  `id` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `volumefolders`
--

INSERT INTO `volumefolders` (`id`, `parentId`, `volumeId`, `name`, `path`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, NULL, 1, 'Images', '', '2020-03-04 18:43:08', '2020-03-04 18:43:08', '230e1f17-1055-4b55-9497-d65203a78ab7'),
(2, NULL, NULL, 'Temporary source', NULL, '2020-03-04 18:43:55', '2020-03-04 18:43:55', '186c3716-c389-4b5d-aa57-246fcbbd2248'),
(3, 2, NULL, 'user_1', 'user_1/', '2020-03-04 18:43:55', '2020-03-04 18:43:55', 'f5f4b21d-f98f-472b-be03-ee9643d465d6'),
(4, 1, 1, '@webroot', '@webroot/', '2020-03-04 19:39:17', '2020-03-04 19:39:17', '433850be-a388-4aab-9f90-0281675f7a35'),
(5, 4, 1, 'images', '@webroot/images/', '2020-03-04 19:39:17', '2020-03-04 19:39:17', '28814a03-ebaa-4c3b-bc53-d3988c9b5b89'),
(6, 5, 1, 'uploads', '@webroot/images/uploads/', '2020-03-04 19:39:17', '2020-03-04 19:39:17', '60afc575-cd79-4253-baf9-17618ec61f76'),
(7, 1, 1, 'images', 'images/', '2020-03-04 19:54:38', '2020-03-04 19:54:38', '59b5c4db-2ad5-4670-afb4-d3485dad67c8'),
(8, 7, 1, 'uploads', 'images/uploads/', '2020-03-04 19:54:38', '2020-03-04 19:54:38', 'f19f2eb3-ebad-479b-af48-97b573c5ba19'),
(9, NULL, 2, 'Images', '', '2020-03-04 20:02:47', '2020-03-04 20:02:47', '57885184-6aeb-4b92-a038-cf2e1b91d8f6');

-- --------------------------------------------------------

--
-- Table structure for table `volumes`
--

CREATE TABLE `volumes` (
  `id` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `url` varchar(255) DEFAULT NULL,
  `settings` text,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `volumes`
--

INSERT INTO `volumes` (`id`, `fieldLayoutId`, `name`, `handle`, `type`, `hasUrls`, `url`, `settings`, `sortOrder`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, NULL, 'Images', 'images', 'craft\\volumes\\Local', 0, 'images/uploads', '{\"path\":\"images/uploads\"}', 1, '2020-03-04 18:43:08', '2020-03-04 19:52:22', '2020-03-04 20:02:25', '794bc5d0-f4c1-4a48-b750-93e2f7c8f290'),
(2, NULL, 'Images', 'images', 'craft\\volumes\\Local', 1, '@web/images/uploads', '{\"path\":\"@webroot/images/uploads\"}', 2, '2020-03-04 20:02:47', '2020-03-04 20:09:15', NULL, '6f33e3a2-877c-4050-b3ec-b0510164719c');

-- --------------------------------------------------------

--
-- Table structure for table `widgets`
--

CREATE TABLE `widgets` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `colspan` tinyint(3) DEFAULT NULL,
  `settings` text,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `widgets`
--

INSERT INTO `widgets` (`id`, `userId`, `type`, `sortOrder`, `colspan`, `settings`, `enabled`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 'craft\\widgets\\RecentEntries', 1, NULL, '{\"section\":\"*\",\"siteId\":\"1\",\"limit\":10}', 1, '2020-03-04 09:23:48', '2020-03-04 09:23:48', '639cfac1-6784-4e01-b38a-9b93b1617f38'),
(2, 1, 'craft\\widgets\\CraftSupport', 2, NULL, '[]', 1, '2020-03-04 09:23:48', '2020-03-04 09:23:48', '8830551d-ea12-4997-818a-37baf6e961d2'),
(3, 1, 'craft\\widgets\\Updates', 3, NULL, '[]', 1, '2020-03-04 09:23:48', '2020-03-04 09:23:48', '8ddaeed8-57b3-47ab-b5dc-6b7a9b409e33'),
(4, 1, 'craft\\widgets\\Feed', 4, NULL, '{\"url\":\"https://craftcms.com/news.rss\",\"title\":\"Craft News\",\"limit\":5}', 1, '2020-03-04 09:23:48', '2020-03-04 09:23:48', '7458132e-d718-4140-ae43-fd9173bc5c71');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assetindexdata`
--
ALTER TABLE `assetindexdata`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assetindexdata_sessionId_volumeId_idx` (`sessionId`,`volumeId`),
  ADD KEY `assetindexdata_volumeId_idx` (`volumeId`);

--
-- Indexes for table `assets`
--
ALTER TABLE `assets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assets_filename_folderId_idx` (`filename`,`folderId`),
  ADD KEY `assets_folderId_idx` (`folderId`),
  ADD KEY `assets_volumeId_idx` (`volumeId`),
  ADD KEY `assets_uploaderId_fk` (`uploaderId`);

--
-- Indexes for table `assettransformindex`
--
ALTER TABLE `assettransformindex`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assettransformindex_volumeId_assetId_location_idx` (`volumeId`,`assetId`,`location`);

--
-- Indexes for table `assettransforms`
--
ALTER TABLE `assettransforms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `assettransforms_name_unq_idx` (`name`),
  ADD UNIQUE KEY `assettransforms_handle_unq_idx` (`handle`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_groupId_idx` (`groupId`),
  ADD KEY `categories_parentId_fk` (`parentId`);

--
-- Indexes for table `categorygroups`
--
ALTER TABLE `categorygroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categorygroups_name_idx` (`name`),
  ADD KEY `categorygroups_handle_idx` (`handle`),
  ADD KEY `categorygroups_structureId_idx` (`structureId`),
  ADD KEY `categorygroups_fieldLayoutId_idx` (`fieldLayoutId`),
  ADD KEY `categorygroups_dateDeleted_idx` (`dateDeleted`);

--
-- Indexes for table `categorygroups_sites`
--
ALTER TABLE `categorygroups_sites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categorygroups_sites_groupId_siteId_unq_idx` (`groupId`,`siteId`),
  ADD KEY `categorygroups_sites_siteId_idx` (`siteId`);

--
-- Indexes for table `changedattributes`
--
ALTER TABLE `changedattributes`
  ADD PRIMARY KEY (`elementId`,`siteId`,`attribute`),
  ADD KEY `changedattributes_elementId_siteId_dateUpdated_idx` (`elementId`,`siteId`,`dateUpdated`),
  ADD KEY `changedattributes_siteId_fk` (`siteId`),
  ADD KEY `changedattributes_userId_fk` (`userId`);

--
-- Indexes for table `changedfields`
--
ALTER TABLE `changedfields`
  ADD PRIMARY KEY (`elementId`,`siteId`,`fieldId`),
  ADD KEY `changedfields_elementId_siteId_dateUpdated_idx` (`elementId`,`siteId`,`dateUpdated`),
  ADD KEY `changedfields_siteId_fk` (`siteId`),
  ADD KEY `changedfields_fieldId_fk` (`fieldId`),
  ADD KEY `changedfields_userId_fk` (`userId`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `content_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  ADD KEY `content_siteId_idx` (`siteId`),
  ADD KEY `content_title_idx` (`title`);

--
-- Indexes for table `craftidtokens`
--
ALTER TABLE `craftidtokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `craftidtokens_userId_fk` (`userId`);

--
-- Indexes for table `deprecationerrors`
--
ALTER TABLE `deprecationerrors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `deprecationerrors_key_fingerprint_unq_idx` (`key`,`fingerprint`);

--
-- Indexes for table `drafts`
--
ALTER TABLE `drafts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `drafts_creatorId_fk` (`creatorId`),
  ADD KEY `drafts_sourceId_fk` (`sourceId`);

--
-- Indexes for table `elementindexsettings`
--
ALTER TABLE `elementindexsettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `elementindexsettings_type_unq_idx` (`type`);

--
-- Indexes for table `elements`
--
ALTER TABLE `elements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `elements_dateDeleted_idx` (`dateDeleted`),
  ADD KEY `elements_fieldLayoutId_idx` (`fieldLayoutId`),
  ADD KEY `elements_type_idx` (`type`),
  ADD KEY `elements_enabled_idx` (`enabled`),
  ADD KEY `elements_archived_dateCreated_idx` (`archived`,`dateCreated`),
  ADD KEY `elements_archived_dateDeleted_draftId_revisionId_idx` (`archived`,`dateDeleted`,`draftId`,`revisionId`),
  ADD KEY `elements_draftId_fk` (`draftId`),
  ADD KEY `elements_revisionId_fk` (`revisionId`);

--
-- Indexes for table `elements_sites`
--
ALTER TABLE `elements_sites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `elements_sites_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  ADD KEY `elements_sites_siteId_idx` (`siteId`),
  ADD KEY `elements_sites_slug_siteId_idx` (`slug`,`siteId`),
  ADD KEY `elements_sites_enabled_idx` (`enabled`),
  ADD KEY `elements_sites_uri_siteId_idx` (`uri`,`siteId`);

--
-- Indexes for table `entries`
--
ALTER TABLE `entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `entries_postDate_idx` (`postDate`),
  ADD KEY `entries_expiryDate_idx` (`expiryDate`),
  ADD KEY `entries_authorId_idx` (`authorId`),
  ADD KEY `entries_sectionId_idx` (`sectionId`),
  ADD KEY `entries_typeId_idx` (`typeId`),
  ADD KEY `entries_parentId_fk` (`parentId`);

--
-- Indexes for table `entrytypes`
--
ALTER TABLE `entrytypes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `entrytypes_name_sectionId_idx` (`name`,`sectionId`),
  ADD KEY `entrytypes_handle_sectionId_idx` (`handle`,`sectionId`),
  ADD KEY `entrytypes_sectionId_idx` (`sectionId`),
  ADD KEY `entrytypes_fieldLayoutId_idx` (`fieldLayoutId`),
  ADD KEY `entrytypes_dateDeleted_idx` (`dateDeleted`);

--
-- Indexes for table `fieldgroups`
--
ALTER TABLE `fieldgroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fieldgroups_name_unq_idx` (`name`);

--
-- Indexes for table `fieldlayoutfields`
--
ALTER TABLE `fieldlayoutfields`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fieldlayoutfields_layoutId_fieldId_unq_idx` (`layoutId`,`fieldId`),
  ADD KEY `fieldlayoutfields_sortOrder_idx` (`sortOrder`),
  ADD KEY `fieldlayoutfields_tabId_idx` (`tabId`),
  ADD KEY `fieldlayoutfields_fieldId_idx` (`fieldId`);

--
-- Indexes for table `fieldlayouts`
--
ALTER TABLE `fieldlayouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fieldlayouts_dateDeleted_idx` (`dateDeleted`),
  ADD KEY `fieldlayouts_type_idx` (`type`);

--
-- Indexes for table `fieldlayouttabs`
--
ALTER TABLE `fieldlayouttabs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fieldlayouttabs_sortOrder_idx` (`sortOrder`),
  ADD KEY `fieldlayouttabs_layoutId_idx` (`layoutId`);

--
-- Indexes for table `fields`
--
ALTER TABLE `fields`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fields_handle_context_unq_idx` (`handle`,`context`),
  ADD KEY `fields_groupId_idx` (`groupId`),
  ADD KEY `fields_context_idx` (`context`);

--
-- Indexes for table `globalsets`
--
ALTER TABLE `globalsets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `globalsets_name_idx` (`name`),
  ADD KEY `globalsets_handle_idx` (`handle`),
  ADD KEY `globalsets_fieldLayoutId_idx` (`fieldLayoutId`);

--
-- Indexes for table `gqlschemas`
--
ALTER TABLE `gqlschemas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gqltokens`
--
ALTER TABLE `gqltokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `gqltokens_accessToken_unq_idx` (`accessToken`),
  ADD UNIQUE KEY `gqltokens_name_unq_idx` (`name`),
  ADD KEY `gqltokens_schemaId_fk` (`schemaId`);

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `matrixblocks`
--
ALTER TABLE `matrixblocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `matrixblocks_ownerId_idx` (`ownerId`),
  ADD KEY `matrixblocks_fieldId_idx` (`fieldId`),
  ADD KEY `matrixblocks_typeId_idx` (`typeId`),
  ADD KEY `matrixblocks_sortOrder_idx` (`sortOrder`);

--
-- Indexes for table `matrixblocktypes`
--
ALTER TABLE `matrixblocktypes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `matrixblocktypes_name_fieldId_unq_idx` (`name`,`fieldId`),
  ADD UNIQUE KEY `matrixblocktypes_handle_fieldId_unq_idx` (`handle`,`fieldId`),
  ADD KEY `matrixblocktypes_fieldId_idx` (`fieldId`),
  ADD KEY `matrixblocktypes_fieldLayoutId_idx` (`fieldLayoutId`);

--
-- Indexes for table `matrixcontent_modules`
--
ALTER TABLE `matrixcontent_modules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `matrixcontent_modules_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  ADD KEY `matrixcontent_modules_siteId_fk` (`siteId`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `migrations_pluginId_idx` (`pluginId`),
  ADD KEY `migrations_type_pluginId_idx` (`type`,`pluginId`);

--
-- Indexes for table `plugins`
--
ALTER TABLE `plugins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plugins_handle_unq_idx` (`handle`);

--
-- Indexes for table `projectconfig`
--
ALTER TABLE `projectconfig`
  ADD PRIMARY KEY (`path`);

--
-- Indexes for table `queue`
--
ALTER TABLE `queue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `queue_channel_fail_timeUpdated_timePushed_idx` (`channel`,`fail`,`timeUpdated`,`timePushed`),
  ADD KEY `queue_channel_fail_timeUpdated_delay_idx` (`channel`,`fail`,`timeUpdated`,`delay`);

--
-- Indexes for table `relations`
--
ALTER TABLE `relations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `relations_fieldId_sourceId_sourceSiteId_targetId_unq_idx` (`fieldId`,`sourceId`,`sourceSiteId`,`targetId`),
  ADD KEY `relations_sourceId_idx` (`sourceId`),
  ADD KEY `relations_targetId_idx` (`targetId`),
  ADD KEY `relations_sourceSiteId_idx` (`sourceSiteId`);

--
-- Indexes for table `resourcepaths`
--
ALTER TABLE `resourcepaths`
  ADD PRIMARY KEY (`hash`);

--
-- Indexes for table `revisions`
--
ALTER TABLE `revisions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `revisions_sourceId_num_unq_idx` (`sourceId`,`num`),
  ADD KEY `revisions_creatorId_fk` (`creatorId`);

--
-- Indexes for table `searchindex`
--
ALTER TABLE `searchindex`
  ADD PRIMARY KEY (`elementId`,`attribute`,`fieldId`,`siteId`);
ALTER TABLE `searchindex` ADD FULLTEXT KEY `searchindex_keywords_idx` (`keywords`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sections_handle_idx` (`handle`),
  ADD KEY `sections_name_idx` (`name`),
  ADD KEY `sections_structureId_idx` (`structureId`),
  ADD KEY `sections_dateDeleted_idx` (`dateDeleted`);

--
-- Indexes for table `sections_sites`
--
ALTER TABLE `sections_sites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sections_sites_sectionId_siteId_unq_idx` (`sectionId`,`siteId`),
  ADD KEY `sections_sites_siteId_idx` (`siteId`);

--
-- Indexes for table `sequences`
--
ALTER TABLE `sequences`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_uid_idx` (`uid`),
  ADD KEY `sessions_token_idx` (`token`),
  ADD KEY `sessions_dateUpdated_idx` (`dateUpdated`),
  ADD KEY `sessions_userId_idx` (`userId`);

--
-- Indexes for table `shunnedmessages`
--
ALTER TABLE `shunnedmessages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `shunnedmessages_userId_message_unq_idx` (`userId`,`message`);

--
-- Indexes for table `sitegroups`
--
ALTER TABLE `sitegroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sitegroups_name_idx` (`name`);

--
-- Indexes for table `sites`
--
ALTER TABLE `sites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sites_dateDeleted_idx` (`dateDeleted`),
  ADD KEY `sites_handle_idx` (`handle`),
  ADD KEY `sites_sortOrder_idx` (`sortOrder`),
  ADD KEY `sites_groupId_fk` (`groupId`);

--
-- Indexes for table `structureelements`
--
ALTER TABLE `structureelements`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `structureelements_structureId_elementId_unq_idx` (`structureId`,`elementId`),
  ADD KEY `structureelements_root_idx` (`root`),
  ADD KEY `structureelements_lft_idx` (`lft`),
  ADD KEY `structureelements_rgt_idx` (`rgt`),
  ADD KEY `structureelements_level_idx` (`level`),
  ADD KEY `structureelements_elementId_idx` (`elementId`);

--
-- Indexes for table `structures`
--
ALTER TABLE `structures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `structures_dateDeleted_idx` (`dateDeleted`);

--
-- Indexes for table `systemmessages`
--
ALTER TABLE `systemmessages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `systemmessages_key_language_unq_idx` (`key`,`language`),
  ADD KEY `systemmessages_language_idx` (`language`);

--
-- Indexes for table `taggroups`
--
ALTER TABLE `taggroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `taggroups_name_idx` (`name`),
  ADD KEY `taggroups_handle_idx` (`handle`),
  ADD KEY `taggroups_dateDeleted_idx` (`dateDeleted`),
  ADD KEY `taggroups_fieldLayoutId_fk` (`fieldLayoutId`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tags_groupId_idx` (`groupId`);

--
-- Indexes for table `templatecacheelements`
--
ALTER TABLE `templatecacheelements`
  ADD KEY `templatecacheelements_cacheId_idx` (`cacheId`),
  ADD KEY `templatecacheelements_elementId_idx` (`elementId`);

--
-- Indexes for table `templatecachequeries`
--
ALTER TABLE `templatecachequeries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `templatecachequeries_cacheId_idx` (`cacheId`),
  ADD KEY `templatecachequeries_type_idx` (`type`);

--
-- Indexes for table `templatecaches`
--
ALTER TABLE `templatecaches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `templatecaches_cacheKey_siteId_expiryDate_path_idx` (`cacheKey`,`siteId`,`expiryDate`,`path`),
  ADD KEY `templatecaches_cacheKey_siteId_expiryDate_idx` (`cacheKey`,`siteId`,`expiryDate`),
  ADD KEY `templatecaches_siteId_idx` (`siteId`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tokens_token_unq_idx` (`token`),
  ADD KEY `tokens_expiryDate_idx` (`expiryDate`);

--
-- Indexes for table `usergroups`
--
ALTER TABLE `usergroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usergroups_handle_unq_idx` (`handle`),
  ADD UNIQUE KEY `usergroups_name_unq_idx` (`name`);

--
-- Indexes for table `usergroups_users`
--
ALTER TABLE `usergroups_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usergroups_users_groupId_userId_unq_idx` (`groupId`,`userId`),
  ADD KEY `usergroups_users_userId_idx` (`userId`);

--
-- Indexes for table `userpermissions`
--
ALTER TABLE `userpermissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userpermissions_name_unq_idx` (`name`);

--
-- Indexes for table `userpermissions_usergroups`
--
ALTER TABLE `userpermissions_usergroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userpermissions_usergroups_permissionId_groupId_unq_idx` (`permissionId`,`groupId`),
  ADD KEY `userpermissions_usergroups_groupId_idx` (`groupId`);

--
-- Indexes for table `userpermissions_users`
--
ALTER TABLE `userpermissions_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userpermissions_users_permissionId_userId_unq_idx` (`permissionId`,`userId`),
  ADD KEY `userpermissions_users_userId_idx` (`userId`);

--
-- Indexes for table `userpreferences`
--
ALTER TABLE `userpreferences`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_uid_idx` (`uid`),
  ADD KEY `users_verificationCode_idx` (`verificationCode`),
  ADD KEY `users_email_idx` (`email`),
  ADD KEY `users_username_idx` (`username`),
  ADD KEY `users_photoId_fk` (`photoId`);

--
-- Indexes for table `volumefolders`
--
ALTER TABLE `volumefolders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `volumefolders_name_parentId_volumeId_unq_idx` (`name`,`parentId`,`volumeId`),
  ADD KEY `volumefolders_parentId_idx` (`parentId`),
  ADD KEY `volumefolders_volumeId_idx` (`volumeId`);

--
-- Indexes for table `volumes`
--
ALTER TABLE `volumes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `volumes_name_idx` (`name`),
  ADD KEY `volumes_handle_idx` (`handle`),
  ADD KEY `volumes_fieldLayoutId_idx` (`fieldLayoutId`),
  ADD KEY `volumes_dateDeleted_idx` (`dateDeleted`);

--
-- Indexes for table `widgets`
--
ALTER TABLE `widgets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `widgets_userId_idx` (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assetindexdata`
--
ALTER TABLE `assetindexdata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `assettransformindex`
--
ALTER TABLE `assettransformindex`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `assettransforms`
--
ALTER TABLE `assettransforms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categorygroups`
--
ALTER TABLE `categorygroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categorygroups_sites`
--
ALTER TABLE `categorygroups_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `craftidtokens`
--
ALTER TABLE `craftidtokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deprecationerrors`
--
ALTER TABLE `deprecationerrors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;

--
-- AUTO_INCREMENT for table `drafts`
--
ALTER TABLE `drafts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `elementindexsettings`
--
ALTER TABLE `elementindexsettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `elements`
--
ALTER TABLE `elements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `elements_sites`
--
ALTER TABLE `elements_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `entrytypes`
--
ALTER TABLE `entrytypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `fieldgroups`
--
ALTER TABLE `fieldgroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fieldlayoutfields`
--
ALTER TABLE `fieldlayoutfields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `fieldlayouts`
--
ALTER TABLE `fieldlayouts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `fieldlayouttabs`
--
ALTER TABLE `fieldlayouttabs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `fields`
--
ALTER TABLE `fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `globalsets`
--
ALTER TABLE `globalsets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gqlschemas`
--
ALTER TABLE `gqlschemas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gqltokens`
--
ALTER TABLE `gqltokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `info`
--
ALTER TABLE `info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `matrixblocktypes`
--
ALTER TABLE `matrixblocktypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `matrixcontent_modules`
--
ALTER TABLE `matrixcontent_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=166;

--
-- AUTO_INCREMENT for table `plugins`
--
ALTER TABLE `plugins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `queue`
--
ALTER TABLE `queue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `relations`
--
ALTER TABLE `relations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `revisions`
--
ALTER TABLE `revisions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sections_sites`
--
ALTER TABLE `sections_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `shunnedmessages`
--
ALTER TABLE `shunnedmessages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sitegroups`
--
ALTER TABLE `sitegroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sites`
--
ALTER TABLE `sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `structureelements`
--
ALTER TABLE `structureelements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `structures`
--
ALTER TABLE `structures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `systemmessages`
--
ALTER TABLE `systemmessages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `taggroups`
--
ALTER TABLE `taggroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `templatecachequeries`
--
ALTER TABLE `templatecachequeries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `templatecaches`
--
ALTER TABLE `templatecaches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tokens`
--
ALTER TABLE `tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `usergroups`
--
ALTER TABLE `usergroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usergroups_users`
--
ALTER TABLE `usergroups_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `userpermissions`
--
ALTER TABLE `userpermissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `userpermissions_usergroups`
--
ALTER TABLE `userpermissions_usergroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `userpermissions_users`
--
ALTER TABLE `userpermissions_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `userpreferences`
--
ALTER TABLE `userpreferences`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `volumefolders`
--
ALTER TABLE `volumefolders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `volumes`
--
ALTER TABLE `volumes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `widgets`
--
ALTER TABLE `widgets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assetindexdata`
--
ALTER TABLE `assetindexdata`
  ADD CONSTRAINT `assetindexdata_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `assets`
--
ALTER TABLE `assets`
  ADD CONSTRAINT `assets_folderId_fk` FOREIGN KEY (`folderId`) REFERENCES `volumefolders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assets_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assets_uploaderId_fk` FOREIGN KEY (`uploaderId`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `assets_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `categories_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `categories_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `categories` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `categorygroups`
--
ALTER TABLE `categorygroups`
  ADD CONSTRAINT `categorygroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `categorygroups_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `categorygroups_sites`
--
ALTER TABLE `categorygroups_sites`
  ADD CONSTRAINT `categorygroups_sites_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `categorygroups_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `changedattributes`
--
ALTER TABLE `changedattributes`
  ADD CONSTRAINT `changedattributes_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `changedattributes_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `changedattributes_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `changedfields`
--
ALTER TABLE `changedfields`
  ADD CONSTRAINT `changedfields_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `changedfields_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `changedfields_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `changedfields_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `content`
--
ALTER TABLE `content`
  ADD CONSTRAINT `content_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `content_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `craftidtokens`
--
ALTER TABLE `craftidtokens`
  ADD CONSTRAINT `craftidtokens_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `drafts`
--
ALTER TABLE `drafts`
  ADD CONSTRAINT `drafts_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `drafts_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `elements`
--
ALTER TABLE `elements`
  ADD CONSTRAINT `elements_draftId_fk` FOREIGN KEY (`draftId`) REFERENCES `drafts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `elements_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `elements_revisionId_fk` FOREIGN KEY (`revisionId`) REFERENCES `revisions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `elements_sites`
--
ALTER TABLE `elements_sites`
  ADD CONSTRAINT `elements_sites_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `elements_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `entries`
--
ALTER TABLE `entries`
  ADD CONSTRAINT `entries_authorId_fk` FOREIGN KEY (`authorId`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `entries_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `entries_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `entries` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `entries_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `entries_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `entrytypes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `entrytypes`
--
ALTER TABLE `entrytypes`
  ADD CONSTRAINT `entrytypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `entrytypes_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fieldlayoutfields`
--
ALTER TABLE `fieldlayoutfields`
  ADD CONSTRAINT `fieldlayoutfields_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fieldlayoutfields_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fieldlayoutfields_tabId_fk` FOREIGN KEY (`tabId`) REFERENCES `fieldlayouttabs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fieldlayouttabs`
--
ALTER TABLE `fieldlayouttabs`
  ADD CONSTRAINT `fieldlayouttabs_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fields`
--
ALTER TABLE `fields`
  ADD CONSTRAINT `fields_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `fieldgroups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `globalsets`
--
ALTER TABLE `globalsets`
  ADD CONSTRAINT `globalsets_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `globalsets_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `gqltokens`
--
ALTER TABLE `gqltokens`
  ADD CONSTRAINT `gqltokens_schemaId_fk` FOREIGN KEY (`schemaId`) REFERENCES `gqlschemas` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `matrixblocks`
--
ALTER TABLE `matrixblocks`
  ADD CONSTRAINT `matrixblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `matrixblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `matrixblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `matrixblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `matrixblocktypes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `matrixblocktypes`
--
ALTER TABLE `matrixblocktypes`
  ADD CONSTRAINT `matrixblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `matrixblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `matrixcontent_modules`
--
ALTER TABLE `matrixcontent_modules`
  ADD CONSTRAINT `matrixcontent_modules_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `matrixcontent_modules_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `migrations`
--
ALTER TABLE `migrations`
  ADD CONSTRAINT `migrations_pluginId_fk` FOREIGN KEY (`pluginId`) REFERENCES `plugins` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `relations`
--
ALTER TABLE `relations`
  ADD CONSTRAINT `relations_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `relations_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `relations_sourceSiteId_fk` FOREIGN KEY (`sourceSiteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `relations_targetId_fk` FOREIGN KEY (`targetId`) REFERENCES `elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `revisions`
--
ALTER TABLE `revisions`
  ADD CONSTRAINT `revisions_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `revisions_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sections`
--
ALTER TABLE `sections`
  ADD CONSTRAINT `sections_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `sections_sites`
--
ALTER TABLE `sections_sites`
  ADD CONSTRAINT `sections_sites_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sections_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sessions`
--
ALTER TABLE `sessions`
  ADD CONSTRAINT `sessions_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `shunnedmessages`
--
ALTER TABLE `shunnedmessages`
  ADD CONSTRAINT `shunnedmessages_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sites`
--
ALTER TABLE `sites`
  ADD CONSTRAINT `sites_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `sitegroups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `structureelements`
--
ALTER TABLE `structureelements`
  ADD CONSTRAINT `structureelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `structureelements_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `taggroups`
--
ALTER TABLE `taggroups`
  ADD CONSTRAINT `taggroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `tags`
--
ALTER TABLE `tags`
  ADD CONSTRAINT `tags_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `taggroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tags_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `templatecacheelements`
--
ALTER TABLE `templatecacheelements`
  ADD CONSTRAINT `templatecacheelements_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `templatecacheelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `templatecachequeries`
--
ALTER TABLE `templatecachequeries`
  ADD CONSTRAINT `templatecachequeries_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `templatecaches`
--
ALTER TABLE `templatecaches`
  ADD CONSTRAINT `templatecaches_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `usergroups_users`
--
ALTER TABLE `usergroups_users`
  ADD CONSTRAINT `usergroups_users_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `usergroups_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `userpermissions_usergroups`
--
ALTER TABLE `userpermissions_usergroups`
  ADD CONSTRAINT `userpermissions_usergroups_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `userpermissions_usergroups_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `userpermissions_users`
--
ALTER TABLE `userpermissions_users`
  ADD CONSTRAINT `userpermissions_users_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `userpermissions_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `userpreferences`
--
ALTER TABLE `userpreferences`
  ADD CONSTRAINT `userpreferences_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_photoId_fk` FOREIGN KEY (`photoId`) REFERENCES `assets` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `volumefolders`
--
ALTER TABLE `volumefolders`
  ADD CONSTRAINT `volumefolders_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `volumefolders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `volumefolders_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `volumes`
--
ALTER TABLE `volumes`
  ADD CONSTRAINT `volumes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `widgets`
--
ALTER TABLE `widgets`
  ADD CONSTRAINT `widgets_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
